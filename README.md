FABUI CAM tool collection

## Getting started

To run the conversion tools you need python >= 3.5 and all dependencies installed.

## OS setup

```bash
>> sudo apt-get install python3-pip
>> sudo apt-get install python3-opencv
>> sudo apt-get install libopencv
>> sudo apt-get install geos
>> sudo apt-get install virtualenv
```

## Environment setup

```bash
>> virtualenv venv
>> source venv/bin/activate
>> python -m pip install -r requirements.txt
```

## Presets

Presets for different convertors are located in `/presets` directory.