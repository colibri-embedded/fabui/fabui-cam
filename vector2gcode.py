#!/usr/bin/env python
# # -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "2.0"

# Import standard python module
import argparse
import time
import gettext
import json
import os

import numpy as np

from jsonschema import validate

from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.filewriter import FileWriter
from fabui.cam.toolpath.laser25dplanner import Laser25DPlanner
#from fabui.cam.toolpath.milling25dplanner import Milling25DPlanner
from fabui.cam.toolpath.engraving25dplanner import Engraving25DPlanner
from fabui.cam.toolpath.debug25dplanner import Debug25DPlanner
from fabui.cam.loaders.dxfgrabber.color import TrueColor
################################################################################

def prepare_dxf_file(dxf_file, config):
    """
    Apply dxf specific settings
    """
    scale = config['general']['scale']
    
    align_map = {
        'middle-center': Drawing2D.MIDDLE_CENTER,
        'middle-left':   Drawing2D.MIDDLE_LEFT,
        'middle-right':  Drawing2D.MIDDLE_RIGHT,
        'top-center':    Drawing2D.TOP_CENTER,
        'top-left':      Drawing2D.TOP_LEFT,
        'top-right':     Drawing2D.TOP_RIGHT,
        'bottom-center': Drawing2D.BOTTOM_CENTER,
        'bottom-left':   Drawing2D.BOTTOM_LEFT,
        'bottom-right':  Drawing2D.BOTTOM_RIGHT
    }
    
    ###
    
    drawing = Drawing2D()
    drawing.loadFromDXF(dxf_file)
    drawing.scale( scale, scale )
    
    # Align to selected homing point
    drawing.align_to( align_map[ config['general']['align'] ] )
    
    return drawing

def laser_planner(dxf_file, gcode_file, config, preview_file=''):
    """
    Laser engraving
    """
    drawing = prepare_dxf_file(dxf_file, config)
    
    tpp = Laser25DPlanner(gcode_file, config, ndigits=3)
    tpp.plan(drawing)
    tpp.finish()

    # Generate preview
    if preview_file:
        drawing.scale_to(1000)
        
        drawing.normalize()
        
        width = drawing.width()
        height = drawing.height()
    
        colors = {}

        for l in config['layers']:
            if 'color' in l:
                colors[ l['name'] ] = ( l['color']['r'], l['color']['g'], l['color']['b'] )

        for l in drawing.layers:
            if l.name not in colors:
                tc = TrueColor.from_aci(l.color)
                colors[l.name] = tc.rgb()

        dbg = Debug25DPlanner(width, height, config, ndigits=3, colors=colors)
        dbg.plan(drawing)
        dbg.finish()
        
        import cv2
        cv2.imwrite(preview_file, dbg.cnc.dbg_img)
        
# def milling_planner(dxf_file, gcode_file, config, preview_file=''):
#     """
#     Milling
#     """
#     drawing = prepare_dxf_file(dxf_file, config)
    
#     tpp = Milling25DPlanner(gcode_file, config, ndigits=3)
#     tpp.plan(drawing)
#     tpp.finish()

def engraving_planner(dxf_file, gcode_file, config, preview_file=''):
    """
    Milling
    """
    drawing = prepare_dxf_file(dxf_file, config)
    
    tpp = Engraving25DPlanner(gcode_file, config, ndigits=3)
    tpp.plan(drawing)
    tpp.finish()

    # Generate preview
    if preview_file:
        drawing.scale_to(1000)
        
        drawing.normalize()
        
        width = drawing.width()
        height = drawing.height()
    
        colors = {}

        for l in config['layers']:
            if 'color' in l:
                colors[ l['name'] ] = ( l['color']['r'], l['color']['g'], l['color']['b'] )

        for l in drawing.layers:
            if l.name not in colors:
                colors[l.name] = (0,0,0)

        dbg = Debug25DPlanner(width, height, config, ndigits=3, colors=colors)
        dbg.plan(drawing)
        dbg.finish()
        
        import cv2
        cv2.imwrite(preview_file, dbg.cnc.dbg_img)

def main():
    # SETTING EXPECTED ARGUMENTS
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("config_file",          help="Preset file [json file]")
    parser.add_argument("input_file",           help="Image file [dxf file]")
    parser.add_argument("-d", "--preview", action="store_true",      help="Generate preview image", default=False)
    parser.add_argument("-o", "--output",       help="Output gcode file.",    default='output.gcode')
    parser.add_argument("-P", "--planner",      help="Planner (laser, engraving).", required=True)
    
    # GET ARGUMENTS
    args = parser.parse_args()

    # INIT VARs
    gcode_file      = args.output
    dxf_file        = args.input_file
    config_file     = args.config_file
    planner         = args.planner
    preview         = args.preview
    preview_file    = ''

    topdir = os.path.dirname(os.path.abspath(__file__))
    output_path = os.path.dirname(gcode_file)

    if planner == 'laser':
        schema_file = os.path.join(topdir,"schemas/laser-vector.schema")
    elif planner == 'engraving':
        schema_file = os.path.join(topdir,"schemas/engraving-vector.schema")
    # elif planner == 'milling':
    #     schema_file = os.path.join(topdir,"schemas/milling-vector.schema")
    else:
        print "Unknown planner '{0}'".format(planner)
        exit(1)

    with open(schema_file) as f:
        schema = json.load(f)
        
    with open(config_file) as f:
        config = json.load(f)
    
    import traceback

    try:
        validate(config, schema)
        layer_schema = schema['properties']['layers']['item']
        for layer in config['layers']:
            validate(layer, layer_schema)
        
        output = FileWriter(gcode_file)

        if preview:
            preview_file = os.path.join(output_path, 'preview.png')
        
        if planner == 'laser':
            laser_planner(dxf_file, output, config, preview_file)
        #elif planner == 'milling':
            #milling_planner(dxf_file, gcode_file, config, preview_file)
        elif planner == 'engraving':
            engraving_planner(dxf_file, gcode_file, config, preview_file)
        
        output.close()
        
    except Exception as e:
        traceback.print_exc()
        #print str(e)

        exit(1)
    
if __name__ == "__main__":
    main()
