#!/usr/bin/env python
# # -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

# Import standard python module
import argparse
import time
import gettext
import json
import os

from jsonschema import validate

# from fabui.cam.loaders import gerber
from fabui.cam.loaders.gerber.render import ShapelyContext

from fabui.cam.common.pcb import PCB
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.toolpath.milling25dplanner import Milling25DPlanner
from fabui.cam.toolpath.engraving25dplanner import Engraving25DPlanner
from fabui.cam.toolpath.isolation25dplanner import Isolation25DPlanner
from fabui.cam.toolpath.debug25dplanner import Debug25DPlanner

from fabui.cam.gcode.filewriter import FileWriter
from img2gcode import laser_planner

import cv2
import numpy as np

def pcb_laser_planner(input_dir, output_dir, config, preview_file=''):
    """
    """
    dpm = 1.0 / config['general']['dot_size']
    exclude_holes = config['pcb']['exclude_holes']

    pcb = PCB(dpm=dpm)
    pcb.from_directory(input_dir)

    top    = pcb.render_top_layers(exclude_holes=exclude_holes)
    bottom = pcb.render_bottom_layers(exclude_holes=exclude_holes)

    top_raster_file = os.path.join(output_dir, 'top.png')
    top_gcode_file  = os.path.join(output_dir, 'top.gcode')
    bottom_raster_file = os.path.join(output_dir, 'bottom.png')
    bottom_gcode_file  = os.path.join(output_dir, 'bottom.gcode')
    #drill_gcode_file  = os.path.join(output_dir, 'drill.gcode')
    #edge_gcode_file  = os.path.join(output_dir, 'edge.gcode')

    cv2.imwrite(top_raster_file, top)
    cv2.imwrite(bottom_raster_file, bottom)

    config['general']['width'] = pcb.width
    config['general']['height'] = pcb.height

    # Generate GCode for top layer
    laser_planner(top_raster_file, top_gcode_file, config)
    # Generate GCode for bottom layer
    laser_planner(bottom_raster_file, bottom_gcode_file, config)

    if preview_file:
        # top
        # bottom
        preview = np.concatenate((top, bottom), axis=0)

        height, width = preview.shape
        ratio = float(height) / float(width)

        width = 1000
        height = int(round(ratio * width))

        preview = cv2.resize(preview, (height, height), interpolation=cv2.INTER_NEAREST)

        cv2.imwrite(preview_file, preview)

def pcb_milling_planner(input_dir, output_dir, config, preview_file=''):
    """
    """
    def prepare_pcb_layer(pcb_layer, config):
        """
        Prepare a PCB layer for further processing.

        @param pcb_layer PCB layer
        @param config Configuration
        """
        shp = ShapelyContext()

        layer_name = layer.layer_class
        # if layer.mirrored:
        #     layer_name += '_mirrored'
        
        if layer.layer_class == 'outline':
            shp.set_ignore_width(True)
            
        layer.cam_source.render( shp )
        if layer.mirrored:
            shp.mirror_x()
        
        if config['general']['rotation']:
            shp.rotate( float(config['general']['rotation']) )

        return (layer_name, shp)

    pcb_config = config['pcb']

    use_mirrored = pcb_config['use_mirrored']
    mark_drill_holes = pcb_config['mark_drilling_holes']
    cut_side = pcb_config['cutting_side']
    drill_side = pcb_config['drilling_side']

    pcb = PCB()
    pcb.from_directory(input_dir)

    for layer in pcb.copper_layers:
        if not layer.mirrored and layer.layer_class == "bottom":
            continue

        layer_name, shp = prepare_pcb_layer(layer, config)
        gcode_filename = os.path.join(output_dir, layer_name + '.gcode')

        print layer_name, gcode_filename, layer.mirrored

        drawing = Drawing2D(default_layer_name=layer_name)
        for fig in shp.figs:
            drawing.addShapelyGeometry(fig)

        tpp_config = {
            'general': config['general'],
            'travel-feedrate': config['general']['travel_feedrate'],
            'safe-z': config['general']['safe_z'],
            'plunge-rate': config['pcb']['milling_z_feedrate'],
            'retract-rate': config['pcb']['milling_z_feedrate'],
            'tool-diameter': config['pcb']['milling_bit_diameter'],
            'layers': {}
        }

        tpp_config['layers'][layer_name] = {
            'feedrate': {
                'type': 'const',
                'value': config['pcb']['milling_feedrate'],
            },
            'speed': {
                'type': 'const',
                'value': config['pcb']['milling_spindle_speed']
            },
            'cut': {
                'type': 'continuous',
                'depth': config['pcb']['milling_depth'],
                'steps': 1,
                'passes': config['pcb']['number_of_milling_passes'],
                'sequential': False
            }
        }

        tpp = Isolation25DPlanner(gcode_filename, tpp_config)
        tpp.plan(drawing)
        tpp.finish()

    for layer in pcb.outline_layers:
        if layer.mirrored and cut_side != "bottom":
            continue

        if not layer.mirrored and cut_side != "top":
            continue

        layer_name, shp = prepare_pcb_layer(layer, config)
        gcode_filename = os.path.join(output_dir, layer_name + '.gcode')
        print layer_name, gcode_filename

        tpp_config = {
            'general': config['general'],
            'travel-feedrate': config['general']['travel_feedrate'],
            'safe-z': config['general']['safe_z'],
            'plunge-rate': config['pcb']['cutting_z_feedrate'],
            'retract-rate': config['pcb']['cutting_z_feedrate'],
            'tool-diameter': config['pcb']['cutting_bit_diameter'],
            'layers': {}
        }

        tpp_config['layers'][layer_name] = {
            'feedrate': {
                'type': 'const',
                'value': config['pcb']['cutting_feedrate'],
            },
            'cut': {
                'type': 'continuous',
                'depth': config['pcb']['cutting_depth'],
                'steps': int(config['pcb']['cutting_depth'] / config['pcb']['cutting_step']),
                'passes': 1,
                'sequential': False
            }
        }

        # tpp =
        # tpp = Isolation25DPlanner(gcode_filename, tpp_config)

    for layer in pcb.drill_layers:
        # Choose the correct side for drilling
        if layer.mirrored and drill_side != "bottom":
            continue

        if not layer.mirrored and drill_side != "top":
            continue

        layer_name, shp = prepare_pcb_layer(layer, config)
        gcode_filename = os.path.join(output_dir, layer_name + '.gcode')
        print layer_name, gcode_filename

        tpp_config = {
            'general': config['general'],
            'travel-feedrate': config['general']['travel_feedrate'],
            'safe-z': config['general']['safe_z'],
            'plunge-rate': config['pcb']['drilling_z_feedrate'],
            'retract-rate': config['pcb']['drilling_z_feedrate'],
            'layers': {}
        }

        tpp_config['layers'][layer_name] = {
            'feedrate': {
                'type': 'const',
                'value': config['general']['travel_feedrate'],
            },
            'cut': {
                'type': 'continuous',
                'depth': config['pcb']['drilling_depth'],
                'steps': 1,
                'passes': 1,
                'sequential': False
            }
        }

        tpp = Milling25DPlanner(gcode_filename, tpp_config)
        #tpp.plan(d)
        tpp.finish()



def main():
    # SETTING EXPECTED ARGUMENTS
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("config_file",          help="Preset file [json file]")
    parser.add_argument("input_dir",            help="Directory containing PCB gerber files")
    parser.add_argument("-d", "--preview", action="store_true",      help="Generate preview image", default=False)
    parser.add_argument("-o", "--output-dir",       help="Output dir.",    default='./')
    parser.add_argument("-P", "--planner",      help="Planner (laser, milling).")
    
    # GET ARGUMENTS
    args = parser.parse_args()

    # INIT VARs
    output_dir      = args.output_dir
    input_dir       = args.input_dir
    config_file     = args.config_file
    planner         = args.planner
    preview         = args.preview
    preview_file    = ''
    
    topdir = os.path.dirname(os.path.abspath(__file__))
    try:
        os.makedirs(output_dir)
    except:
        pass

    if planner == 'laser':
        schema_file = os.path.join(topdir, "schemas/laser-pcb.schema")
    elif planner == 'milling':
        schema_file = os.path.join(topdir, "schemas/milling-pcb.schema")
    else:
        print "Unknown planner '{0}'".format(planner)
        exit(1)

    with open(schema_file) as f:
        schema = json.load(f)
        
    with open(config_file) as f:
        config = json.load(f)
    
    try:
        validate(config, schema)
        if 'layers' in schema['properties']:
            layer_schema = schema['properties']['layers']['item']
            for layer in config['layers']:
                validate(layer, layer_schema)

        if preview:
            preview_file = os.path.join(output_dir, 'preview.png')

        if planner == 'laser':
            pcb_laser_planner(input_dir, output_dir, config, preview_file)

        elif planner == 'milling':
            pcb_milling_planner(input_dir, output_dir, config, preview_file)
        
    except Exception as e:
        raise
        # print str(e)
        # exit(1)
    
if __name__ == "__main__":
    main()