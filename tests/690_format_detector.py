import subprocess
import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.loaders.detect import detect_format

class TestFormatDetector(TestCase):
    
    def setUp(self):
        pass
        
    def test_png_format(self):
        fmt = detect_format('./data/png/chicken_twist_logo_496x330.png')
        self.assertEquals('image/png', fmt)
        
    def test_jpg_format(self):
        fmt = detect_format('./data/jpg/cat_armor_color_1920x1080.jpg')
        self.assertEquals('image/jpeg', fmt)
        
    def test_dxf_format(self):
        fmt = detect_format('./data/dxf/fabtotum_MFNY2017.dxf')
        self.assertEquals('application/dxf', fmt)
        
    def test_stl_format(self):
        fmt = detect_format('./data/stl/makerook_small_support.stl')
        self.assertEquals('application/sla', fmt)
        
    def test_svg_format(self):
        fmt = detect_format('./data/svg/inkscape_primitives.svg')
        self.assertEquals('image/svg+xml', fmt)

if __name__ == '__main__':
    unittest.main()
