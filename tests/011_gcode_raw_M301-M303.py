import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_M301(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M301()
        
        self.assertTrue('parameter must be specified'in str(context.exception))

    def test_M301_p(self):
        self.__default_config()
        
        self.raw.M301(10.3)
        self.assertEqual(str(self.output), 'M301 P10.3')

    def test_M301_p_param(self):
        self.__default_config()
        
        self.raw.M301(P=10.3)
        self.assertEqual(str(self.output), 'M301 P10.3')

    def test_M301_i_param(self):
        self.__default_config()
        
        self.raw.M301(I=10.3)
        self.assertEqual(str(self.output), 'M301 I10.3')

    def test_M301_d_param(self):
        self.__default_config()
        
        self.raw.M301(D=10.3)
        self.assertEqual(str(self.output), 'M301 D10.3')
        
    def test_M301_pid_param(self):
        self.__default_config()
        
        self.raw.M301(P=10.3,I=12.4,D=14.5)
        self.assertEqual(str(self.output), 'M301 P10.3 I12.4 D14.5')
        
    def test_M301_pid_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M301(P=10.3,I=12.4,D=14.5)
        self.assertEqual(str(self.output), 'M301P10.3I12.4D14.5')
        
    def test_M302(self):
        self.__default_config()
        
        self.raw.M302()
        self.assertEqual(str(self.output), 'M302')
        
    def test_M302_s(self):
        self.__default_config()
        
        self.raw.M302(100)
        self.assertEqual(str(self.output), 'M302 S100')
        
    def test_M302_s_param(self):
        self.__default_config()
        
        self.raw.M302(S=100)
        self.assertEqual(str(self.output), 'M302 S100')
        
    def test_M302_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M302(S=100)
        self.assertEqual(str(self.output), 'M302S100')
        
    def test_M302_s_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M106(RAW.TEMP_MAX +1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_M302_s_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M106(RAW.TEMP_MIN -1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
    
    def test_M303(self):
        self.__default_config(compact=True)
        
        with self.assertRaises(TypeError) as context:
            self.raw.M303()
        
        self.assertTrue("arguments (1 given)" in str(context.exception))
    
    def test_M303_s(self):
        self.__default_config()
        
        self.raw.M303(100)
        self.assertEqual(str(self.output), 'M303 S100')
    
    def test_M303_s_param(self):
        self.__default_config()
        
        self.raw.M303(S=100)
        self.assertEqual(str(self.output), 'M303 S100')
    
    def test_M303_c_param(self):
        self.__default_config()
        
        self.raw.M303(S=100, C=8)
        self.assertEqual(str(self.output), 'M303 S100 C8')
    
    def test_M303_e_param(self):
        self.__default_config()
        
        self.raw.M303(S=100,E=-1)
        self.assertEqual(str(self.output), 'M303 S100 E-1')
    
    def test_M303_sce_param(self):
        self.__default_config()
        
        self.raw.M303(S=100,C=8,E=-1)
        self.assertEqual(str(self.output), 'M303 S100 C8 E-1')
    
    def test_M303_sce_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M303(S=100,C=8,E=-1)
        self.assertEqual(str(self.output), 'M303S100C8E-1')
    
if __name__ == '__main__':
    unittest.main()
