import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False):
        self.cnc.useProbe = use_probe
        self.output.clear()
        
    def __move_axis(self, x = 0.0, y = 0.0, z = 0.0, e = 0.0, f = 100):
        self.cnc.curX = x
        self.cnc.curY = y
        self.cnc.curZ = z
        self.cnc.curE = e
        self.cnc.setFeedrate(f)

    def test_moveto_new_feedrate(self):
        self.__default_config()
        
        self.__move_axis(f=10)
        self.cnc.moveTo(X=10, Y=12, F=100)
        self.assertEqual(str(self.output), 'G1 X10 Y12 F100')
        
    def test_moveto_old_feedrate(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.moveTo(X=1, Y=1, F=100)
        self.output.clear()
        
        self.cnc.moveTo(X=10, Y=12, F=100)
        self.assertEqual(str(self.output), 'G1 X10 Y12')
    
    def test_moveby_new_feedrate(self):
        self.__default_config()
        
        self.__move_axis(f=10)
        self.cnc.moveBy(X=10, Y=12, F=100)
        self.assertEqual(str(self.output), 'G1 X10 Y12 F100')
        
    def test_moveby_old_feedrate(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.moveBy(X=1, Y=1, F=100)
        self.output.clear()
        
        self.cnc.moveBy(X=9, Y=11, F=100)
        self.assertEqual(str(self.output), 'G1 X10 Y12')
        
    def test_set_feedrate_moveto(self):
        self.__default_config()
        self.__move_axis(f=100)
        
        self.cnc.setFeedrate(300)
        self.cnc.moveTo(10, 20)
        
        feedrate = self.cnc.getFeedrate()
        self.assertEqual(feedrate, 300)
        self.assertEqual(str(self.output), 'G1 X10 Y20 F300')
        
    def test_set_feedrate_moveby(self):
        self.__default_config()
        self.__move_axis(f=100)
        
        self.cnc.setFeedrate(300)
        self.cnc.moveBy(10, 20)
        
        feedrate = self.cnc.getFeedrate()
        self.assertEqual(feedrate, 300)
        self.assertEqual(str(self.output), 'G1 X10 Y20 F300')

if __name__ == '__main__':
    unittest.main()
