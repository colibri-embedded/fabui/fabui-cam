import unittest
from unittest import TestCase
#from mock import patch, Mock, PropertyMock

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact

    def test_G2(self):
        self.__default_config()
        
        self.raw.G2()
        self.assertEqual(str(self.output), 'G2')
        
    def test_G2_xy(self):
        self.__default_config()
        
        self.raw.G2(10.5, 11.5)
        self.assertEqual(str(self.output), 'G2 X10.5 Y11.5')
        
    def test_G2_z_error(self):
        self.__default_config()
        
        with self.assertRaises(TypeError) as context:
            self.raw.G2(Z=12.5)
        
        self.assertTrue("unexpected keyword argument 'Z'" in str(context.exception))

    def test_G2_xyij(self):
        self.__default_config()
        
        self.raw.G2(10.5, 11.5, 12.5, 13.5)
        self.assertEqual(str(self.output), 'G2 X10.5 Y11.5 I12.5 J13.5')

    def test_G2_xyijef(self):
        self.__default_config()
        
        self.raw.G2(10.5, 11.5, 12.5, 13.5, 3.5, 300)
        self.assertEqual(str(self.output), 'G2 X10.5 Y11.5 I12.5 J13.5 E3.5 F300')

    def test_G2_xyijf(self):
        self.__default_config()
        
        self.raw.G2(10.5, 11.5, 12.5, 13.5, F=300)
        self.assertEqual(str(self.output), 'G2 X10.5 Y11.5 I12.5 J13.5 F300')

    def test_G3(self):
        self.__default_config()
        
        self.raw.G3()
        self.assertEqual(str(self.output), 'G3')
        
    def test_G3_xy(self):
        self.__default_config()
        
        self.raw.G3(10.5, 11.5)
        self.assertEqual(str(self.output), 'G3 X10.5 Y11.5')
        
    def test_G3_xyijef(self):
        self.__default_config()
        
        self.raw.G3(10.5, 11.5, 12.5, 13.5, 3.5, 300)
        self.assertEqual(str(self.output), 'G3 X10.5 Y11.5 I12.5 J13.5 E3.5 F300')

    def test_G4(self):
        self.__default_config()
        
        self.raw.G4()
        self.assertEqual(str(self.output), 'G4')
        
    def test_G4_s(self):
        self.__default_config()
        
        self.raw.G4(5)
        self.assertEqual(str(self.output), 'G4 S5')
        
    def test_G4_p(self):
        self.__default_config()
        
        self.raw.G4(P=5)
        self.assertEqual(str(self.output), 'G4 P5')
        
    def test_G4_s_and_p_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.G4(1, 500)
        
        self.assertTrue('G4: S and P parameters' in str(context.exception))
        
if __name__ == '__main__':
    unittest.main()
