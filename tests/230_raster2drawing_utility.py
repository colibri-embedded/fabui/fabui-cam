import unittest
from unittest import TestCase
import numpy as np

# Unit under test
from img2gcode import laser_planner

from fabui.cam.common.raster import Raster
from fabui.cam.common.raster2drawing import ( 
    image2drawing, angle2line_params, line_bbox_intersec, line_length )

class TestRaster2Drawing(TestCase):
    
    def setUp(self):
        pass
        
    def test_angle2line_params_0deg(self):
        k, c, c_max = angle2line_params(0, 100, 50)

        self.assertEqual(k, 0.0)
        self.assertEqual(c, 0.0)
        self.assertEqual(c_max, 50)
        
    def test_angle2line_params_90deg(self):
        k, c, c_max = angle2line_params(90, 100, 50)

        self.assertEqual(k, np.inf)
        self.assertEqual(c, 0.0)
        self.assertEqual(c_max, 100)
        
    def test_angle2line_params_45deg(self):
        k, c, c_max = angle2line_params(45, 100, 50)

        self.assertEqual(k, 1.0)
        self.assertEqual(c, -100)
        self.assertEqual(c_max, 150)
        
    def test_angle2line_params_neg45deg(self):
        k, c, c_max = angle2line_params(-45, 100, 50)

        self.assertEqual(k, -1.0)
        self.assertEqual(c, -100)
        self.assertEqual(c_max, 150)
        
    def test_bbox_line_0deg(self):
        pts = line_bbox_intersec(100, 50, k=0.0, c=0.0)
        
        self.assertEqual( len(pts), 2 )
        self.assertEqual( pts[0], (0,0) )
        self.assertEqual( pts[1], (100,0) )
        
    def test_bbox_line_90deg(self):
        pts = line_bbox_intersec(100, 50, k=np.inf, c=0.0)
        
        self.assertEqual( len(pts), 2 )
        self.assertEqual( pts[0], (0,0) )
        self.assertEqual( pts[1], (0,50) )
        
    def test_bbox_line_45deg(self):
        pts = line_bbox_intersec(100, 50, k=1, c=0.0)
        
        self.assertEqual( len(pts), 2 )
        self.assertEqual( pts[0], (0,0) )
        self.assertEqual( pts[1], (50,50) )
        
    def test_bbox_line_neg45deg(self):
        pts = line_bbox_intersec(100, 50, k=-1, c=50.0)
        
        self.assertEqual( len(pts), 2 )
        self.assertEqual( pts[0], (0,50) )
        self.assertEqual( pts[1], (50,0) )
    
    def test_line_length(self):
        
        self.assertEqual( line_length( (0,0), (10,0) ), 10.0)
        self.assertEqual( line_length( (0,0), (0,10) ), 10.0)
        self.assertEqual( line_length( (10,0), (20,0) ), 10.0)
        self.assertEqual( line_length( (0,10), (0,20) ), 10.0)
            
if __name__ == '__main__':
    unittest.main()
