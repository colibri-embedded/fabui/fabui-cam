import unittest
from unittest import TestCase
#from mock import patch, Mock, PropertyMock

# Unit under test
from fabui.cam.gcode.raw import RAW

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact

    def test_G0(self):
        self.__default_config()
        
        self.raw.G0()
        self.assertEqual(str(self.output), 'G0')
        
    def test_G0_x(self):
        self.__default_config()
        
        self.raw.G0(10)
        self.assertEqual(str(self.output), 'G0 X10')
        
    def test_G0_x_param(self):
        self.__default_config()
        self.raw.G0(X=10)
        self.assertEqual(str(self.output), 'G0 X10')
        
    def test_G0_y_param(self):
        self.__default_config()
        
        self.raw.G0(Y=10)
        self.assertEqual(str(self.output), 'G0 Y10')
        
    def test_G0_z_param(self):
        self.__default_config()
        
        self.raw.G0(Z=10)
        self.assertEqual(str(self.output), 'G0 Z10')
        
    def test_G0_e_param(self):
        self.__default_config()
        
        self.raw.G0(E=10)
        self.assertEqual(str(self.output), 'G0 E10')
        
    def test_G0_f_param(self):
        self.__default_config()
        
        self.raw.G0(F=100)
        self.assertEqual(str(self.output), 'G0 F100')
        
    def test_G0_xy(self):
        self.__default_config()
        
        self.raw.G0(10, 11)
        self.assertEqual(str(self.output), 'G0 X10 Y11')
        
    def test_G0_xyz(self):
        self.__default_config()
        
        self.raw.G0(10, 11, 12)
        self.assertEqual(str(self.output), 'G0 X10 Y11 Z12')
        
    def test_G0_xyze(self):
        self.__default_config()
        
        self.raw.G0(10, 11, 12, 13)
        self.assertEqual(str(self.output), 'G0 X10 Y11 Z12 E13')
        
    def test_G0_xyzef(self):
        self.__default_config()
        
        self.raw.G0(10, 11, 12, 13, 300)
        self.assertEqual(str(self.output), 'G0 X10 Y11 Z12 E13 F300')
        
    def test_G0_xyzf(self):
        self.__default_config()
        
        self.raw.G0(10, 11, 12, F=300)
        self.assertEqual(str(self.output), 'G0 X10 Y11 Z12 F300')
        
    def test_G0_xyzef_float(self):
        self.__default_config()
        
        self.raw.G0(10.1, 11.1, 12.1, 13.1, 300.1)
        self.assertEqual(str(self.output), 'G0 X10.1 Y11.1 Z12.1 E13.1 F300.1')
        
    def test_G0_xyzef_float_param(self):
        self.__default_config()
        
        self.raw.G0(Y=10.1, Z=11.1, X=12.1, E=13.1, F=300.1)
        self.assertEqual(str(self.output), 'G0 X12.1 Y10.1 Z11.1 E13.1 F300.1')
        
    def test_G1_xyzef(self):
        self.__default_config()
        
        self.raw.G1(10.1, 11.1, 12.1, 13.1, 300.1)
        self.assertEqual(str(self.output), 'G1 X10.1 Y11.1 Z12.1 E13.1 F300.1')

    def test_G1_xyzef_float_param(self):
        self.__default_config()
        
        self.raw.G1(Y=10.1, Z=11.1, X=12.1, E=13.1, F=300.1)
        self.assertEqual(str(self.output), 'G1 X12.1 Y10.1 Z11.1 E13.1 F300.1')
        
    def test_G0_x_2_ndigits(self):
        self.__default_config(ndigits = 2)
        
        self.raw.G0(10.123456)
        self.assertEqual(str(self.output), 'G0 X10.12')
        
    def test_G0_x_3_ndigits(self):
        self.__default_config(ndigits = 3)
        
        self.raw.G0(10.123456)
        self.assertEqual(str(self.output), 'G0 X10.123')
        
    def test_G0_x_4_ndigits(self):
        """
        Python round() function rounds up if the last decimal is 5,
        therefore 10.12345 becomes 10.1235
        """
        self.__default_config(ndigits = 4)
        
        self.raw.G0(10.123456)
        self.assertEqual(str(self.output), 'G0 X10.1235')
        
    def test_G0_xyzf_compact(self):
        self.__default_config(compact = True)
        
        self.raw.G0(10.5, 11.6, 12.7, F=300)
        self.assertEqual(str(self.output), 'G0X10.5Y11.6Z12.7F300')
        
    def test_G0_xyzf_compact_2_ndigits(self):
        self.__default_config(ndigits = 2, compact = True)
        
        self.raw.G0(10.512, 11.612, 12.712, F=300)
        self.assertEqual(str(self.output), 'G0X10.51Y11.61Z12.71F300')
        
    def test_M400(self):
        self.__default_config()
        
        self.raw.M400()
        self.assertEqual(str(self.output), 'M400')

if __name__ == '__main__':
    unittest.main()
