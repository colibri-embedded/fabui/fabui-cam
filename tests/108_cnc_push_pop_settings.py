import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        pass

    def __default_config(self, feedrate=100, use_probe=False, xyzMode=CNC.eABSOLUTE, eMode=CNC.eABSOLUTE):
        output = OneWriteOutput()
        
        config = {
            'feedrate': feedrate,
            'use-probe': use_probe,
            'xyz-mode': xyzMode,
            'e-mode': eMode
        }
        
        cnc = CNC(output, config=config, ndigits=3)
        
        return output, cnc

    def test_push_settings(self):
        output, cnc = self.__default_config()
        
        cnc.pushSettings()
        settings = cnc.settingsStack.pop(-1)
        
        self.assertEqual( settings['feedrate'], cnc.config['feedrate'] )
        self.assertEqual( settings['xyz-mode'], cnc.config['xyz-mode'] )
        self.assertEqual( settings['e-mode'], cnc.config['e-mode'] )
        self.assertEqual( settings['use-probe'], cnc.config['use-probe'] )
        
    def test_push_pop_settings_abs_rel(self):
        output, cnc = self.__default_config()
        
        cnc.pushSettings()
        
        cnc.setAxisMode(CNC.X_AXIS, CNC.eRELATIVE)
        cnc.setAxisMode(CNC.E_AXIS, CNC.eRELATIVE)
        
        cnc.popSettings()
        self.assertTrue( cnc.isAbsolute(CNC.X_AXIS) )
        self.assertEqual( str(output), 'G91|M83|G90|M82')
        
    def test_push_pop_settings_abs_abs(self):
        output, cnc = self.__default_config()
        
        cnc.pushSettings()
        
        cnc.setAxisMode(CNC.X_AXIS, CNC.eABSOLUTE)
        cnc.setAxisMode(CNC.E_AXIS, CNC.eABSOLUTE)
        
        cnc.popSettings()
        self.assertTrue( cnc.isAbsolute(CNC.X_AXIS) )
        self.assertEqual( str(output), '')
        
    def test_push_pop_settings_feedrate(self):
        output, cnc = self.__default_config()
        
        # Push
        cnc.pushSettings()
        cnc.setFeedrate(300)
        cnc.moveTo(10, 20)
        
        # Pop
        cnc.popSettings()
        cnc.moveTo(0, 2)
        cnc.moveBy(1, 3)
        
        self.assertEqual( str(output), 'G1 X10 Y20 F300|G1 X0 Y2 F100|G1 X1 Y5')
        
if __name__ == '__main__':
    unittest.main()
