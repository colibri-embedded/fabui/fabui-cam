import unittest
from unittest import TestCase
#from mock import patch, Mock, PropertyMock

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_G10_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.G10()
        
        self.assertTrue("no attribute 'G10'" in str(context.exception))
        
    def test_G11_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.G11()
        
        self.assertTrue("no attribute 'G11'" in str(context.exception))
        
    def test_M0_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M0()
        
        self.assertTrue("no attribute 'M0'" in str(context.exception))
        
    def test_M1_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M1()
        
        self.assertTrue("no attribute 'M1'" in str(context.exception))
        
    def test_M42_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M42()
        
        self.assertTrue("no attribute 'M42'" in str(context.exception))
        
    def test_M80_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M80()
        
        self.assertTrue("no attribute 'M80'" in str(context.exception))
        
    def test_M81_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M81()
        
        self.assertTrue("no attribute 'M81'" in str(context.exception))
        
    def test_M84_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M84()
        
        self.assertTrue("no attribute 'M84'" in str(context.exception))
        
    def test_M200_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M200()
        
        self.assertTrue("no attribute 'M200'" in str(context.exception))
        
    def test_M202_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M202()
        
        self.assertTrue("no attribute 'M202'" in str(context.exception))
        
    def test_M304_not_implemented(self):
        self.__default_config()
        
        with self.assertRaises(AttributeError) as context:
            self.raw.M304()
        
        self.assertTrue("no attribute 'M304'" in str(context.exception))

        
if __name__ == '__main__':
    unittest.main()
