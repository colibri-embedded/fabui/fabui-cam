import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False):
        self.cnc.useProbe = use_probe
        self.output.clear()
        
    def __move_axis(self, x = 0.0, y = 0.0, z = 0.0, e = 0.0, f = 100):
        self.cnc.curX = x
        self.cnc.curY = y
        self.cnc.curZ = z
        self.cnc.curE = e
        self.cnc.setFeedrate(f)

    # Tool moves
    def test_move_by_x(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10)
        self.cnc.moveBy(1)
        self.assertEqual(str(self.output), 'G1 X10 F100|G1 X11')
        self.assertTrue( self.cnc.isAt(X=11) )
        
    def test_move_by_x_neg(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10)
        self.cnc.moveBy(-1)
        self.assertEqual(str(self.output), 'G1 X10 F100|G1 X9')
        self.assertTrue( self.cnc.isAt(X=9) )
        
    def test_move_by_x_ndigit(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10)
        self.cnc.moveBy(1.001)
        self.assertEqual(str(self.output), 'G1 X10 F100|G1 X11.001')
        self.assertTrue( self.cnc.isAt(X=11.001) )
        
    def test_move_by_x_ndigit2(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10)
        self.cnc.moveBy(1.0005)
        self.assertEqual(str(self.output), 'G1 X10 F100|G1 X11.001')
        self.assertTrue( self.cnc.isAt(X=11.001) )
        
    def test_move_by_y(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Y=10)
        self.cnc.moveBy(Y=1)
        self.assertEqual(str(self.output), 'G1 Y10 F100|G1 Y11')
        self.assertTrue( self.cnc.isAt(Y=11) )
        
    def test_move_by_y_neg(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Y=10)
        self.cnc.moveBy(Y=-1)
        self.assertEqual(str(self.output), 'G1 Y10 F100|G1 Y9')
        self.assertTrue( self.cnc.isAt(Y=9) )
        
    def test_move_by_y_ndigit(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Y=10)
        self.cnc.moveBy(Y=1.001)
        self.assertEqual(str(self.output), 'G1 Y10 F100|G1 Y11.001')
        self.assertTrue( self.cnc.isAt(Y=11.001) )
        
    def test_move_by_y_ndigit2(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Y=10)
        self.cnc.moveBy(Y=1.0005)
        self.assertEqual(str(self.output), 'G1 Y10 F100|G1 Y11.001')
        self.assertTrue( self.cnc.isAt(Y=11.001) )
        
    def test_move_by_z(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Z=10)
        self.cnc.moveBy(Z=1)
        self.assertEqual(str(self.output), 'G1 Z10 F100|G1 Z11')
        self.assertTrue( self.cnc.isAt(Z=11) )
        
    def test_move_by_z_neg(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Z=10)
        self.cnc.moveBy(Z=-1)
        self.assertEqual(str(self.output), 'G1 Z10 F100|G1 Z9')
        self.assertTrue( self.cnc.isAt(Z=9) )
        
    def test_move_by_z_ndigit(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Z=10)
        self.cnc.moveBy(Z=1.001)
        self.assertEqual(str(self.output), 'G1 Z10 F100|G1 Z11.001')
        self.assertTrue( self.cnc.isAt(Z=11.001) )
        
    def test_move_by_z_ndigit2(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Z=10)
        self.cnc.moveBy(Z=1.0005)
        self.assertEqual(str(self.output), 'G1 Z10 F100|G1 Z11.001')
        self.assertTrue( self.cnc.isAt(Z=11.001) )
        
    def test_move_by_e(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(E=10)
        self.cnc.moveBy(E=1)
        self.assertEqual(str(self.output), 'G1 E10 F100|G1 E11')
        self.assertTrue( self.cnc.isAt(E=11) )
        
    def test_move_by_e_neg(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(E=10)
        self.cnc.moveBy(E=-1)
        self.assertEqual(str(self.output), 'G1 E10 F100|G1 E9')
        self.assertTrue( self.cnc.isAt(E=9) )

    def test_move_by_e_ndigit(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(E=10)
        self.cnc.moveBy(E=1.001)
        self.assertEqual(str(self.output), 'G1 E10 F100|G1 E11.001')
        self.assertTrue( self.cnc.isAt(E=11.001) )
        
    def test_move_by_e_ndigit2(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(E=10)
        self.cnc.moveBy(E=1.0005)
        self.assertEqual(str(self.output), 'G1 E10 F100|G1 E11.001')
        self.assertTrue( self.cnc.isAt(E=11.001) )
        
    def test_move_by_xy(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10, 11)
        self.cnc.moveBy(1, 2)
        self.assertEqual(str(self.output), 'G1 X10 Y11 F100|G1 X11 Y13')
        self.assertTrue( self.cnc.isAt(X=11, Y=13) )
        
    def test_move_by_xyz(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10, 11, 13)
        self.cnc.moveBy(1, 2, 3)
        self.assertEqual(str(self.output), 'G1 X10 Y11 Z13 F100|G1 X11 Y13 Z16')
        self.assertTrue( self.cnc.isAt(X=11, Y=13, Z=16) )
        
    def test_move_by_xyze(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10, 11, 13, 14)
        self.cnc.moveBy(1, 2, 3, 4)
        self.assertEqual(str(self.output), 'G1 X10 Y11 Z13 E14 F100|G1 X11 Y13 Z16 E18')
        self.assertTrue( self.cnc.isAt(X=11, Y=13, Z=16, E=18) )
        
    def test_travel_by_xyze(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10, 11, 13, 14)
        self.cnc.travelBy(1, 2, 3, 4)
        self.assertEqual(str(self.output), 'G1 X10 Y11 Z13 E14 F100|G0 X11 Y13 Z16 E18')
        self.assertTrue( self.cnc.isAt(X=11, Y=13, Z=16, E=18) )

if __name__ == '__main__':
    unittest.main()
