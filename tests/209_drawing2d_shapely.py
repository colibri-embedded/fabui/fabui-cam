import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError
from fabui.cam.common.drawing2d import use_shapely


class TestDrawing2D(TestCase):
    
    def setUp(self):
        pass
    
    @unittest.skipIf(not use_shapely, "shapely library not found")
    def test_new_object(self):
        d = Drawing2D()

if __name__ == '__main__':
    unittest.main()
