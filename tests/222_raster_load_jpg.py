import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.raster import Raster

class TestRaster(TestCase):
    
    def setUp(self):
        pass
    
    def test_raster_load_jpg_upsize_width(self):
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)
        r.loadFromFile('./data/jpg/lion_head_grayscale_500x868.jpg')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (1736,1000) )
        self.assertEqual( r.width(), 1000 )
        self.assertEqual( r.height(), 1736 )
    
    def test_raster_load_jpg_upsize_height(self):
        dpm   = 1 / 0.1 #mm
        height = 100.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/jpg/lion_head_grayscale_500x868.jpg')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (1000,576) )
        self.assertEqual( r.width(), 576 )
        self.assertEqual( r.height(), 1000 )
    
    def test_raster_load_jpg_downsize_width(self):
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)
        r.loadFromFile('./data/jpg/cat_armor_color_1920x1080.jpg')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (562,1000) )
        self.assertEqual( r.width(), 1000 )
        self.assertEqual( r.height(), 562 )
    
    def test_raster_load_jpg_downsize_height(self):
        dpm   = 1 / 0.1 #mm
        height = 100.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/jpg/cat_armor_color_1920x1080.jpg')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (1000,1777) )
        self.assertEqual( r.width(), 1777 )
        self.assertEqual( r.height(), 1000 )
        
    def test_raster_load_grayscale(self):
        dpm   = 1 / 0.1 #mm
        height = 12.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/jpg/mini_pcb_black_white_191x145.jpg')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (120,158) )
        self.assertEqual( r.width(), 158 )
        self.assertEqual( r.height(), 120 )
        
if __name__ == '__main__':
    unittest.main()
