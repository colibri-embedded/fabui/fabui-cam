import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.writerbase import WriterBase
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.toolpath.engraving25dplanner import Engraving25DPlanner

class OneWriteOutput(WriterBase):
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestEngraving25DPlanner(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=True, no_comment=False):
        output = OneWriteOutput()
        planner = Engraving25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        return output, planner
        
    def __default_config(self, optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "travel_feedrate": 10000,
                'safe_z': 4.0,
                'plunge_rate' : 10,
                'retract_rate': 30
            },
            "layers": [
                {
                    "name": "0",
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "speed":{
                        "type": "const",
                        "value": 8000
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 1,
                        "steps": 1,
                        "sequential": False
                    }
                }
            ]
        }
        
    def test_engraving25_planner_new_object(self):
        output, planner = self.__default(compact=True)
        
        self.assertFalse(planner is None)
        
    def test_engraving25_planner_new_object_with_config(self):
        config = self.__default_config()
        output, planner = self.__default(config=config, compact=True)
        
        self.assertFalse(planner is None)
        
    def test_engraving25_planner_engrave(self):
        config = self.__default_config()
        
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
        planner.plan(drawing)
        planner.finish()
        
        self.assertEqual( str(output), 'G0Z4F30|M3S8000|G0X-50Y-30F10000|G1Z-1F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G0Z4F30|G0X0Y0F10000|M400|G4S3|M5')
        
    def test_engraving25_planner_cut(self):
        config = self.__default_config()
        config['layers'][0]['cut']['depth'] = 3
        config['layers'][0]['cut']['steps'] = 2
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
        planner.plan(drawing)
        planner.finish()
        
        self.assertEqual( str(output), 'G0Z4F30|M3S8000|G0X-50Y-30F10000|G1Z-1.5F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G1Z-3F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G0Z4F30|G0X0Y0F10000|M400|G4S3|M5')
        
if __name__ == '__main__':
    unittest.main()
