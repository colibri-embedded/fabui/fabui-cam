import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.milling25d import Milling25D
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestMilling25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = Milling25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
        
    def test_milling25d_spindle_on_cw(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn()
        self.assertEqual(str(output), 'G0Z5F20|M3S14000')
        
    def test_milling25d_spindle_setspeed(self):
        output, cnc = self.__default(compact=True)
        
        cnc.setSpindleSpeed(6000)
        cnc.spindleOn()
        self.assertEqual(str(output), 'G0Z5F20|M3S6000')
        
    def test_milling25d_spindle_on_cw_speed(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn(8000)
        self.assertEqual(str(output), 'G0Z5F20|M3S8000')
        
    def test_milling25d_spindle_on_ccw(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn(direction=CNC.eCCW)
        self.assertEqual(str(output), 'G0Z5F20|M4S14000')
        
    def test_milling25d_spindle_on_ccw_speed(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn(8000, direction=CNC.eCCW)
        self.assertEqual(str(output), 'G0Z5F20|M4S8000')
        
    def test_milling25d_spindle_on_speed_change(self):
        output, cnc = self.__default(compact=True)
        
        cnc.setSpindleSpeed(6000)
        cnc.spindleOn()
        cnc.setSpindleSpeed(8000)
        cnc.spindleOn()
        self.assertEqual(str(output), 'G0Z5F20|M3S6000|M3S8000')
        
    def test_milling25d_spindle_speed_change_mill(self):
        output, cnc = self.__default(compact=True)
        
        cnc.setSpindleSpeed(6000)
        cnc.millTo(10, 11)
        cnc.setSpindleSpeed(8000)
        cnc.millTo(20, 22)
        self.assertEqual(str(output), 'G0Z5F20|M3S6000|G1Z-1F10|G1X10Y11F30|G0Z5F20|M3S8000|G1Z-1F10|G1X20Y22F30')
        
    def test_milling25d_spindle_on_param(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn(speed=8000, direction=CNC.eCW)
        self.assertEqual(str(output), 'G0Z5F20|M3S8000')
        
    def test_milling25d_spindle_idle_off(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOff()
        self.assertEqual(str(output), '')
        
    def test_milling25d_spindle_off(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn()
        cnc.spindleOff()
        self.assertEqual(str(output), 'G0Z5F20|M3S14000|M5')
        
    def test_milling25d_spindle_direction_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.spindleOn(direction=5)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_milling25d_spindle_wrong_type_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.spindleOn('4000')
        
        self.assertTrue('must be a number' in str(context.exception))
        
    def test_milling25d_spindle_rpm_too_high_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.spindleOn(RAW.RPM_MAX +1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_milling25d_spindle_rpm_too_low_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.spindleOn(RAW.RPM_MIN -1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))

    #~ def test_milling25d_auto_spindleon(self):
        #~ output, cnc = self.__default(compact=True)
        
        #~ cnc.travelTo(10, 10)
        #~ self.assertEqual(str(output), 'M3S14000|M5')

if __name__ == '__main__':
    unittest.main()
