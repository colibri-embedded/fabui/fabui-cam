import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.writerbase import WriterBase
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.toolpath.simple25dplanner import Simple25DPlanner

class OneWriteOutput(WriterBase):
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestSimple25DPlanner(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=True, no_comment=False):
        output = OneWriteOutput()
        planner = Simple25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        return output, planner
        
    def __default_config(self, optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "zero_cut": False
            },
            "layers": [
                {
                    "name": "0",
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 1,
                        "steps": 1,
                        "sequential": False
                    }
                }
            ]
        }
        
    def test_simple_planner_new_object(self):
        output, planner = self.__default(compact=True)
        
        self.assertFalse(planner is None)

    def test_simple_planner_optimize0(self):
        config = self.__default_config(optimize=0)
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')
        
        planner.plan(drawing)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1Y100F100|G0Z5F20|G0X20F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X60Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X100Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X80Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X40Y100F100|G1Z-1F10|G1Y0F100' )

    def test_simple_planner_optimize1(self):
        config = self.__default_config(optimize=1)
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')
        
        planner.plan(drawing)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1Y100F100|G0Z5F20|G0X20F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X40Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X60Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X80Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X100Y100F100|G1Z-1F10|G1Y0F100' )

    def test_simple_planner_optimize2(self):
        config = self.__default_config(optimize=2)
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')
        
        planner.plan(drawing)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1Y100F100|G0Z5F20|G0X20F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X40F100|G1Z-1F10|G1Y100F100|G0Z5F20|G0X60F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X80F100|G1Z-1F10|G1Y100F100|G0Z5F20|G0X100F100|G1Z-1F10|G1Y0F100' )
    
    def test_simple_planner_finish(self):
        config = self.__default_config()
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        layer = drawing.getLayerByName('Default')
        layer.name = '0'
        drawing.addLine( (1.0, 2.1), (3.2, 4.3) )
        
        planner.plan(drawing)
        planner.finish()
    
        self.assertEqual( str(output), 'G0Z5F20|G0X1Y2.1F100|G1Z-1F10|G1X3.2Y4.3F100|G0Z5F20|G0X0Y0F100|M400')
        
    def test_simple_planner_cut(self):
        config = self.__default_config()
        config['layers'][0]['cut']['depth'] = 3
        config['layers'][0]['cut']['steps'] = 2
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
        planner.plan(drawing)
        planner.finish()
        
        self.assertEqual( str(output), 'G0Z5F20|G0X-50Y-30F100|G1Z-1.5F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G1Z-3F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G0Z5F20|G0X0Y0F100|M400')
        
    def test_simple_planner_cut_w_zerocut(self):
        config = self.__default_config()
        config['layers'][0]['cut']['depth'] = 3
        config['layers'][0]['cut']['steps'] = 2
        config['general']['zero_cut'] = True
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
        planner.plan(drawing)
        planner.finish()
        
        self.assertEqual( str(output), 'G0Z5F20|G0X-50Y-30F100|G1Z0F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G1Z-1.5F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G0Z5F20|G0X0Y0F100|M400')
        
if __name__ == '__main__':
    unittest.main()
