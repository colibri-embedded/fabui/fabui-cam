import unittest
from unittest import TestCase
from math import sqrt

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

try:
    import shapely
    use_shapely = True
except ImportError:
    use_shapely = False

class TestDrawing2D(TestCase):
    
    def setUp(self):
        self.DXF_LIBRECAD_PRIMITIVES = './data/dxf/librecad_primitives.dxf'
    
    def test_align_middle_center(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        d.align_to(align=Drawing2D.MIDDLE_CENTER)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (0,0) )
    
    def test_align_middle_left(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.MIDDLE_LEFT)
        
        #~ print d.bounds()
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (w2,0) )
    
    def test_align_middle_right(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.MIDDLE_RIGHT)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (-w2,0) )
    
    def test_align_top_center(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.TOP_CENTER)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (0,-h2) )
    
    def test_align_top_left(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.TOP_LEFT)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (w2,-h2) )
    
    def test_align_top_right(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.TOP_RIGHT)
        
        #~ print d.bounds()
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (-w2,-h2) )
    
    def test_align_bottom_center(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.BOTTOM_CENTER)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (0,h2) )
    
    def test_align_bottom_right(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.BOTTOM_RIGHT)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (-w2,h2) )
    
    def test_align_bottom_left(self):
        d = Drawing2D()
        d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        
        w2 = d.width() / 2.0
        h2 = d.height() / 2.0
        
        d.align_to(align=Drawing2D.BOTTOM_LEFT)
        
        x1,y1,x2,y2 = d.bounds()
        
        cx = (x1+x2) / 2
        cy = (y1+y2) / 2
        
        self.assertEqual( (cx,cy), (w2,h2) )
        
if __name__ == '__main__':
    unittest.main()
