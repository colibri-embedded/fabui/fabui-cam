import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.milling25d import Milling25D
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestMilling25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, no_comment=False, position=None):
        output = OneWriteOutput()
        cnc = Milling25D(output, config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
        
    def test_milling25d_finish(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn()
        cnc.millTo(10,11)
        cnc.finish()
        self.assertEqual(str(output), 'G0Z5F20|M3S14000|G1Z-1F10|G1X10Y11F30|; Finish milling|G0Z5F20|M400|; Wait for 3 sec and switch off spindle|G4S3|M5')
        
    def test_milling25d_finish_return_home(self):
        output, cnc = self.__default(compact=True)
        
        cnc.spindleOn()
        cnc.millTo(10,11)
        cnc.finish(return_home=True)
        self.assertEqual(str(output), 'G0Z5F20|M3S14000|G1Z-1F10|G1X10Y11F30|; Finish milling|G0Z5F20|G0X0Y0F100|M400|; Wait for 3 sec and switch off spindle|G4S3|M5')
        
if __name__ == '__main__':
    unittest.main()
