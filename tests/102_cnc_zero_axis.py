import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False):
        self.cnc.useProbe = use_probe
        self.output.clear()
        
    def __move_axis(self):
        self.cnc.curX = 10.5
        self.cnc.curY = 20.6
        self.cnc.curZ = 30.7
        self.cnc.curE = 40.8

    def test_zero_all(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroAll()
        
        self.assertEqual( str(self.output), 'G92')
        
    def test_zero_xyz(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroXYZ()
        self.assertEqual( str(self.output), 'G92 X0 Y0 Z0')
        self.assertEqual( self.cnc.curX, 0.0)
        self.assertEqual( self.cnc.curY, 0.0)
        self.assertEqual( self.cnc.curZ, 0.0)
        self.assertEqual( self.cnc.curE, 40.8)
        
    def test_zero_xy(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroXY()
        self.assertEqual( str(self.output), 'G92 X0 Y0')
        self.assertEqual( self.cnc.curX, 0.0)
        self.assertEqual( self.cnc.curY, 0.0)
        self.assertEqual( self.cnc.curZ, 30.7)
        self.assertEqual( self.cnc.curE, 40.8)
        
    def test_zero_z(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroZ()
        self.assertEqual( str(self.output), 'G92 Z0')
        self.assertEqual( self.cnc.curX, 10.5)
        self.assertEqual( self.cnc.curY, 20.6)
        self.assertEqual( self.cnc.curZ, 0.0)
        self.assertEqual( self.cnc.curE, 40.8)
        
    def test_zero_e(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroE()
        self.assertEqual( str(self.output), 'G92 E0')
        self.assertEqual( self.cnc.curX, 10.5)
        self.assertEqual( self.cnc.curY, 20.6)
        self.assertEqual( self.cnc.curZ, 30.7)
        self.assertEqual( self.cnc.curE, 0.0)
        
    def test_zero_custom(self):
        self.__default_config()
        
        self.cnc.zeroAxis(Y=0.0, Z=0.0)
        self.assertEqual( str(self.output), 'G92 Y0 Z0')
        
    def test_zero_xyz_to_custom(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroXYZ(X=1.2, Y=2.3, Z=4.5)
        self.assertEqual( str(self.output), 'G92 X1.2 Y2.3 Z4.5')
        self.assertEqual( self.cnc.curX, 1.2)
        self.assertEqual( self.cnc.curY, 2.3)
        self.assertEqual( self.cnc.curZ, 4.5)
        self.assertEqual( self.cnc.curE, 40.8)
        
    def test_zero_xy_to_custom(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroXY(X=1.2, Y=2.3)
        self.assertEqual( str(self.output), 'G92 X1.2 Y2.3')
        self.assertEqual( self.cnc.curX, 1.2)
        self.assertEqual( self.cnc.curY, 2.3)
        self.assertEqual( self.cnc.curZ, 30.7)
        self.assertEqual( self.cnc.curE, 40.8)
        
    def test_zero_z_to_custom(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroZ(Z=4.5)
        self.assertEqual( str(self.output), 'G92 Z4.5')
        self.assertEqual( self.cnc.curX, 10.5)
        self.assertEqual( self.cnc.curY, 20.6)
        self.assertEqual( self.cnc.curZ, 4.5)
        self.assertEqual( self.cnc.curE, 40.8)
        
    def test_zero_e_to_custom(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.zeroE(E=6.7)
        self.assertEqual( str(self.output), 'G92 E6.7')
        self.assertEqual( self.cnc.curX, 10.5)
        self.assertEqual( self.cnc.curY, 20.6)
        self.assertEqual( self.cnc.curZ, 30.7)
        self.assertEqual( self.cnc.curE, 6.7)
        
    
        
if __name__ == '__main__':
    unittest.main()
