import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment
    
    # M300
    def test_M300(self):
        self.__default_config()
        
        self.raw.M300()
        self.assertEqual(str(self.output), 'M300')

    def test_M300_s(self):
        self.__default_config()
        
        self.raw.M300(3)
        self.assertEqual(str(self.output), 'M300 S3')
        
    def test_M300_s_param(self):
        self.__default_config()
        
        self.raw.M300(S=3)
        self.assertEqual(str(self.output), 'M300 S3')
        
    def test_M300_sd(self):
        self.__default_config()
        
        self.raw.M300(3, 10)
        self.assertEqual(str(self.output), 'M300 S3 D10')
        
    def test_M300_sdp(self):
        self.__default_config()
        
        self.raw.M300(3, 10, 5)
        self.assertEqual(str(self.output), 'M300 S3 D10 P5')
        
    def test_M300_sdp_param(self):
        self.__default_config()
        
        self.raw.M300(D=3, S=10, P=5)
        self.assertEqual(str(self.output), 'M300 S10 D3 P5')
        
    def test_M300_sdp_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M300(D=3, S=10, P=5)
        self.assertEqual(str(self.output), 'M300S10D3P5')
        
    def test_M300_d_without_s_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M300(D=5)
            
        self.assertTrue('cannot be used without S'in str(context.exception))
        
    def test_M300_p_without_s_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M300(P=5)
            
        self.assertTrue('cannot be used without S'in str(context.exception))
    
    # M700
    def test_M700_s(self):
        self.__default_config()
        
        self.raw.M700(128)
        self.assertEqual(str(self.output), 'M700 S128')
        
    def test_M700_s_param(self):
        self.__default_config()
        
        self.raw.M700(S=128)
        self.assertEqual(str(self.output), 'M700 S128')
        
    def test_M700_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M700(S=128)
        self.assertEqual(str(self.output), 'M700S128')
        
    def test_M700_s_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M700(RAW.PWM_MIN - 1)
        
        self.assertTrue('out of allowed range' in str(context.exception))
        
    def test_M700_s_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M700(RAW.PWM_MAX + 1)
        
        self.assertTrue('out of allowed range' in str(context.exception))
    
    # M701
    def test_M701_s(self):
        self.__default_config()
        
        self.raw.M701(128)
        self.assertEqual(str(self.output), 'M701 S128')
    
    # M702
    def test_M702_s(self):
        self.__default_config()
        
        self.raw.M702(128)
        self.assertEqual(str(self.output), 'M702 S128')
    
    # M703
    def test_M703_s(self):
        self.__default_config()
        
        self.raw.M703(128)
        self.assertEqual(str(self.output), 'M703 S128')

    # M704
    def test_M704(self):
        self.__default_config()
        
        self.raw.M704()
        self.assertEqual(str(self.output), 'M704')
    
    # M705
    def test_M705(self):
        self.__default_config()
        
        self.raw.M705()
        self.assertEqual(str(self.output), 'M705')
    
    # M706
    def test_M706_s(self):
        self.__default_config()
        
        self.raw.M706(128)
        self.assertEqual(str(self.output), 'M706 S128')
    
if __name__ == '__main__':
    unittest.main()
