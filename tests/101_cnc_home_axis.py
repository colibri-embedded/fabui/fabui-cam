import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False):
        self.cnc.useProbe = use_probe
        self.output.clear()

    def test_home_all(self):
        self.__default_config()
        
        self.cnc.homeAll()
        self.assertEqual( str(self.output), 'G27')
        
    def test_home_xy(self):
        self.__default_config()
        
        self.cnc.homeXY()
        self.assertEqual( str(self.output), 'G27 X Y')
        
    def test_home_z(self):
        self.__default_config()
        
        self.cnc.homeZ()
        self.assertEqual( str(self.output), 'G27 Z')
        
    def test_home_custom(self):
        self.__default_config()
        
        self.cnc.homeAxis(Y=True, Z=True)
        self.assertEqual( str(self.output), 'G27 Y Z')

    def test_home_all_w_probe(self):
        self.__default_config(use_probe=True)
        
        self.cnc.homeAll()
        self.assertEqual( str(self.output), 'G28')
        
    def test_home_xy_w_probe(self):
        self.__default_config(use_probe=True)
        
        self.cnc.homeXY()
        self.assertEqual( str(self.output), 'G28 X Y')
        
    def test_home_z_w_probe(self):
        self.__default_config(use_probe=True)
        
        self.cnc.homeZ()
        self.assertEqual( str(self.output), 'G28 Z')
        
    def test_home_custom_w_probe(self):
        self.__default_config(use_probe=True)
        
        self.cnc.homeAxis(Y=True, Z=True)
        self.assertEqual( str(self.output), 'G28 Y Z')
        
if __name__ == '__main__':
    unittest.main()
