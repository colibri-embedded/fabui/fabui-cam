import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.writerbase import WriterBase
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.toolpath.laser25dplanner import Laser25DPlanner

class OneWriteOutput(WriterBase):
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestLaser25DPlanner(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=True, no_comment=False):
        output = OneWriteOutput()
        planner = Laser25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        return output, planner
        
    def __default_config(self, optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "travel_feedrate": 10000,
                "off_during_travel": True
            },
            "layers": [
                {
                    "name": "0",
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "pwm": {
                        "type": "const",
                        "value": 255
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 0,
                        "steps": 1,
                        "sequential": False
                    }
                }
            ]
        }
        
    def test_laser25_planner_new_object(self):
        output, planner = self.__default(compact=True)
        
        self.assertFalse(planner is None)

    def test_laser25_planner_new_object_with_config(self):
        config = self.__default_config()
        output, planner = self.__default(config=config, compact=True)
        
        self.assertFalse(planner is None)
        
    def test_laser25_planner_engrave(self):
        config = self.__default_config()
        
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
        planner.plan(drawing)
        planner.finish()
        
        self.assertEqual( str(output), 'G0X-50Y-30F10000|M61S255|G1X50F100|G1X0Y56.603|G1X-50Y-30|M62|G0X0Y0F10000|M400')
        
    def test_laser25_planner_cut(self):
        config = self.__default_config()
        config['layers'][0]['cut']['depth'] = 3
        config['layers'][0]['cut']['steps'] = 2
        output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
        planner.plan(drawing)
        planner.finish()
        
        self.assertEqual( str(output), 'G0X-50Y-30F10000|M61S255|G1X50F100|G1X0Y56.603|G1X-50Y-30|M62|G0Z-1.5F1000|M61S255|G1X50F100|G1X0Y56.603|G1X-50Y-30|M62|G0X0Y0F10000|M400')
        
if __name__ == '__main__':
    unittest.main()
