import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    
    """
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = CNC(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
    
    def test_cnc_new_object(self):
        output, cnc = self.__default(compact=True)
        
        self.assertFalse(cnc is None)
        self.assertEqual( cnc.config['feedrate'], 30 )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], False )
        
    def test_cnc_new_config(self):
        output, cnc = self.__default(config={
            'feedrate': 50.0,
            'use-probe': True
        })
        self.assertFalse(cnc is None)
        
        self.assertEqual( cnc.config['feedrate'], 50 )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], True )
    
    def test_cnc_set_config(self):
        output, cnc = self.__default()
        
        # Set new config with missing fields
        config={
            'feedrate': 50.0
        }
        cnc.setConfig(config)
        
        self.assertEqual( cnc.config['feedrate'], 50 )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], False )
    
    def test_cnc_update_config(self):
        # Set initial config
        output, cnc = self.__default()
        
        config={
            'feedrate': 50.0
        }
        
        cnc.updateConfig(config)
        
        self.assertEqual( cnc.config['feedrate'], 50 )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], False )
    
    def test_cnc_update_config(self):
        # Set initial config
        output, cnc = self.__default()
        
        cnc.moveTo(20, 21)
        
        config = {'feedrate': 50.0}
        cnc.updateConfig(config)
        
        cnc.moveTo(20, 21)
        
        self.assertEqual( cnc.config['feedrate'], 50 )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], False )
        
        self.assertEqual( str(output), 'G1 X20 Y21 F30' )
        
        
if __name__ == '__main__':
    unittest.main()
