import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.gcode.cnc import CNC

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestCNC25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False):
        output = OneWriteOutput()
        cnc = CNC25D(output, config, ndigits=ndigits, compact=compact)
        
        return output, cnc
    
    def test_cnc25d_new_object(self):
        output, cnc = self.__default()
        
        self.assertFalse(cnc is None)
        self.assertEqual( cnc.config['cut-feedrate'], 30.0 )
        self.assertEqual( cnc.config['travel-feedrate'], 100.0 )
        self.assertEqual( cnc.config['plunge-rate'], 10.0 )
        self.assertEqual( cnc.config['retract-rate'], 20.0 )
        self.assertEqual( cnc.config['safe-z'], 5 )
        self.assertEqual( cnc.config['cut-depth'], 1 )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], False )
    
    def test_cnc25d_new_config(self):
        output, cnc = self.__default(config={
            'cut-feedrate': 50.0,
            'plunge-rate': 20.0,
            'retract-rate': 21.0,
            'safe-z': 1.0
        })
        self.assertFalse(cnc is None)
        
        self.assertEqual( cnc.config['cut-feedrate'], 50.0 )
        self.assertEqual( cnc.config['travel-feedrate'], 100.0 )
        self.assertEqual( cnc.config['plunge-rate'], 20.0 )
        self.assertEqual( cnc.config['retract-rate'], 21.0 )
        self.assertEqual( cnc.config['safe-z'], 1 )
        self.assertEqual( cnc.config['cut-depth'], 1 )
    
    def test_cnc25d_set_config(self):
        # Set initial config
        output, cnc = self.__default(config={
            'cut-feedrate': 1.0,
            'travel-feedrate': 10.0,
            'plunge-rate': 20.0,
            'retract-rate': 21.0,
            'safe-z': 1.0
        })
        self.assertFalse(cnc is None)
        
        # Set new config with missing fields
        config={
            'cut-feedrate': 50.0,
            'plunge-rate': 21.0,
            'retract-rate': 22.0,
            'safe-z': 3.0
        }
        
        cnc.setConfig(config)
        
        self.assertEqual( cnc.config['cut-feedrate'], 50.0 )
        self.assertNotEqual( cnc.config['travel-feedrate'], 10.0 )
        self.assertEqual( cnc.config['travel-feedrate'], 100.0 )
        self.assertEqual( cnc.config['plunge-rate'], 21.0 )
        self.assertEqual( cnc.config['retract-rate'], 22.0 )
        self.assertEqual( cnc.config['safe-z'], 3 )
        self.assertEqual( cnc.config['cut-depth'], 1 )
    
    def test_cnc25d_update_config(self):
        # Set initial config
        output, cnc = self.__default(config={
            'cut-feedrate': 1.0,
            'travel-feedrate': 10.0,
            'plunge-rate': 20.0,
            'retract-rate': 21.0,
            'safe-z': 1.0
        })
        self.assertFalse(cnc is None)
        
        config={
            'cut-feedrate': 50.0,
            'plunge-rate': 21.0,
            'retract-rate': 22.0,
            'safe-z': 3.0
        }
        
        cnc.updateConfig(config)
        
        self.assertEqual( cnc.config['cut-feedrate'], 50.0 )
        self.assertEqual( cnc.config['travel-feedrate'], 10.0 )
        self.assertEqual( cnc.config['plunge-rate'], 21.0 )
        self.assertEqual( cnc.config['retract-rate'], 22.0 )
        self.assertEqual( cnc.config['safe-z'], 3 )
        self.assertEqual( cnc.config['cut-depth'], 1 )
    
    def test_cnc25d_set_cut_depth(self):
        output, cnc = self.__default()
        
        self.assertEqual( cnc.config['cut-depth'], 1 )
        
        cnc.setCutDepth(2.5)
        self.assertEqual( cnc.config['cut-depth'], 2.5 )
        self.assertEqual( cnc.getCutDepth(), 2.5 )
        
        cnc.setCutDepth(20.5)
        self.assertEqual( cnc.config['cut-depth'], 20.5 )
        self.assertEqual( cnc.getCutDepth(), 20.5 )
        
if __name__ == '__main__':
    unittest.main()
