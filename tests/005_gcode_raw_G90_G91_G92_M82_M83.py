import unittest
from unittest import TestCase
#from mock import patch, Mock, PropertyMock

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    # G90
    def test_G90(self):
        self.__default_config()
        
        self.raw.G90()
        self.assertEqual(str(self.output), 'G90')
        
    # G91
    def test_G91(self):
        self.__default_config()
        
        self.raw.G91()
        self.assertEqual(str(self.output), 'G91')
        
    # G92
    def test_G92(self):
        self.__default_config()
        
        self.raw.G92()
        self.assertEqual(str(self.output), 'G92')
        
    def test_G92_x(self):
        self.__default_config()
        
        self.raw.G92(10)
        self.assertEqual(str(self.output), 'G92 X10')
        
    def test_G92_x_param(self):
        self.__default_config()
        
        self.raw.G92(X=10)
        self.assertEqual(str(self.output), 'G92 X10')
        
    def test_G92_xy(self):
        self.__default_config()
        
        self.raw.G92(10, 11)
        self.assertEqual(str(self.output), 'G92 X10 Y11')
        
    def test_G92_y_param(self):
        self.__default_config()
        
        self.raw.G92(Y=11)
        self.assertEqual(str(self.output), 'G92 Y11')
        
    def test_G92_xyz(self):
        self.__default_config()
        
        self.raw.G92(10, 11, 12)
        self.assertEqual(str(self.output), 'G92 X10 Y11 Z12')
        
    def test_G92_z_param(self):
        self.__default_config()
        
        self.raw.G92(Z=10)
        self.assertEqual(str(self.output), 'G92 Z10')
        
    def test_G92_e_param(self):
        self.__default_config()
        
        self.raw.G92(E=13)
        self.assertEqual(str(self.output), 'G92 E13')
        
    def test_G92_xyz_param(self):
        self.__default_config()
        
        self.raw.G92(Z=12, Y=11, X=10)
        self.assertEqual(str(self.output), 'G92 X10 Y11 Z12')
        
    def test_G92_xyz_compact(self):
        self.__default_config(compact=True)
        
        self.raw.G92(Z=12, Y=11, X=10)
        self.assertEqual(str(self.output), 'G92X10Y11Z12')
        
    def test_G92_xyze_compact(self):
        self.__default_config(compact=True)
        
        self.raw.G92(Z=12, Y=11, X=10, E=13)
        self.assertEqual(str(self.output), 'G92X10Y11Z12E13')
        
    def test_G92_x_zero(self):
        self.__default_config()
        
        self.raw.G92(X=0.0)
        self.assertEqual(str(self.output), 'G92 X0')
        
    def test_G92_y_zero(self):
        self.__default_config()
        
        self.raw.G92(Y=0.0)
        self.assertEqual(str(self.output), 'G92 Y0')
        
    def test_G92_z_zero(self):
        self.__default_config()
        
        self.raw.G92(Z=0.0)
        self.assertEqual(str(self.output), 'G92 Z0')
        
    def test_G92_e_zero(self):
        self.__default_config()
        
        self.raw.G92(E=0.0)
        self.assertEqual(str(self.output), 'G92 E0')
        
    def test_G92_all_zero(self):
        self.__default_config()
        
        self.raw.G92(X=0.0, Y=0.0, Z=0.0, E=0.0)
        self.assertEqual(str(self.output), 'G92')

    def test_M82(self):
        self.__default_config()
        
        self.raw.M82()
        self.assertEqual(str(self.output), 'M82')

    def test_M83(self):
        self.__default_config()
        
        self.raw.M83()
        self.assertEqual(str(self.output), 'M83')
        
if __name__ == '__main__':
    unittest.main()
