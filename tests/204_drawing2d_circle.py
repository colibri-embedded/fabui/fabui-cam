import unittest
from unittest import TestCase
from math import sqrt

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        self.ERROR_MARGIN = 0.0000000001
        
    def test_add_circle(self):
        d = Drawing2D()
        
        p_center = (0,1)
        p_r = 10.0
        
        bound = d.addCircle(p_center, p_r)
        self.assertEqual( bound, (-10.0, -9.0, 20.0, 20.0) )
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'circle' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )
        
        pts = prim['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - p_center[0])
            dy = float(pt[1] - p_center[1])
            
            r = sqrt( dx * dx + dy*dy)
            error = abs( r  - p_r)
            self.assertTrue( error < self.ERROR_MARGIN )
        
    def test_add_circle_param(self):
        d = Drawing2D()
        
        p_center = (0,1)
        p_r = 10.0
        
        bound = d.addCircle(radius=p_r, center=p_center)
        self.assertEqual( bound, (-10.0, -9.0, 20.0, 20.0) )
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'circle' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )
        
        pts = prim['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - p_center[0])
            dy = float(pt[1] - p_center[1])
            
            r = sqrt( dx * dx + dy*dy)
            error = abs( r  - p_r)
            self.assertTrue( error < self.ERROR_MARGIN )
        
    def test_add_circle_custom_layer(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("Second")
        
        p_center = (0,1)
        p_r = 10.0
        
        d.addCircle(p_center, p_r, layer=idx)
        
        self.assertEqual( len(d.layers[idx].primitives), 1 )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'circle' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )

    def test_add_circle_missing_layer_error(self):
        d = Drawing2D()
        
        p_center = (0,1)
        p_r = 10.0
        
        with self.assertRaises(MissingLayerError) as context:
            d.addCircle(p_center, p_r, layer=5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))

if __name__ == '__main__':
    unittest.main()
