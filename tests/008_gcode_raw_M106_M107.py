import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_M106_s(self):
        self.__default_config()
        
        self.raw.M106(255)
        self.assertEqual(str(self.output), 'M106 S255')
        
    def test_M106_s_param(self):
        self.__default_config()
        
        self.raw.M106(S=255)
        self.assertEqual(str(self.output), 'M106 S255')
        
    def test_M106_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M106(S=255)
        self.assertEqual(str(self.output), 'M106S255')
        
    def test_M106_pwm_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M106(RAW.PWM_MAX + 10)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_M106_temp_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M106(RAW.PWM_MIN - 10)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_M107(self):
        self.__default_config()
        
        self.raw.M107()
        self.assertEqual(str(self.output), 'M107')

        
if __name__ == '__main__':
    unittest.main()
