import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.cnc25d import CNC25D

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestCNC25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = CNC25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
    
    def test_cnc25d_mill_polyline(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        points = [(10, 11), (15,16), (20,9), (10, 11)]
        
        cnc.millPolyline(points)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100|G1Z-1F10|G1X15Y16F30|G1X20Y9|G1X10Y11' )
    
    def test_cnc25d_mill_polyline_reverse(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        points = [(10, 11), (15,16), (20,9), (10, 11)]
        
        cnc.millPolyline(points, True)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100|G1Z-1F10|G1X20Y9F30|G1X15Y16|G1X10Y11' )
    
    def test_cnc25d_mill_polyline_reverse_param(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        p_points = [(10, 11), (15,16), (20,9), (10, 11)]
        p_reverse = True
        
        cnc.millPolyline(points=p_points, reverse=p_reverse)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100|G1Z-1F10|G1X20Y9F30|G1X15Y16|G1X10Y11' )
        
    def test_cnc25d_mill_polyline_cut_depth(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        p_points = [(10, 11), (15,16), (20,9), (10, 11)]
        
        cnc.setCutDepth(1.0)
        cnc.millPolyline(p_points)
        
        cnc.setCutDepth(2.0)
        cnc.millPolyline(p_points)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100|G1Z-1F10|G1X15Y16F30|G1X20Y9|G1X10Y11|G1Z-2F10|G1X15Y16F30|G1X20Y9|G1X10Y11' )
        
    def test_cnc25d_mill_polyline_travel(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        p_points = [(10, 11), (15,16), (20,9), (10, 11)]
        p_points2 = [(100, 110), (150,160), (200,90), (100, 110)]
        
        cnc.setCutDepth(1.0)
        cnc.millPolyline(p_points)
        
        cnc.setCutDepth(2.0)
        cnc.millPolyline(p_points2)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100|G1Z-1F10|G1X15Y16F30|G1X20Y9|G1X10Y11|G0Z5F20|G0X100Y110F100|G1Z-2F10|G1X150Y160F30|G1X200Y90|G1X100Y110' )

        
if __name__ == '__main__':
    unittest.main()
