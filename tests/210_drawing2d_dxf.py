import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

try:
    import shapely
    use_shapely = True
except ImportError:
    use_shapely = False

class TestDrawing2D(TestCase):
    
    def setUp(self):
        self.DXF_LIBRECAD_PRIMITIVES = './data/dxf/librecad_primitives.dxf'
    
    def __load_primitives(self, application = 'librecad', layer='0'):
        d = Drawing2D()
        
        result = d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        self.assertTrue( result )
        
        default_layer = d.getLayerByName(layer)
        self.assertFalse(default_layer is None)
        
        return d, default_layer
    
    def test_librecad_dxf_load(self):
        d = Drawing2D()
        
        result = d.loadFromDXF(self.DXF_LIBRECAD_PRIMITIVES)
        self.assertTrue( result )
        
        self.assertEqual( len(d.layers), 4 )
        
        default_layer = d.getLayerByName('0')
        self.assertFalse(default_layer is None)
        
        self.assertEqual( len(default_layer.primitives), 7 )
        self.assertEqual( default_layer.name, '0' )
        self.assertEqual( default_layer.color, Drawing2D.WHITE )
        
        second_layer = d.getLayerByName('Second')
        self.assertFalse(second_layer is None)
        
        self.assertEqual( len(second_layer.primitives), 1 )
        self.assertEqual( second_layer.name, 'Second' )
        self.assertEqual( second_layer.color, Drawing2D.RED )
        
        found = {}
        for prim in default_layer.primitives:
            if prim['type'] not in found:
                found[ prim['type'] ] = 1
            else:
                found[ prim['type'] ] += 1
        
        self.assertEqual( found, {
            'spline': 1, 
            'ellipse': 1, 
            'arc': 2, 
            'polyline': 1, 
            'circle': 1, 
            'line': 1})
            
    def test_librecad_dxf_load_error(self):
        d = Drawing2D()
        
        result = d.loadFromDXF('./data/dxf/librecad_corrupted.dxf')
        self.assertFalse( result )
        
    def test_librecad_dxf_line(self):
        d, default_layer = self.__load_primitives()
        
        found = False

        for prim in default_layer.primitives:
            if prim['type'] == 'line':
                found = True
                pts = prim['points']
                self.assertEqual(pts[0], (-10.0, 10.0))
                self.assertEqual(pts[1], ( 10.0, 10.0))
                break
                
        self.assertTrue(found)
        
    def test_librecad_dxf_polyline(self):
        d, default_layer = self.__load_primitives()
        
        found = False

        for prim in default_layer.primitives:
            if prim['type'] == 'polyline':
                found = True
                self.assertEqual( prim['filled'], False )
                self.assertEqual( prim['closed'], False )
                pts = prim['points']
                self.assertEqual(pts,  [(-20.0, 0.0), (-20.0, 20.0), (20.0, 20.0), (20.0, 0.0)] )
                break
                
        self.assertTrue(found)
    
    def test_librecad_dxf_spline(self):
        d, default_layer = self.__load_primitives()
        
        found = False

        for prim in default_layer.primitives:
            if prim['type'] == 'spline':
                found = True
                self.assertEqual( prim['knots'], [0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0] )
                self.assertEqual( prim['control_points'], [(-20.0, 0.0, 0.0), (0.0, -10.0, 0.0), (20.0, 0.0, 0.0), (20.0, 0.0, 0.0)] )
                self.assertEqual( prim['degree'], 3 )
                break
                
        self.assertTrue(found)
        
    def test_librecad_dxf_circle(self):
        d, default_layer = self.__load_primitives()
        
        found = False
        
        for prim in default_layer.primitives:
            if prim['type'] == 'circle':
                found = True
                self.assertEqual( prim['center'], (0,0) )
                self.assertEqual( prim['radius'], 10.0 )
                self.assertEqual( prim['filled'], False )
                break
                
        self.assertTrue(found)
        
    def test_librecad_dxf_ellipse(self):
        d, default_layer = self.__load_primitives()
        
        found = False
        
        for prim in default_layer.primitives:
            if prim['type'] == 'ellipse':
                found = True
                self.assertEqual( prim['center'], (0,0) )
                self.assertEqual( prim['major_axis'], (20.0, 0.0) )
                self.assertEqual( prim['ratio'],  0.5 )
                self.assertEqual( prim['end'],    360.0 )
                self.assertEqual( prim['start'],  0.0 )
                self.assertEqual( prim['filled'], False )
                break
                
        self.assertTrue(found)
    
    @unittest.skip("Not yet implemented")
    def test_librecad_dxf_mtext(self):
        d, default_layer = self.__load_primitives()
    
    @unittest.skip("Not yet implemented")
    def test_librecad_dxf_text(self):
        d, default_layer = self.__load_primitives()
        
if __name__ == '__main__':
    unittest.main()
