import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.writerbase import WriterBase
from fabui.cam.gcode.filewriter import FileWriter
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.toolpath.advanced25dplanner import Advanced25DPlanner

class OneWriteOutput(WriterBase):
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestAdvanced25DPlanner(TestCase):
    
    def setUp(self):
        print
        pass
    
    def __default(self, config={}, ndigits=3, compact=True, no_comment=False, output=None):
        if output is None:
            output = OneWriteOutput()
        planner = Advanced25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        return output, planner
        
    def __default_config(self, layer_name="0", optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "zero_cut": False
            },
            "layers": [
                {
                    "name": layer_name,
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 1,
                        "steps": 1,
                        "sequential": False
                    }
                }
            ]
        }

    def __advanced_config(self, optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "zero_cut": False
            },
            "layers": [
                {
                    "name": "pockets",
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "cut": {
                        "type": "pocket",
                        "depth": 1,
                        "steps": 1,
                        "passes": 1,
                        "sequential": False
                    }
                }
            ]
        }
        
    # def test_advanced_planner_new_object(self):
    #     output, planner = self.__default(compact=True)
        
    #     self.assertFalse(planner is None)

    # def test_advanced_plan(self):
    #     config = self.__default_config(optimize=0)
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)

    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')

    #     planner.plan(drawing)

    # def test_advanced_plan2(self):
    #     config = self.__default_config(optimize=0)
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)

    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')

    #     planner.plan(drawing)

    # def test_advanced_plan3(self):
    #     config = self.__default_config(optimize=0)
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)

    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_simple_lines2.dxf')

    #     planner.plan(drawing)

    # def test_advanced_plan4(self):
    #     config = self.__default_config(optimize=0, layer_name="Default")
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)

    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/dxf_default.dxf')

    #     planner.plan(drawing)

    def test_advanced_plan5(self):
        config = self.__advanced_config(optimize=1)

        fw = FileWriter('/tmp/output.gcode')

        output, planner = self.__default(config=config, compact=True, no_comment=True, output=fw)

        drawing = Drawing2D()
        drawing.loadFromDXF('./data/dxf/librecad_advanced25d_1.dxf')

        planner.plan(drawing)

    # def test_advanced_planner_optimize0(self):
    #     config = self.__default_config(optimize=0)
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        
    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')
        
    #     planner.plan(drawing)
        
    #     self.assertEqual( str(output), 'G1Z-1F10|G1Y100F100|G0Z5F20|G0X20F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X60Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X100Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X80Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X40Y100F100|G1Z-1F10|G1Y0F100' )

    # def test_advanced_planner_optimize1(self):
    #     config = self.__default_config(optimize=1)
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)
        
        
    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')
        
    #     planner.plan(drawing)
        
    #     self.assertEqual( str(output), 'G1Z-1F10|G1Y100F100|G0Z5F20|G0X20F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X40Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X60Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X80Y100F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X100Y100F100|G1Z-1F10|G1Y0F100' )

    # def test_advanced_planner_optimize2(self):
    #     config = self.__default_config(optimize=2)
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)
        
    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_simple_lines.dxf')
        
    #     planner.plan(drawing)
        
    #     self.assertEqual( str(output), 'G1Z-1F10|G1Y100F100|G0Z5F20|G0X20F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X40F100|G1Z-1F10|G1Y100F100|G0Z5F20|G0X60F100|G1Z-1F10|G1Y0F100|G0Z5F20|G0X80F100|G1Z-1F10|G1Y100F100|G0Z5F20|G0X100F100|G1Z-1F10|G1Y0F100' )
    
    # def test_advanced_planner_finish(self):
    #     config = self.__default_config()
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)
        
    #     drawing = Drawing2D()
    #     layer = drawing.getLayerByName('Default')
    #     layer.name = '0'
    #     drawing.addLine( (1.0, 2.1), (3.2, 4.3) )
        
    #     planner.plan(drawing)
    #     planner.finish()
    
    #     self.assertEqual( str(output), 'G0Z5F20|G0X1Y2.1F100|G1Z-1F10|G1X3.2Y4.3F100|G0Z5F20|G0X0Y0F100|M400')
        
    # def test_advanced_planner_cut(self):
    #     config = self.__default_config()
    #     config['layers'][0]['cut']['depth'] = 3
    #     config['layers'][0]['cut']['steps'] = 2
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)
        
    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
    #     planner.plan(drawing)
    #     planner.finish()
        
    #     self.assertEqual( str(output), 'G0Z5F20|G0X-50Y-30F100|G1Z-1.5F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G1Z-3F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G0Z5F20|G0X0Y0F100|M400')
        
    # def test_advanced_planner_cut_w_zerocut(self):
    #     config = self.__default_config()
    #     config['layers'][0]['cut']['depth'] = 3
    #     config['layers'][0]['cut']['steps'] = 2
    #     config['general']['zero_cut'] = True
    #     output, planner = self.__default(config=config, compact=True, no_comment=True)
        
    #     drawing = Drawing2D()
    #     drawing.loadFromDXF('./data/dxf/librecad_triangle.dxf')
        
    #     planner.plan(drawing)
    #     planner.finish()
        
    #     self.assertEqual( str(output), 'G0Z5F20|G0X-50Y-30F100|G1Z0F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G1Z-1.5F10|G1X50F100|G1X0Y56.603|G1X-50Y-30|G0Z5F20|G0X0Y0F100|M400')
        
if __name__ == '__main__':
    unittest.main()
