import unittest
from unittest import TestCase
#from mock import patch, Mock, PropertyMock

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    # G27
    def test_G27(self):
        self.__default_config()
        
        self.raw.G27()
        self.assertEqual(str(self.output), 'G27')
        
    def test_G27_x(self):
        self.__default_config()
        
        self.raw.G27(True)
        self.assertEqual(str(self.output), 'G27 X')
        
    def test_G27_x_param(self):
        self.__default_config()
        
        self.raw.G27(X=True)
        self.assertEqual(str(self.output), 'G27 X')
        
    def test_G27_y_param(self):
        self.__default_config()
        
        self.raw.G27(Y=True)
        self.assertEqual(str(self.output), 'G27 Y')
        
    def test_G27_z_param(self):
        self.__default_config()
        
        self.raw.G27(Z=True)
        self.assertEqual(str(self.output), 'G27 Z')
        
    def test_G27_xy(self):
        self.__default_config()
        
        self.raw.G27(True, True)
        self.assertEqual(str(self.output), 'G27 X Y')
        
    def test_G27_xy_param(self):
        self.__default_config()
        
        self.raw.G27(X=True, Y=True)
        self.assertEqual(str(self.output), 'G27 X Y')
        
    def test_G27_xyz(self):
        self.__default_config()
        
        self.raw.G27(True, True, True)
        self.assertEqual(str(self.output), 'G27')
        
    def test_G27_xyz_param(self):
        self.__default_config()
        
        self.raw.G27(X=True, Y=True, Z=True)
        self.assertEqual(str(self.output), 'G27')
    
    # G28
    def test_G28(self):
        self.__default_config()
        
        self.raw.G28()
        self.assertEqual(str(self.output), 'G28')
        
    def test_G28_x(self):
        self.__default_config()
        
        self.raw.G28(True)
        self.assertEqual(str(self.output), 'G28 X')
        
    def test_G28_x_param(self):
        self.__default_config()
        
        self.raw.G28(X=True)
        self.assertEqual(str(self.output), 'G28 X')
        
    def test_G28_y_param(self):
        self.__default_config()
        
        self.raw.G28(Y=True)
        self.assertEqual(str(self.output), 'G28 Y')
        
    def test_G28_z_param(self):
        self.__default_config()
        
        self.raw.G28(Z=True)
        self.assertEqual(str(self.output), 'G28 Z')
        
    def test_G28_xy(self):
        self.__default_config()
        
        self.raw.G28(True, True)
        self.assertEqual(str(self.output), 'G28 X Y')
        
    def test_G28_xy_param(self):
        self.__default_config()
        
        self.raw.G28(X=True, Y=True)
        self.assertEqual(str(self.output), 'G28 X Y')
        
    def test_G28_xyz(self):
        self.__default_config()
        
        self.raw.G28(True, True, True)
        self.assertEqual(str(self.output), 'G28')
        
    def test_G28_xyz_param(self):
        self.__default_config()
        
        self.raw.G28(X=True, Y=True, Z=True)
        self.assertEqual(str(self.output), 'G28')

    # G29
    def test_G29(self):
        self.__default_config()
        
        self.raw.G29()
        self.assertEqual(str(self.output), 'G29')
        
    # G30
    def test_G30(self):
        self.__default_config()
        
        self.raw.G30()
        self.assertEqual(str(self.output), 'G30')
        
    def test_G30_u(self):
        self.__default_config()
        
        self.raw.G30(300.5)
        self.assertEqual(str(self.output), 'G30 U300.5')
        
    def test_G30_u_param(self):
        self.__default_config()
        
        self.raw.G30(U=300.5)
        self.assertEqual(str(self.output), 'G30 U300.5')
        
    def test_G30_ud(self):
        self.__default_config()
        
        self.raw.G30(300.5, 200.5)
        self.assertEqual(str(self.output), 'G30 U300.5 D200.5')
        
    def test_G30_d_param(self):
        self.__default_config()
        
        self.raw.G30(D=200.5)
        self.assertEqual(str(self.output), 'G30 D200.5')
        
    def test_G30_ud_compact(self):
        self.__default_config(compact=True)
        
        self.raw.G30(U=300.5, D=200.5)
        self.assertEqual(str(self.output), 'G30U300.5D200.5')
    
    # G38
    def test_G38(self):
        self.__default_config()
        
        self.raw.G38()
        self.assertEqual(str(self.output), 'G38')
        
    def test_G38_s(self):
        self.__default_config()
        
        self.raw.G38(300.5)
        self.assertEqual(str(self.output), 'G38 S300.5')
        
    def test_G38_s_param(self):
        self.__default_config()
        
        self.raw.G38(S=300.5)
        self.assertEqual(str(self.output), 'G38 S300.5')
        
    def test_G38_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.G38(S=300.5)
        self.assertEqual(str(self.output), 'G38S300.5')
        
    def test_M401(self):
        self.__default_config()
        
        self.raw.M401()
        self.assertEqual(str(self.output), 'M401')
        
    def test_M402(self):
        self.__default_config()
        
        self.raw.M402()
        self.assertEqual(str(self.output), 'M402')
        
    def test_M746_s(self):
        self.__default_config()
        
        self.raw.M746(0)
        self.assertEqual(str(self.output), 'M746 S0')
        
    def test_M746_s_param(self):
        self.__default_config()
        
        self.raw.M746(S=0)
        self.assertEqual(str(self.output), 'M746 S0')
        
    def test_M746_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M746(S=0)
        self.assertEqual(str(self.output), 'M746S0')
        
    def test_M746_s_too_high_error(self):
        self.__default_config(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M746(S=RAW.PROBE_ID_MAX +1)
        
        self.assertTrue('out of allowed range' in str(context.exception))
        
    def test_M746_s_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M746(S=RAW.PROBE_ID_MIN -1)
        
        self.assertTrue('out of allowed range' in str(context.exception))
        
if __name__ == '__main__':
    unittest.main()
