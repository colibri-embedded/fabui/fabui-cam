import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False):
        self.cnc.useProbe = use_probe
        self.output.clear()
        
    def __move_axis(self, x = 0.0, y = 0.0, z = 0.0, e = 0.0):
        self.cnc.curX = x
        self.cnc.curY = y
        self.cnc.curZ = z
        self.cnc.curE = e

    # X
    def test_at_x_int(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(X=0) )

    def test_at_x_int_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(X=10) )
        
    def test_at_x_float(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(X=0.0) )

    def test_at_x_float_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(X=10.1) )
        
    def test_at_x_ndigits(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(X=0.0001) )
        
    def test_at_x_ndigits_error(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(X=0.0005) )

    # Y
    def test_at_y_int(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(Y=0) )

    def test_at_y_int_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(Y=10) )
        
    def test_at_y_float(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(Y=0.0) )
        
    def test_at_y_float_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(Y=10.1) )
        
    def test_at_y_ndigits(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(Y=0.0001) )
        
    def test_at_y_ndigits_error(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(Y=0.0005) )

    # Z
    def test_at_z_int(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(Z=0) )

    def test_at_z_int_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(Z=10) )
        
    def test_at_z_float(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(Z=0.0) )
        
    def test_at_z_float_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(Z=10.1) )
        
    def test_at_z_ndigits(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(Z=0.0001) )
        
    def test_at_z_ndigits_error(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(Z=0.0005) )
        
    # E
    def test_at_e_int(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(E=0) )

    def test_at_e_int_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(E=10) )
        
    def test_at_e_float(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(E=0.0) )
        
    def test_at_e_float_false(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(E=10.1) )
        
    def test_at_e_ndigits(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertTrue( self.cnc.isAt(E=0.0001) )
        
    def test_at_e_ndigits_error(self):
        self.__default_config()
        
        self.__move_axis()
        self.assertFalse( self.cnc.isAt(E=0.0005) )
        
    # XY
    def test_at_xy_int(self):
        self.__default_config()
        
        self.__move_axis(5.0, 6.0)
        self.assertTrue( self.cnc.isAt(5,6) )
        
    def test_at_xy_int_false(self):
        self.__default_config()
        
        self.__move_axis(5.0, 6.0)
        self.assertFalse( self.cnc.isAt(6,7) )
        
    def test_at_xy_float(self):
        self.__default_config()
        
        self.__move_axis(5.123, 6.456)
        self.assertTrue( self.cnc.isAt(5.123,6.456) )
        
    def test_at_xy_float_false(self):
        self.__default_config()
        
        self.__move_axis(4.123, 6.456)
        self.assertFalse( self.cnc.isAt(5.123,6.456) )
        
    # XYZ
    def test_at_xyz_int(self):
        self.__default_config()
        
        self.__move_axis(5.0, 6.0, 7.0)
        self.assertTrue( self.cnc.isAt(5,6,7) )
        
    def test_at_xyz_int_false(self):
        self.__default_config()
        
        self.__move_axis(5.0, 6.0, 7.0)
        self.assertFalse( self.cnc.isAt(5,8,7) )
        
    def test_at_xyz_float(self):
        self.__default_config()
        
        self.__move_axis(5.123, 6.456, 7.789)
        self.assertTrue( self.cnc.isAt(5.123,6.456,7.789) )
        
    def test_at_xyz_float_false(self):
        self.__default_config()
        
        self.__move_axis(5.123, 6.456, 7.789)
        self.assertFalse( self.cnc.isAt(5.123,6.458,7.789) )
        
    # XYZE
    def test_at_xyze_int(self):
        self.__default_config()
        
        self.__move_axis(5.0, 6.0, 7.0, 8.0)
        self.assertTrue( self.cnc.isAt(5,6,7, 8) )
        
    def test_at_xyze_int_false(self):
        self.__default_config()
        
        self.__move_axis(5.0, 6.0, 7.0, 8.0)
        self.assertFalse( self.cnc.isAt(5,8,7,9) )
        
    def test_at_xyze_float(self):
        self.__default_config()
        
        self.__move_axis(5.123, 6.456, 7.789, 8.024)
        self.assertTrue( self.cnc.isAt(5.123,6.456,7.789,8.024) )
        
    def test_at_xyze_float_false(self):
        self.__default_config()
        
        self.__move_axis(5.123, 6.456, 7.789, 8.024)
        self.assertFalse( self.cnc.isAt(5.123,6.458,7.789,8.04) )
        
if __name__ == '__main__':
    unittest.main()
