import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False, xyzMode=CNC.eABSOLUTE, eMode=CNC.eABSOLUTE):
        self.cnc.useProbe = use_probe
        self.cnc.xyzMode = xyzMode
        self.cnc.eMode = eMode
        self.output.clear()
        
    def __move_axis(self, x = 0.0, y = 0.0, z = 0.0, e = 0.0, f = 100):
        self.cnc.curX = x
        self.cnc.curY = y
        self.cnc.curZ = z
        self.cnc.curE = e
        self.cnc.config['feedrate'] = f
        self.cnc.update_feedrate = False

    def test_comment(self):
        self.__default_config()
        self.cnc.comment("Test comment")
        self.assertEqual(str(self.output), '; Test comment')
        
    # Wait
    def test_wait_no_param_error(self):
        self.__default_config()
        
        with self.assertRaises(TypeError) as context:
            self.cnc.wait()
        
        self.assertTrue("arguments (1 given)" in str(context.exception))
        
    def test_wait_s(self):
        self.__default_config()
        
        self.cnc.wait(10)
        self.assertEqual(str(self.output), 'G4 S10')

    # Sync
    def test_sync(self):
        self.__default_config()
        
        self.cnc.sync()
        self.assertEqual(str(self.output), 'M400')
        
    def test_finish_all_moves(self):
        self.__default_config()
        
        self.cnc.finishAllMoves()
        self.assertEqual(str(self.output), 'M400')
    
    # Get current position
    def test_get_current_position_default(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition()
        self.assertEqual( pos, (1,2,3) )
        
    def test_get_current_position_x(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition(CNC.X_AXIS)
        self.assertEqual( pos, 1 )
        
    def test_get_current_position_y(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition(CNC.Y_AXIS)
        self.assertEqual( pos, 2 )
        
    def test_get_current_position_z(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition(CNC.Z_AXIS)
        self.assertEqual( pos, 3 )
        
    def test_get_current_position_e(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition(CNC.E_AXIS)
        self.assertEqual( pos, 4 )
        
    def test_get_current_position_xy(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition(CNC.XY_AXIS)
        self.assertEqual( pos, (1,2) )
        
    def test_get_current_position_all(self):
        self.__default_config()
        
        self.__move_axis(1,2,3,4)
        pos = self.cnc.getCurrentPosition(CNC.ALL_AXIS)
        self.assertEqual( pos, (1,2,3,4) )
        
    # Get/set feedrate
    def test_get_feedrate(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        feedrate = self.cnc.getFeedrate()
        self.assertEqual( feedrate, 200 )
        
    def test_get_feedrate_after_move(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.moveTo(10,11, F=300)
        feedrate = self.cnc.getFeedrate()
        self.assertEqual( feedrate, 300 )
        
    def test_set_new_feedrate_moveto(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.setFeedrate(300)
        self.cnc.moveTo(10, 11)
        self.assertEqual( str(self.output), 'G1 X10 Y11 F300' )
        
    def test_set_old_feedrate_moveto(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.setFeedrate(200)
        self.cnc.moveTo(10, 11)
        self.assertEqual( str(self.output), 'G1 X10 Y11' )
        
    def test_set_new_feedrate_moveby(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.setFeedrate(300)
        self.cnc.moveBy(10, 11)
        self.assertEqual( str(self.output), 'G1 X10 Y11 F300' )
        
    def test_set_old_feedrate_moveby(self):
        self.__default_config()
        
        self.__move_axis(f=200)
        self.cnc.setFeedrate(200)
        self.cnc.moveBy(10, 11)
        self.assertEqual( str(self.output), 'G1 X10 Y11' )
        
if __name__ == '__main__':
    unittest.main()
