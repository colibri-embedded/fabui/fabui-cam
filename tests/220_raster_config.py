import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.raster import Raster

class TestRaster(TestCase):
    
    def setUp(self):
        pass
    
    def test_raster_default_config(self):
        r = Raster()
        
        self.assertFalse( r is None )
        self.assertEqual( r.config['dpm'], 10 )
        self.assertEqual( r.config['invert'], False)
        self.assertEqual( r.config['width'], 100.0)
        self.assertEqual( r.config['height'], None)
    
    def test_raster_custom_width(self):
        r = Raster(width=50)
        
        self.assertFalse( r is None )
        self.assertEqual( r.config['dpm'], 10 )
        self.assertEqual( r.config['invert'], False)
        self.assertEqual( r.config['width'], 50.0)
        self.assertEqual( r.config['height'], None)
    
    def test_raster_custom_width(self):
        r = Raster(height=50)
        
        self.assertFalse( r is None )
        self.assertEqual( r.config['dpm'], 10 )
        self.assertEqual( r.config['invert'], False)
        self.assertEqual( r.config['width'], None)
        self.assertEqual( r.config['height'], 50)

    def test_raster_custom_invert(self):
        r = Raster(invert=True)
        
        self.assertFalse( r is None )
        self.assertEqual( r.config['dpm'], 10 )
        self.assertEqual( r.config['invert'], True)
        self.assertEqual( r.config['width'], 100.0)
        self.assertEqual( r.config['height'], None)

    def test_raster_custom_dpm(self):
        r = Raster(dpm=20)
        
        self.assertFalse( r is None )
        self.assertEqual( r.config['dpm'], 20 )
        self.assertEqual( r.config['invert'], False)
        self.assertEqual( r.config['width'], 100.0)
        self.assertEqual( r.config['height'], None)

    def test_raster_custom_width_n_height(self):
        r = Raster(width=50, height=30)
        
        self.assertFalse( r is None )
        self.assertEqual( r.config['dpm'], 10 )
        self.assertEqual( r.config['invert'], False)
        self.assertEqual( r.config['width'], 50)
        self.assertEqual( r.config['height'], None)
        
if __name__ == '__main__':
    unittest.main()
