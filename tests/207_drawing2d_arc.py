import unittest
from unittest import TestCase
from math import sqrt

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        self.ERROR_MARGIN = 0.0000000001

    def __point_diff(self, a, b):
        dx = abs(a[0] - b[0])
        dy = abs(a[1] - b[1])
        
        return sqrt(dx*dx + dy*dy)

    def test_add_arc_90(self):
        d = Drawing2D()
        
        p_center = (0,0)
        p_r = 10.0
        p_start = 0.0
        p_end = 90.0
        
        x,y,w,h = d.addArc(p_center, p_r, p_start, p_end)
        self.assertEqual( w, p_r )
        self.assertEqual( h, p_r )
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'arc' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )
        
        pts = prim['points']
        self.assertTrue( self.__point_diff(pts[0],  ( p_r + p_center[0], p_center[1])) < self.ERROR_MARGIN )
        self.assertTrue( self.__point_diff(pts[-1], ( p_center[0], p_r + p_center[1])) < self.ERROR_MARGIN )
        
        pts = prim['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - p_center[0])
            dy = float(pt[1] - p_center[1])
            
            r = sqrt( dx * dx + dy*dy)
            error = abs( r  - p_r)
            self.assertTrue( error < self.ERROR_MARGIN )

    def test_add_arc_180(self):
        d = Drawing2D()
        
        p_center = (0,1)
        p_r = 10.0
        p_start = 0.0
        p_end = 180.0
        
        x,y,w,h = d.addArc(p_center, p_r, p_start, p_end)
        self.assertEqual( w, p_r*2 )
        self.assertEqual( h, p_r )
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'arc' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )
        
        pts = prim['points']
        self.assertTrue( self.__point_diff(pts[0],  ( p_r + p_center[0], p_center[1])) < self.ERROR_MARGIN )
        self.assertTrue( self.__point_diff(pts[-1], (-p_r + p_center[0], p_center[1])) < self.ERROR_MARGIN )
        
        pts = prim['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - p_center[0])
            dy = float(pt[1] - p_center[1])
            
            r = sqrt( dx * dx + dy*dy)
            error = abs( r  - p_r)
            self.assertTrue( error < self.ERROR_MARGIN )
    
    def test_arc_param(self):
        d = Drawing2D()
        
        p_center = (0,1)
        p_r = 10.0
        p_start = 0.0
        p_end = 180.0
        
        d.addArc(radius=p_r, center=p_center, start=p_start, end=p_end, layer=0)
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'arc' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )
    
    def test_add_arc_custom_layer(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("Second")
        
        p_center = (0,1)
        p_r = 10.0
        p_start = 0.0
        p_end = 180.0
        
        d.addArc(p_center, p_r, p_start, p_end, layer=idx)
        self.assertEqual( len(d.layers[idx].primitives), 1 )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'arc' )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['radius'], p_r )

    def test_add_arc_missing_layer_error(self):
        d = Drawing2D()
        
        p_center = (0,1)
        p_r = 10.0
        p_start = 0.0
        p_end = 180.0
        
        with self.assertRaises(MissingLayerError) as context:
            idx = 5
            d.addArc(p_center, p_r, p_start, p_end, layer=idx)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))

if __name__ == '__main__':
    unittest.main()
