import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.writerbase import WriterBase
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.toolpath.simple25dplanner import Simple25DPlanner

class OneWriteOutput(WriterBase):
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestSimple25DPlanner(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=True, no_comment=False):
        output = OneWriteOutput()
        planner = Simple25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        return output, planner
        
    def __default_config(self, optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "zero_cut": False
            },
            "layers": [
                {
                    "name": "0",
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 1,
                        "steps": 1,
                        "sequential": False
                    }
                }
            ]
        }
        
    def test_simple_planner_value_map(self):
        output, planner = self.__default(compact=True)
        
        self.assertFalse(planner is None)
        
        self.assertEqual( planner.value_map['const'], planner.value_map_const )
        self.assertEqual( planner.value_map['linear'], planner.value_map_linear )

    def test_simple_planner_value_map_linear(self):
        output, planner = self.__default(compact=True)
        self.assertFalse(planner is None)
        
        cfg = {
            "in_min": 0,
            "in_max": 255,
            "out_min": 0,
            "out_max": 255
        }
        
        self.assertEqual( planner.value_map_linear(0, cfg), 0 )
        self.assertEqual( planner.value_map_linear(128, cfg), 128 )
        
        cfg = {
            "in_min": 0,
            "in_max": 255,
            "out_min": 0,
            "out_max": 100
        }
        
        self.assertEqual( planner.value_map_linear(0  , cfg), 0 )
        self.assertEqual( planner.value_map_linear(128, cfg), 50 )
        self.assertEqual( planner.value_map_linear(255, cfg), 100 )

    def test_simple_planner_value_map_linear_invert(self):
        output, planner = self.__default(compact=True)
        self.assertFalse(planner is None)
        
        cfg = {
            "in_min": 0,
            "in_max": 255,
            "out_min": 255,
            "out_max": 0
        }
        
        self.assertEqual( planner.value_map_linear(0  , cfg), 255 )
        self.assertEqual( planner.value_map_linear(255, cfg), 0 )
        
        cfg = {
            "in_min": 0,
            "in_max": 255,
            "out_min": 100,
            "out_max": 0
        }
        
        self.assertEqual( planner.value_map_linear(0  , cfg), 100 )
        self.assertEqual( planner.value_map_linear(128, cfg), 49 )
        self.assertEqual( planner.value_map_linear(255, cfg), 0 )
        
if __name__ == '__main__':
    unittest.main()
