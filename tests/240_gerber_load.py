import unittest
from unittest import TestCase
import numpy as np

from fabui.cam.loaders import gerber
from fabui.cam.loaders.gerber.render import ShapelyContext

from fabui.cam.gcode.writerbase import WriterBase
from fabui.cam.gcode.filewriter import FileWriter
from fabui.cam.common.drawing2d import Drawing2D
# from fabui.cam.toolpath.advanced25dplanner import Advanced25DPlanner
from fabui.cam.toolpath.simple25dplanner import Simple25DPlanner

import shapely.geometry as sg
# from shapely.ops import cascaded_union
# from shapely.ops import linemerge

# Unit under test

class TestGerberLoader(TestCase):
    
    def setUp(self):
        pass

    def __default(self, config={}, ndigits=3, compact=True, no_comment=False, output=None):
        planner = Advanced25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        return output, planner

    def __simple(self, config={}, ndigits=3, compact=True, no_comment=False, output=None):
        planner = Simple25DPlanner(output=output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        return output, planner
        
    def __advanced_config(self, layer_name="0", optimize=0):
        return {
            "version": 2,
            "general": {
                "optimize": optimize,
                "zero_cut": False,
                "plunge_rate": 50,
                "retract_rate": 80,
                "travel_rate": 200
            },
            "layers": [
                {
                    "name": layer_name,
                    "feedrate": {
                        "type": "const",
                        "value": 100
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 1,
                        "steps": 1,
                        "passes": 1,
                        "sequential": False
                    }
                }
            ]
        }

    def test_gerber_load_ohdb2(self):

        config = self.__advanced_config(layer_name="bottom", optimize=2)
        fw = FileWriter('/tmp/output.gcode')

        output, planner = self.__simple(config=config, compact=True, no_comment=True, output=fw)

        pcb_config = {
            'rotation' : 0.0,
            'flip-top-bottom' : False
        }

        pcb = gerber.PCB.from_directory('./data/gerber/ohdb-v2', verbose=False)

        layer_shape = {}
        
        # Convert gerber to shapely objects
        for layer in pcb.layers:
            shp = ShapelyContext()
            
            if layer.layer_class == 'outline':
                shp.set_ignore_width(True)
                
            layer.cam_source.render( shp )
            # print layer
            if layer.mirrored:
                # print "- mirroring"
                layer_shape[layer.layer_class+'_mirrored'] = shp
                shp.mirror_x()
            else:
                layer_shape[layer.layer_class] = shp
            
            if (layer.layer_class == 'bottom' and pcb_config['flip-top-bottom'] == False) or (layer.layer_class == 'top' and pcb_config['flip-top-bottom'] == True):
                shp.mirror_x()
                    
            #shp.rotate( float(config['rotation']) )


        drawing = Drawing2D()

        for name, ctx in layer_shape.items():
            # print name, len(ctx.figs)
            lyr_obj, idx = drawing.addLayer(name)

            figs = []

            for fig in ctx.figs:
                # print "-", fig.geom_type
                if fig.geom_type == 'Polygon':
                    figs.append( fig )
                elif fig.geom_type == 'LineString':
                    dist = np.linalg.norm( np.array(fig.coords[-1]) - np.array(fig.coords[0]))
                    if dist < 0.01:
                        # print "IS-CLOSED"

                        poly = sg.Polygon( fig.coords[:-1] )
                        figs.append( poly )


            dia = 0.05

            if name == 'bottom':
                for i in xrange(10):
                    mp = sg.MultiPolygon(figs)
                    tps = mp.buffer(dia*(i+1) )

                    if tps.geom_type == 'Polygon':
                        drawing.addShapelyGeometry(tps, idx)
                    else:
                        for tp in tps:
                            drawing.addShapelyGeometry(tp, idx)

            # if name == 'outline':
            #     # for f in ctx.figs:
            #         # drawing.addShapelyGeometry(f, idx)
            #     mp = sg.MultiPolygon(figs)
            #     tps = mp.buffer(1)

            #     if tps.geom_type == 'Polygon':
            #         drawing.addShapelyGeometry(tps, idx)
            #     else:
            #         for tp in tps:
            #             drawing.addShapelyGeometry(tp, idx)

        planner.plan(drawing)
        planner.finish()

    # def test_gerber_load_ohdb2_raster(self):
    #     config = self.__advanced_config(layer_name="outline", optimize=2)
    #     fw = FileWriter('/tmp/output.gcode')

    #     output, planner = self.__simple(config=config, compact=True, no_comment=True, output=fw)

    #     pcb_config = {
    #         'rotation' : 0.0,
    #         'flip-top-bottom' : False
    #     }

    #     pcb = gerber.PCB.from_directory('./data/gerber/ohdb-v2', verbose=False)
            
if __name__ == '__main__':
    unittest.main()
