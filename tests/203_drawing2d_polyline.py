import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, ArgumentError, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        pass
        
    def test_add_polyline(self):
        d = Drawing2D()
        
        p_points = [ (0,1), (0,2), (1,3), (2,3) ]
        
        d.addPolyline(p_points)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'polyline' )
        self.assertEqual( len(prim['points']), 4 )
        
        pts = prim['points']
        for i,pt in enumerate(pts):
                self.assertEqual( pt, p_points[i] )
        
    def test_add_polyline_closed(self):
        d = Drawing2D()
        
        p_points = [ (0,1), (0,2), (1,3), (2,3) ]
        
        d.addPolyline(p_points, closed=True)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'polyline' )
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        for i,pt in enumerate(pts):
                self.assertEqual( pt, p_points[i] )
        
        # Last point is same as the first one
        self.assertEqual( pts[-1], pts[0] )
        
    def test_add_polyline_params(self):
        d = Drawing2D()
        
        p_points = [ (0,1), (0,2), (1,3), (2,3) ]
        
        d.addPolyline(closed=True, points=p_points, filled=True)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'polyline' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        for i,pt in enumerate(pts):
                self.assertEqual( pt, p_points[i] )
        
        # Last point is same as the first one
        self.assertEqual( pts[-1], pts[0] )
        
    def test_add_polyline_custom_layer(self):
        d = Drawing2D()
        layer, idx = d.addLayer("Second")
        
        p_points = [ (0,1), (0,2), (1,3), (2,3) ]
        
        d.addPolyline(closed=True, points=p_points, filled=True, layer=idx)
        self.assertEqual( len(d.layers[idx].primitives), 1 )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'polyline' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        for i,pt in enumerate(pts):
                self.assertEqual( pt, p_points[i] )
        
        # Last point is same as the first one
        self.assertEqual( pts[-1], pts[0] )
        
    def test_add_polyline_not_enough_points_0_error(self):
        d = Drawing2D()
        
        p_points = []
        
        with self.assertRaises(ArgumentError) as context:
            d.addPolyline(p_points)
        
        self.assertTrue('at least two points are needed' in str(context.exception))
        
    def test_add_polyline_not_enough_points_1_error(self):
        d = Drawing2D()
        
        p_points = [ (0,1) ]
        
        with self.assertRaises(ArgumentError) as context:
            d.addPolyline(p_points)
        
        self.assertTrue('at least two points are needed' in str(context.exception))
        
    def test_add_polyline_missing_layer_error(self):
        d = Drawing2D()
        
        p_points = [ (0,1), (0,2), (1,3), (2,3) ]
        
        with self.assertRaises(MissingLayerError) as context:
            d.addPolyline(p_points, layer=5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
        
if __name__ == '__main__':
    unittest.main()
