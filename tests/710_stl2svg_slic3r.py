import subprocess
import unittest
from unittest import TestCase

# Unit under test
from stl2svg import run_slic3r

class TestSTL2SVG(TestCase):
    
    def setUp(self):
        pass
        
    def test_stl2svg_run_slic3r(self):
        
        in_fn = './data/stl/makerook_small_support.stl'
        out_fn = '/tmp/output.svg'

        cfg = {
            "output": {
                "svg": True
            },
            "layers": {
                "height": 0.1
            }
        }

        exit_code = run_slic3r(cfg, in_fn, out_fn)
        self.assertEquals(0, exit_code)

    def test_stl2svg_cli_run_success(self):
        cfg_fns = ['./data/preset/slicer/printer/fabtotum_prism.json', 
                   './data/preset/slicer/sla/default_quality.json']
        in_fn = './data/stl/makerook_small_support.stl'
        out_fn = '/tmp/output.svg'

        cmd = '../stl2svg.py'
        cmd += ' ' + in_fn
        cmd += ' -c ' + cfg_fns[0]
        cmd += ' -c ' + cfg_fns[1]
        cmd += ' -o ' + out_fn

        exit_code = subprocess.call(cmd.split())
        self.assertEquals(0, exit_code)

if __name__ == '__main__':
    unittest.main()
