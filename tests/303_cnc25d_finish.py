import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.cnc25d import CNC25D

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestCNC25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = CNC25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
    
    def test_cnc25d_mill_finish(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.millTo(50,50)
        cnc.finish()
        
        self.assertEqual( str(output), 'G1Z-1F10|G1X50Y50F30|G0Z5F20|M400' )
    
    def test_cnc25d_travel_finish(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.travelTo(50,50)
        cnc.finish()
        
        self.assertEqual( str(output), 'G0Z5F20|G0X50Y50F100|M400' )
    
    def test_cnc25d_mill_finish_return_home(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.millTo(50,50)
        cnc.finish(return_home=True)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1X50Y50F30|G0Z5F20|G0X0Y0F100|M400' )
    
    def test_cnc25d_travel_finish_return_home(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.travelTo(50,50)
        cnc.finish(return_home=True)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X50Y50F100|G0X0Y0|M400' )
    
    def test_cnc25d_mill_finish_goto_custom_xy(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.millTo(50,50)
        cnc.finish(X=100, Y=0)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1X50Y50F30|G0Z5F20|G0X100Y0F100|M400' )
    
    def test_cnc25d_travel_finish_goto_custom_xy(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.travelTo(50,50)
        cnc.finish(X=100, Y=0)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X50Y50F100|G0X100Y0|M400' )
    
    def test_cnc25d_mill_finish_goto_custom_x(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.millTo(50,50)
        cnc.finish(X=100)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1X50Y50F30|G0Z5F20|G0X100F100|M400' )
    
    def test_cnc25d_travel_finish_goto_custom_x(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.travelTo(50,50)
        cnc.finish(X=100)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X50Y50F100|G0X100|M400' )
    
    def test_cnc25d_mill_finish_goto_custom_y(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.millTo(50,50)
        cnc.finish(Y=0)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1X50Y50F30|G0Z5F20|G0Y0F100|M400' )
        
    def test_cnc25d_travel_finish_goto_custom_y(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.travelTo(50,50)
        cnc.finish(Y=0)
        
        self.assertEqual( str(output), 'G0Z5F20|G0X50Y50F100|G0Y0|M400' )

        
if __name__ == '__main__':
    unittest.main()
