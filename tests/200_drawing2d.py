import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        pass
        
    def test_new_object(self):
        
        try:
            draw2d = Drawing2D()
        except Exception as e:
            draw2d = None
        
        self.assertFalse(draw2d is None)
    
    def test_empty_drawing_defaults(self):
        d = Drawing2D()
        
        self.assertEqual( d.width(), 0)
        self.assertEqual( d.height(), 0)
        self.assertEqual( len(d.layers), 1)
        self.assertEqual( d.layers[0].name, "Default")
        self.assertEqual( d.layers[0].color, 7)
        self.assertEqual( d.layers[0].primitives, [])
        
    def test_add_layer_defaults(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer()
        
        self.assertEqual( idx, 1)
        self.assertEqual( len(d.layers), 2)
        self.assertEqual( d.layers[1].name, "Layer_1")
        self.assertEqual( d.layers[1].color, Drawing2D.WHITE)
        
    def test_add_layer_custom(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("New layer", Drawing2D.RED)
        
        self.assertEqual( idx, 1)
        self.assertEqual( len(d.layers), 2)
        self.assertEqual( d.layers[1].name, "New layer")
        self.assertEqual( d.layers[1].color, Drawing2D.RED)
        
    def test_remove_layer(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("New layer", Drawing2D.RED)
        
        self.assertEqual( len(d.layers), 2)
        d.removeLayer(idx)
        self.assertEqual( len(d.layers), 1)
        
    def test_remove_layer_error(self):
        d = Drawing2D()
        
        with self.assertRaises(MissingLayerError) as context:
            idx = 5
            d.removeLayer(idx)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
        
    def test_clear(self):
        d = Drawing2D()
        
        d.clear()
        
        self.assertEqual( len(d.layers), 0)

if __name__ == '__main__':
    unittest.main()
