import unittest
from unittest import TestCase
from math import sqrt

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        self.knots = [0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0]
        self.control_points = [(0.4575990476449685, -3.19978538375501, 0.0), (0.776448989092944, -3.212201513519668, 0.0), (0.9434615946731124, -3.02355683640601, 0.0), (0.9586368643854719, -2.633851352414041, 0.0)]
        self.degree = 3
        
        self.ERROR_MARGIN = 0.0000000001

    def __point_diff(self, a, b):
        dx = abs(a[0] - b[0])
        dy = abs(a[1] - b[1])
        
        return sqrt(dx*dx + dy*dy)

    def test_add_spline(self):
        d = Drawing2D()
        
        d.addSpline(self.control_points, self.knots, self.degree)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'spline' )
        self.assertEqual( len(prim['points']), 32 )
        
        pts = prim['points']
        self.assertTrue( self.__point_diff(pts[0], self.control_points[0]) < self.ERROR_MARGIN )
        self.assertTrue( self.__point_diff(pts[-1], self.control_points[-1]) < self.ERROR_MARGIN )

    def test_add_spline_param(self):
        d = Drawing2D()
        
        d.addSpline(knots=self.knots, degree=self.degree, control_points=self.control_points, layer=0)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'spline' )
        self.assertEqual( len(prim['points']), 32 )
        
        pts = prim['points']
        self.assertTrue( self.__point_diff(pts[0], self.control_points[0]) < self.ERROR_MARGIN )
        self.assertTrue( self.__point_diff(pts[-1], self.control_points[-1]) < self.ERROR_MARGIN )
        
        
    def test_add_spline_custom_layer(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("Second")
        
        d.addSpline(self.control_points, self.knots, self.degree, layer=idx)
        self.assertEqual( len(layer.primitives), 1 )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'spline' )
        self.assertEqual( len(prim['points']), 32 )
        
        pts = prim['points']
        self.assertTrue( self.__point_diff(pts[0], self.control_points[0]) < self.ERROR_MARGIN )
        self.assertTrue( self.__point_diff(pts[-1], self.control_points[-1]) < self.ERROR_MARGIN )
        
    def test_add_spline_missing_layer_error(self):
        d = Drawing2D()
        
        with self.assertRaises(MissingLayerError) as context:
            d.addSpline(self.control_points, self.knots, self.degree, layer=5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
    

if __name__ == '__main__':
    unittest.main()
