import unittest
from unittest import TestCase
from math import sqrt

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

try:
    import shapely
    use_shapely = True
except ImportError:
    use_shapely = False

class TestDrawing2D(TestCase):
    
    def setUp(self):
        self.ERROR_MARGIN = 0.0000000001
    
    def test_scale_down(self):
        d = Drawing2D()
        
        p_first  = (1.0, 2.0)
        p_second = (100, 200)
        p_r = 100
        sx = 0.5
        sy = 0.5
        
        
        d.addLine( p_first, p_second )
        d.addRect2( p_first, p_second )
        d.addCircle( p_first, p_r )
        
        d.scale(sx, sy)
        
        line = d.layers[0].primitives[0]
        pts = line['points']
        self.assertEqual( pts[0], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( pts[1], (p_second[0]*sx, p_second[1]*sy) )
        
        rect = d.layers[0].primitives[1]
        pts = rect['points']
        self.assertEqual( pts[0], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( rect['first'], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( pts[2], (p_second[0]*sx, p_second[1]*sy) )
        self.assertEqual( rect['second'], (p_second[0]*sx, p_second[1]*sy) )
        
        circle = d.layers[0].primitives[2]
        pts = circle['points']
        self.assertEqual( circle['center'], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( circle['radius'], p_r*sx )
        
        for i, pt in enumerate(pts):
            dx = float(pt[0] - p_first[0]*sx)
            dy = float(pt[1] - p_first[1]*sy)
            
            r = sqrt( dx*dx + dy*dy)
            error = abs( r  - p_r*sx)
            self.assertTrue( error < self.ERROR_MARGIN )
    
    def test_scale_up(self):
        d = Drawing2D()
        
        p_first  = (1.0, 2.0)
        p_second = (100, 200)
        p_r = 100
        sx = 2.5
        sy = 2.5
        
        
        d.addLine( p_first, p_second )
        d.addRect2( p_first, p_second )
        d.addCircle( p_first, p_r )
        
        d.scale(sx, sy)
        
        line = d.layers[0].primitives[0]
        pts = line['points']
        self.assertEqual( pts[0], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( pts[1], (p_second[0]*sx, p_second[1]*sy) )
        
        rect = d.layers[0].primitives[1]
        pts = rect['points']
        self.assertEqual( pts[0], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( rect['first'], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( pts[2], (p_second[0]*sx, p_second[1]*sy) )
        self.assertEqual( rect['second'], (p_second[0]*sx, p_second[1]*sy) )
        
        circle = d.layers[0].primitives[2]
        pts = circle['points']
        self.assertEqual( circle['center'], (p_first[0]*sx, p_first[1]*sy) )
        self.assertEqual( circle['radius'], p_r*sx )
        
        for i, pt in enumerate(pts):
            dx = float(pt[0] - p_first[0]*sx)
            dy = float(pt[1] - p_first[1]*sy)
            
            r = sqrt( dx*dx + dy*dy)
            error = abs( r  - p_r*sx)
            self.assertTrue( error < self.ERROR_MARGIN )
        
    def test_scale_to(self):
        d = Drawing2D()
        
        p_first  = (0.0, 0.0)
        p_second = (100, 200)
        p_center = (50, 50)
        p_r = 50
        
        d.addLine( p_first, p_second )
        d.addRect2( p_first, p_second )
        d.addCircle( p_center, p_r )
        
        d.scale_to(target_width=10)
        
        self.assertEqual( d.width(), 10 )
        self.assertEqual( d.height(), 20 )
        
    def test_scale_to_not_zero_center(self):
        d = Drawing2D()
        
        p_first  = (-10.0, 0.0)
        p_second = (100, 200)
        p_center = (50, 50)
        p_r = 50
        
        d.addLine( p_first, p_second )
        d.addRect2( p_first, p_second )
        d.addCircle( p_center, p_r )
        
        d.scale_to(target_width=10)
        
        self.assertEqual( d.width(), 10 )
        
    def test_transform_scale(self):
        d = Drawing2D()
        
        p_center = (50, 50)
        p_r = 50
        d.addCircle( p_center, p_r )
        
        d.transform(0.5, 0.5)
        
        self.assertEqual( (d.width(), d.height()), (50.0, 50.0) )
        
    def test_transform_offset_x(self):
        d = Drawing2D()
        
        p_center = (50, 50)
        p_r = 50
        d.addCircle( p_center, p_r )
        
        d.transform(ox=10)
        
        circle = d.layers[0].primitives[0]
        
        self.assertEqual( (d.width(), d.height()), (100.0, 100.0) )
        self.assertEqual( circle['center'], (60, 50) )
        
        new_center = (60, 50)
        pts = circle['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - new_center[0])
            dy = float(pt[1] - new_center[1])
            
            r = sqrt( dx*dx + dy*dy)
            error = abs( r  - p_r)
            self.assertTrue( error < self.ERROR_MARGIN )
        
    def test_transform_offset_y(self):
        d = Drawing2D()
        
        p_center = (50, 50)
        p_r = 50
        d.addCircle( p_center, p_r )
        
        d.transform(oy=10)
        
        circle = d.layers[0].primitives[0]
        
        self.assertEqual( (d.width(), d.height()), (100.0, 100.0) )
        self.assertEqual( circle['center'], (50, 60) )
        
        new_center = (50, 60)
        pts = circle['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - new_center[0])
            dy = float(pt[1] - new_center[1])
            
            r = sqrt( dx*dx + dy*dy)
            error = abs( r  - p_r)
            self.assertTrue( error < self.ERROR_MARGIN )
        
    def test_transform(self):
        d = Drawing2D()
        
        p_center = (50, 50)
        p_r = 50
        d.addCircle( p_center, p_r )
        
        d.transform(sx=0.5, sy=0.5, ox=10, oy=20)
        
        circle = d.layers[0].primitives[0]
        
        self.assertEqual( (d.width(), d.height()), (50.0, 50.0) )
        self.assertEqual( circle['center'], (30, 35) )
        
        new_center = (30, 35)
        pts = circle['points']
        for i, pt in enumerate(pts):
            dx = float(pt[0] - new_center[0])
            dy = float(pt[1] - new_center[1])
            
            r = sqrt( dx*dx + dy*dy)
            error = abs( r  - p_r*0.5)
            self.assertTrue( error < self.ERROR_MARGIN )
        
if __name__ == '__main__':
    unittest.main()
