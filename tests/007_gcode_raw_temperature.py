import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_104_no_param_error(self):
        self.__default_config()
        
        with self.assertRaises(TypeError) as context:
            self.raw.M104()
        
        self.assertTrue('arguments (1 given)'in str(context.exception))
    
    def test_M104_s(self):
        self.__default_config()
        
        self.raw.M104(150)
        self.assertEqual(str(self.output), 'M104 S150')
        
    def test_M104_s_param(self):
        self.__default_config()
        
        self.raw.M104(S=150)
        self.assertEqual(str(self.output), 'M104 S150')
        
    def test_M104_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M104(S=150)
        self.assertEqual(str(self.output), 'M104S150')
        
    def test_M104_temp_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M104(RAW.TEMP_MAX + 10)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
        
    def test_M104_temp_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M104(RAW.TEMP_MIN - 10)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
    
    # M105
    
    def test_M105(self):
        self.__default_config()
        
        self.raw.M105()
        self.assertEqual(str(self.output), 'M105')
        
    # M109
    
    def test_109_no_param_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M109()
        
        self.assertTrue('S or R parameter'in str(context.exception))
    
    def test_M109_s(self):
        self.__default_config()
        
        self.raw.M109(150)
        self.assertEqual(str(self.output), 'M109 S150')
        
    def test_M109_s_param(self):
        self.__default_config()
        
        self.raw.M109(S=150)
        self.assertEqual(str(self.output), 'M109 S150')
        
    def test_109_s_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M109(RAW.TEMP_MAX + 10)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
        
    def test_109_s_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M109(RAW.TEMP_MIN - 10)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
        
    def test_M109_r_param(self):
        self.__default_config()
        
        self.raw.M109(R=150)
        self.assertEqual(str(self.output), 'M109 R150')
        
    def test_M109_r_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M109(R=150)
        self.assertEqual(str(self.output), 'M109R150')
        
    def test_M109_s_t(self):
        self.__default_config()
        
        self.raw.M109(S=150,T=1)
        self.assertEqual(str(self.output), 'M109 S150 T1')
        
    def test_M109_t_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M109(150, T=RAW.EXTRUDER_ID_MAX + 1)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
        
    def test_M109_t_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M109(150, T=RAW.EXTRUDER_ID_MIN - 1)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
    
    # M140
    def test_M140_no_param_error(self):
        self.__default_config()
        
        with self.assertRaises(TypeError) as context:
            self.raw.M104()
        
        self.assertTrue('arguments (1 given)'in str(context.exception))
    
    def test_M140_s(self):
        self.__default_config()
        
        self.raw.M140(150)
        self.assertEqual(str(self.output), 'M140 S150')
        
    def test_M140_s_param(self):
        self.__default_config()
        
        self.raw.M140(S=150)
        self.assertEqual(str(self.output), 'M140 S150')
        
    def test_M140_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M140(S=150)
        self.assertEqual(str(self.output), 'M140S150')
        
    def test_M140_temp_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M140(RAW.TEMP_MAX + 10)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
        
    def test_M140_temp_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M140(RAW.TEMP_MIN - 10)
        
        self.assertTrue('is out of allowed range'in str(context.exception))
        
    # M190
    def test_M190_s_param(self):
        self.__default_config()
        
        self.raw.M190(S=150)
        self.assertEqual(str(self.output), 'M190 S150')

        
if __name__ == '__main__':
    unittest.main()
