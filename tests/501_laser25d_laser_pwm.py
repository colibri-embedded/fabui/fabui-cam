import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.laser25d import Laser25D
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestLaser25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = Laser25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
        
    def test_laser25d_laser_on_default(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        self.assertEqual(str(output), 'M61S255')
        
    def test_laser25d_laser_on_config(self):
        output, cnc = self.__default(config={'pwm':128}, compact=True)
        
        cnc.laserOn()
        self.assertEqual(str(output), 'M61S128')
        
    def test_laser25d_laser_on_setpwm(self):
        output, cnc = self.__default(compact=True)
        
        cnc.setPWM(123)
        cnc.laserOn()
        self.assertEqual(str(output), 'M61S123')
        
    def test_laser25d_laser_off_idle(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOff()
        self.assertEqual(str(output), '')
        
    def test_laser25d_laser_on_2x(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.laserOn()
        self.assertEqual(str(output), 'M61S255')
        
    def test_laser25d_laser_off_idle(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.laserOff()
        self.assertEqual(str(output), 'M61S255|M62')
        
    def test_laser25d_laser_on_wrong_type_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.laserOn('255')
        
        self.assertTrue('must be a number' in str(context.exception))
        
    def test_laser25d_laser_on_pwm_to_high_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.laserOn(RAW.PWM_MAX +1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_laser25d_laser_on_pwm_to_high_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.laserOn(RAW.PWM_MIN -1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
if __name__ == '__main__':
    unittest.main()
