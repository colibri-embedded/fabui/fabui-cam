import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        pass
        
    def test_add_line(self):
        d = Drawing2D()
        
        p_start = (0,1)
        p_end   = (2,3)
        
        d.addLine(p_start, p_end)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'line' )
        self.assertEqual( len(prim['points']), 2 )
        
        pts = prim['points']
        self.assertEqual( pts[0], (0,1) )
        self.assertEqual( pts[1], (2,3) )
        
    def test_add_line_param(self):
        d = Drawing2D()
        
        p_start = (0,1)
        p_end   = (2,3)
        
        d.addLine(end=p_end, start=p_start)
        self.assertEqual( len(d.layers[0].primitives), 1 )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'line' )
        self.assertEqual( len(prim['points']), 2 )
        
        pts = prim['points']
        self.assertEqual( pts[0], (0,1) )
        self.assertEqual( pts[1], (2,3) )
        
    def test_add_line_custom_layer(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("Second")
        
        p_start = (0,1)
        p_end   = (2,3)
        
        d.addLine( (0.5, 0.6), (0.7, 0.8) )
        d.addLine(p_start, p_end, idx)
        
        self.assertEqual( len(d.layers[idx].primitives), 1 )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'line' )
        self.assertEqual( len(prim['points']), 2 )
        
        pts = prim['points']
        self.assertEqual( pts[0], (0,1) )
        self.assertEqual( pts[1], (2,3) )
        
    def test_add_line_missing_layer_error(self):
        d = Drawing2D()
        
        p_start = (0,1)
        p_end   = (2,3)
        
        with self.assertRaises(MissingLayerError) as context:
            d.addLine(p_start, p_end, 5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
        
        
    

if __name__ == '__main__':
    unittest.main()
