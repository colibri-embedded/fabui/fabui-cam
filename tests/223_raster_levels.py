import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.raster import Raster

class TestRaster(TestCase):
    
    def setUp(self):
        pass
    
    def test_raster_4_levels(self):
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)
        r.loadFromFile('./data/png/gray_levels_50x20.png')

        levels = r.createLevels(4)
        self.assertEqual( len(levels), 4 )
        
        level_values = [21, 93, 163, 232]
        
        i = 0
        for lvl in levels:
            self.assertEqual( lvl['value'], level_values[i] )
            #~ print lvl['value']
            i += 1
    
    def test_raster_6_levels(self):
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)
        r.loadFromFile('./data/png/gray_levels_50x20.png')

        levels = r.createLevels(6)
        self.assertEqual( len(levels), 6 )
        
        level_values = [10, 59, 107, 153, 198, 243]
        
        i = 0
        for lvl in levels:
            self.assertEqual( lvl['value'], level_values[i] )
            i += 1
    
    def test_raster_1_level(self):
        dpm   = 1 / 0.1 #mm
        height = 12.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/jpg/mini_pcb_black_white_191x145.jpg')
        
        img = r.getImage()
        self.assertEqual( img.shape[:2], (120,158) )
        
        levels = r.createLevels(1)
        self.assertEqual( len(levels), 1 )
        self.assertEqual( levels[0]['data'][0,0][0], 255 )
        
    def test_raster_1_level_invert(self):
        dpm   = 1 / 0.1 #mm
        height = 12.0
        
        r = Raster(dpm=dpm, height=height, invert=True)
        r.loadFromFile('./data/jpg/mini_pcb_black_white_191x145.jpg')
        
        img = r.getImage()
        self.assertEqual( img.shape[:2], (120,158) )
        
        levels = r.createLevels(1)
        self.assertEqual( len(levels), 1 )
        self.assertEqual( levels[0]['data'][0,0][0], 0 )
    
if __name__ == '__main__':
    unittest.main()
