import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.laser25d import Laser25D
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestLaser25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = Laser25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
    
    def test_laser25d_new_object(self):
        output, cnc = self.__default(compact=True)
        
        self.assertFalse(cnc is None)
        self.assertEqual( cnc.config['pwm'], 255 )
        self.assertEqual( cnc.config['cut-feedrate'], 100.0 )
        self.assertEqual( cnc.config['travel-feedrate'], 200.0 )
        self.assertEqual( cnc.config['plunge-rate'], 200.0 )
        self.assertEqual( cnc.config['retract-rate'], 200.0 )
        self.assertEqual( cnc.config['safe-z'], 0.0 )
        self.assertEqual( cnc.config['cut-depth'], 0.0 )
        self.assertEqual( cnc.config['off-during-travel'], True )
        self.assertEqual( cnc.config['xyz-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['e-mode'], CNC.eABSOLUTE )
        self.assertEqual( cnc.config['use-probe'], False )
        
        
    def test_laser25d_setpwm(self):
        output, cnc = self.__default(compact=True)
        
        cnc.setPWM(123)
        self.assertEqual( cnc.config['pwm'], 123 )
        
    def test_laser25d_setpwm_type_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.setPWM('255')
        
        self.assertTrue('must be a number' in str(context.exception))
        
    def test_laser25d_setpwm_too_high_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.setPWM(RAW.PWM_MAX +1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_laser25d_setpwm_too_low_error(self):
        output, cnc = self.__default(compact=True)
        
        with self.assertRaises(ParameterError) as context:
            cnc.setPWM(RAW.PWM_MIN -1)
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
if __name__ == '__main__':
    unittest.main()
