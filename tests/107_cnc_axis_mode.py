import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False, xyzMode=CNC.eABSOLUTE, eMode=CNC.eABSOLUTE):
        #~ self.cnc.useProbe = use_probe
        self.cnc.config['use-probe'] = use_probe
        self.cnc.config['xyz-mode'] = xyzMode
        self.cnc.config['e-mode'] = eMode
        self.output.clear()
        
    def __move_axis(self, x = 0.0, y = 0.0, z = 0.0, e = 0.0, f = 100):
        self.cnc.curX = x
        self.cnc.curY = y
        self.cnc.curZ = z
        self.cnc.curE = e
        self.cnc.curF = f

    # Is absolute
    def test_is_absolute_x(self):
        self.__default_config()
        
        self.assertTrue( self.cnc.isAbsolute(CNC.X_AXIS) )
        
    def test_is_absolute_y(self):
        self.__default_config()
        
        self.assertTrue( self.cnc.isAbsolute(CNC.Y_AXIS) )
        
    def test_is_absolute_z(self):
        self.__default_config()
        
        self.assertTrue( self.cnc.isAbsolute(CNC.Z_AXIS) )
        
    def test_is_absolute_e(self):
        self.__default_config()
        
        self.assertTrue( self.cnc.isAbsolute(CNC.E_AXIS) )
    
    # Is relative
    def test_is_relative_x(self):
        self.__default_config()
        
        self.assertFalse( self.cnc.isRelative(CNC.X_AXIS) )
        
    def test_is_relative_y(self):
        self.__default_config()
        
        self.assertFalse( self.cnc.isRelative(CNC.Y_AXIS) )
        
    def test_is_relative_z(self):
        self.__default_config()
        
        self.assertFalse( self.cnc.isRelative(CNC.Z_AXIS) )
        
    def test_is_relative_e(self):
        self.__default_config()
        
        self.assertFalse( self.cnc.isRelative(CNC.E_AXIS) )
    
    # Set axis mode
    def test_set_axis_mode_x(self):
        self.__default_config()
        
        self.cnc.setAxisMode(CNC.X_AXIS, CNC.eRELATIVE)
        self.assertFalse( self.cnc.isAbsolute(CNC.X_AXIS) )
        
        self.cnc.setAxisMode(CNC.X_AXIS, CNC.eABSOLUTE)
        self.assertTrue( self.cnc.isAbsolute(CNC.X_AXIS))
        
    def test_set_axis_mode_y(self):
        self.__default_config()
        
        self.cnc.setAxisMode(CNC.Y_AXIS, CNC.eRELATIVE)
        self.assertFalse(self.cnc.isAbsolute(CNC.Y_AXIS) )
        
        self.cnc.setAxisMode(CNC.Y_AXIS, CNC.eABSOLUTE)
        self.assertTrue( self.cnc.isAbsolute(CNC.Y_AXIS) )
        
    def test_set_axis_mode_z(self):
        self.__default_config()
        
        self.cnc.setAxisMode(CNC.Z_AXIS, CNC.eRELATIVE)
        self.assertFalse( self.cnc.isAbsolute(CNC.Z_AXIS) )
        
        self.cnc.setAxisMode(CNC.Z_AXIS, CNC.eABSOLUTE)
        self.assertTrue( self.cnc.isAbsolute(CNC.Z_AXIS) )
        
    def test_set_axis_mode_e(self):
        self.__default_config()
        
        self.cnc.setAxisMode(CNC.E_AXIS, CNC.eRELATIVE)
        self.assertFalse( self.cnc.isAbsolute(CNC.E_AXIS) )
        
        self.cnc.setAxisMode(CNC.E_AXIS, CNC.eABSOLUTE)
        self.assertTrue( self.cnc.isAbsolute(CNC.E_AXIS) )
        
    def test_set_axis_mode_xyz_old(self):
        self.__default_config()
        
        self.cnc.setAxisMode(CNC.X_AXIS, CNC.eABSOLUTE)
        self.assertEqual( str(self.output), '' )
        
    def test_set_axis_mode_xyz_new_abs(self):
        self.__default_config(xyzMode=CNC.eRELATIVE)
        
        self.cnc.setAxisMode(CNC.X_AXIS, CNC.eABSOLUTE)
        self.assertEqual( str(self.output), 'G90' )
        
    def test_set_axis_mode_xyz_new_rel(self):
        self.__default_config(xyzMode=CNC.eABSOLUTE)
        
        self.cnc.setAxisMode(CNC.X_AXIS, CNC.eRELATIVE)
        self.assertEqual( str(self.output), 'G91' )
        
    def test_set_axis_mode_e_new_abs(self):
        self.__default_config(eMode=CNC.eRELATIVE)
        
        self.cnc.setAxisMode(CNC.E_AXIS, CNC.eABSOLUTE)
        self.assertEqual( str(self.output), 'M82' )
        
    def test_set_axis_mode_e_new_rel(self):
        self.__default_config(eMode=CNC.eABSOLUTE)
        
        self.cnc.setAxisMode(CNC.E_AXIS, CNC.eRELATIVE)
        self.assertEqual( str(self.output), 'M83' )
        
if __name__ == '__main__':
    unittest.main()
