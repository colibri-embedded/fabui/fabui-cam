import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestGCodeCNC(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.cnc = CNC(self.output, ndigits=3)

    def __default_config(self, use_probe=False):
        self.cnc.useProbe = use_probe
        self.output.clear()
        
    def __move_axis(self, x = 0.0, y = 0.0, z = 0.0, e = 0.0, f = 100):
        self.cnc.curX = x
        self.cnc.curY = y
        self.cnc.curZ = z
        self.cnc.curE = e
        self.cnc.setFeedrate(f)

    # Tool moves
    def test_move_to_xy(self):
        self.__default_config()
        
        self.cnc.setFeedrate(200)
        
        self.cnc.moveTo(10, 11)
        self.assertEqual(str(self.output), 'G1 X10 Y11 F200')
        self.assertTrue( self.cnc.isAt(X=10, Y=11) )

    def test_move_to_xyz(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(10, 11, 12)
        self.assertEqual(str(self.output), 'G1 X10 Y11 Z12 F100')
        self.assertTrue( self.cnc.isAt(X=10, Y=11, Z=12) )
        
    def test_move_to_x_param(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(X=10)
        self.assertEqual(str(self.output), 'G1 X10 F100')
        self.assertTrue( self.cnc.isAt(X=10) )
        
    def test_move_to_y_param(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Y=10)
        self.assertEqual(str(self.output), 'G1 Y10 F100')
        self.assertTrue( self.cnc.isAt(Y=10) )
        
    def test_move_to_z_param(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.moveTo(Z=10)
        self.assertEqual(str(self.output), 'G1 Z10 F100')
        self.assertTrue( self.cnc.isAt(Z=10) )
        
    # Travel moves
    def test_travel_to_xy(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.travelTo(10, 11)
        self.assertEqual(str(self.output), 'G0 X10 Y11 F100')
        self.assertTrue( self.cnc.isAt(X=10, Y=11) )

    def test_travel_to_xyz(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.travelTo(10, 11, 12)
        self.assertEqual(str(self.output), 'G0 X10 Y11 Z12 F100')
        self.assertTrue( self.cnc.isAt(X=10, Y=11, Z=12) )
        
    def test_travel_to_x_param(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.travelTo(X=10)
        self.assertEqual(str(self.output), 'G0 X10 F100')
        self.assertTrue( self.cnc.isAt(X=10) )
        
    def test_travel_to_y_param(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.travelTo(Y=10)
        self.assertEqual(str(self.output), 'G0 Y10 F100')
        self.assertTrue( self.cnc.isAt(Y=10) )
        
    def test_travel_to_z_param(self):
        self.__default_config()
        
        self.__move_axis()
        self.cnc.travelTo(Z=10)
        self.assertEqual(str(self.output), 'G0 Z10 F100')
        self.assertTrue( self.cnc.isAt(Z=10) )
    

if __name__ == '__main__':
    unittest.main()
