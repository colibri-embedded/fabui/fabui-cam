import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.raster import Raster

class TestRaster(TestCase):
    
    def setUp(self):
        pass
    
    def test_raster_load_png_upsize_width(self):
        import cv2
        
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)        
        r.loadFromFile('./data/png/chicken_twist_logo_496x330.png')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (665,1000) )
        self.assertEqual( r.width(), 1000 )
        self.assertEqual( r.height(), 665 )
    
    def test_raster_load_png_upsize_height(self):
        import cv2
        
        dpm   = 1 / 0.1 #mm
        height = 100.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/png/chicken_twist_logo_496x330.png')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (1000,1503) )
        self.assertEqual( r.width(), 1503 )
        self.assertEqual( r.height(), 1000 )
    
    def test_raster_load_png_downsize_width(self):
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)
        r.loadFromFile('./data/png/printers_distillery_logo_1920x1080.png')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (562,1000) )
        self.assertEqual( r.width(), 1000 )
        self.assertEqual( r.height(), 562 )
    
    def test_raster_load_png_downsize_height(self):
        dpm   = 1 / 0.1 #mm
        height = 100.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/png/printers_distillery_logo_1920x1080.png')
        
        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (1000,1777) )
        self.assertEqual( r.width(), 1777 )
        self.assertEqual( r.height(), 1000 )
    
    def test_raster_load_png_transparent_width(self):
        dpm   = 1 / 0.1 #mm
        width = 100.0
        
        r = Raster(dpm=dpm, width=width)
        r.loadFromFile('./data/png/fabtotum_logo_transparent_300x89.png')

        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (296,1000) )
        self.assertEqual( r.width(), 1000 )
        self.assertEqual( r.height(), 296 )
    
    def test_raster_load_png_transparent_height(self):
        dpm   = 1 / 0.1 #mm
        height = 100.0
        
        r = Raster(dpm=dpm, height=height)
        r.loadFromFile('./data/png/fabtotum_logo_transparent_300x89.png')

        img = r.getImage()
        
        self.assertEqual( img.shape[:2], (1000,3370) )
        self.assertEqual( r.width(), 3370 )
        self.assertEqual( r.height(), 1000 )
        
if __name__ == '__main__':
    unittest.main()
