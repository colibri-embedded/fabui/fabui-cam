import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.cnc25d import CNC25D

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestCNC25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = CNC25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
    
    def test_cnc25d_travelto_from_zero_z(self):
        output, cnc = self.__default(compact=True)
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,0) )
        
        cnc.travelTo(10, 11)
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100' )
    
    def test_cnc25d_travelto_from_safe_z(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        output, cnc = self.__default(compact=True, position=(0,0,safe_z))
        
        position = cnc.getCurrentPosition()
        self.assertEqual(position, (0,0,safe_z) )
        
        cnc.travelTo(10, 11)
        self.assertEqual( str(output), 'G0X10Y11F100' )
    
    def test_cnc25d_travelto_plunge(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True)
        
        self.assertEqual(cnc.getCurrentPosition(), (0,0,0) )
        
        cnc.travelTo(10, 11)
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100' )
        self.assertEqual(cnc.getCurrentPosition(), (10,11,safe_z) )
        
        output.clear()
        cnc.millTo(10, 11)
        self.assertEqual( str(output), 'G1Z-1F10' )
        self.assertEqual(cnc.getCurrentPosition(), (10,11,-cut_depth) )
        
    def test_cnc25d_travelto_plunge_2x(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True)
        
        self.assertEqual(cnc.getCurrentPosition(), (0,0,0) )
        
        cnc.travelTo(10, 11)
        self.assertEqual( str(output), 'G0Z5F20|G0X10Y11F100' )
        self.assertEqual(cnc.getCurrentPosition(), (10,11,safe_z) )
        
        output.clear()
        cnc.millTo(10, 11)
        cnc.setCutDepth(2.0)
        cnc.millTo(10, 11)
        
        self.assertEqual( str(output), 'G1Z-1F10|G1Z-2' )
        self.assertEqual(cnc.getCurrentPosition(), (10,11,-2.0) )
        
    def test_cnc25d_mill_mill(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True, position=(0,0,-cut_depth))
        
        cnc.millTo(10, 11)
        cnc.millTo(20, 11)
        self.assertEqual( str(output), 'G1X10Y11F30|G1X20' )
        
    def test_cnc25d_push_pop_settings(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True, position=(0,0,-cut_depth))
        
        cnc.millTo(10, 11)
        cnc.millTo(20, 11)
        
        cnc.pushSettings()
        cnc.setFeedrate(5)
        cnc.millTo(10, 31)
        cnc.millTo(20, 41)
        cnc.popSettings()
        
        cnc.millTo(0, 31)
        cnc.millTo(0, 41)
        
        self.assertEqual( str(output), 'G1X10Y11F30|G1X20|G1X10Y31F5|G1X20Y41|G1X0Y31F30|G1Y41' )
        
    def test_cnc25d_mill_plunge_mill(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True, position=(0,0,-cut_depth))
        
        cnc.millTo(10, 11)
        cnc.setCutDepth(2.0)
        cnc.millTo(20, 11)
        self.assertEqual( str(output), 'G1X10Y11F30|G1Z-2F10|G1X20F30' )
        
    def test_cnc25d_mill_plunge_mill_x2(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True, position=(0,0,-cut_depth))
        
        cnc.millTo(10, 11)
        cnc.setCutDepth(2.0)
        cnc.millTo(20, 11)
        cnc.millTo(40, 21)
        self.assertEqual( str(output), 'G1X10Y11F30|G1Z-2F10|G1X20F30|G1X40Y21' )
        
    def test_cnc25d_mill_travel_mill(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True, position=(0,0,-cut_depth))
        
        cnc.millTo(10, 11)
        cnc.travelTo(20,11)
        cnc.millTo(40, 21)
        self.assertEqual( str(output), 'G1X10Y11F30|G0Z5F20|G0X20F100|G1Z-1F10|G1X40Y21F30' )
        
    def test_cnc25d_mill_travel_same_mill(self):
        safe_z = CNC25D.DEFAULT_CONFIG['safe-z']
        cut_depth = CNC25D.DEFAULT_CONFIG['cut-depth']
        output, cnc = self.__default(compact=True, position=(0,0,-cut_depth))
        
        cnc.millTo(10, 11)
        cnc.travelTo(10,11)
        cnc.millTo(40, 21)
        self.assertEqual( str(output), 'G1X10Y11F30|G1X40Y21' )
        
if __name__ == '__main__':
    unittest.main()
