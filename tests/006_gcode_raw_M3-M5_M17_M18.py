import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_M3_no_param_error(self):
        self.__default_config()
        
        with self.assertRaises(TypeError) as context:
            self.raw.M3()
        
        self.assertTrue('arguments (1 given)', context.exception)
        
    def test_M3_s(self):
        self.__default_config()
        
        self.raw.M3(8000)
        self.assertEqual(str(self.output), 'M3 S8000')
        
    def test_M3_s_param(self):
        self.__default_config()
        
        self.raw.M3(S=8000)
        self.assertEqual(str(self.output), 'M3 S8000')
        
    def test_M3_s_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M3(8000)
        self.assertEqual(str(self.output), 'M3S8000')
        
    def test_M4_no_param_error(self):
        self.__default_config()
        
        with self.assertRaises(TypeError) as context:
            self.raw.M4()
        
        self.assertTrue('arguments (1 given)' in str(context.exception))
        
    def test_M4_rpm_too_high_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M4( RAW.RPM_MAX + 1000 )
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_M4_rpm_too_low_error(self):
        self.__default_config()
        
        with self.assertRaises(ParameterError) as context:
            self.raw.M4( RAW.RPM_MIN - 1000 )
        
        self.assertTrue('is out of allowed range' in str(context.exception))
        
    def test_M4_s(self):
        self.__default_config()
        
        self.raw.M4(8000)
        self.assertEqual(str(self.output), 'M4 S8000')
        
    def test_M5(self):
        self.__default_config()
        
        self.raw.M5()
        self.assertEqual(str(self.output), 'M5')
        
    def test_M17(self):
        self.__default_config()
        
        self.raw.M17()
        self.assertEqual(str(self.output), 'M17')
        
    def test_M18(self):
        self.__default_config()
        
        self.raw.M18()
        self.assertEqual(str(self.output), 'M18')
        
if __name__ == '__main__':
    unittest.main()
