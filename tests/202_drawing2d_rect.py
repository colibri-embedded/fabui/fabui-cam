import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, ArgumentError, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        pass
        
    def test_add_rect(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        width  = abs( p_first[0] - p_second[0] )
        height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect(p_first, width, height)
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],width,height) )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect_filled(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        width  = abs( p_first[0] - p_second[0] )
        height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect(p_first, width, height, filled=True)
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],width,height) )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect_custom_layer(self):
        d = Drawing2D()
        layer, idx = d.addLayer("Second")
         
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect(p_first, p_width, p_height, idx)
        
        self.assertEqual( len(d.layers[idx].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],p_width,p_height) )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect_param(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect(width=p_width, layer=0, first=p_first, filled=True, height=p_height)
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],p_width,p_height) )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect_first_error(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )

        with self.assertRaises(ArgumentError) as context:
            d.addRect(p_first[0], p_width, p_height)
        
        self.assertTrue('first parameter must be a' in str(context.exception))
        
    def test_add_rect_width_error(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )

        with self.assertRaises(ArgumentError) as context:
            d.addRect(p_first, '3', p_height)
        
        self.assertTrue('width parameter must be a' in str(context.exception))
        
    def test_add_rect_height_error(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )

        with self.assertRaises(ArgumentError) as context:
            d.addRect(p_first, p_width, '4')
        
        self.assertTrue('height parameter must be a' in str(context.exception))
        
    def test_add_rect2(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        width  = abs( p_first[0] - p_second[0] )
        height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect2(p_first, p_second)
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],width,height) )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect2_filled(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        width  = abs( p_first[0] - p_second[0] )
        height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect2(p_first, p_second, filled=True)
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],width,height) )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect2_custom_layer(self):
        d = Drawing2D()
        layer, idx = d.addLayer("Second")
         
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect2(p_first, p_second, idx)
        
        self.assertEqual( len(d.layers[idx].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],p_width,p_height) )
        
        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect2_param(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )
        
        bound = d.addRect2(second=p_second, layer=0, first=p_first, filled=True)
        
        self.assertEqual( len(d.layers[0].primitives), 1 )
        self.assertEqual( bound, (p_first[0],p_first[1],p_width,p_height) )
        
        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'rect' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( prim['first'], p_first )
        self.assertEqual( prim['second'], p_second )
        
        self.assertEqual( len(prim['points']), 5 )
        
        pts = prim['points']
        self.assertEqual( pts[0], p_first )
        self.assertEqual( pts[2], p_second )
        
    def test_add_rect2_first_error(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )

        with self.assertRaises(ArgumentError) as context:
            d.addRect2(p_width, p_second)
        
        self.assertTrue('first parameter must be a' in str(context.exception))
        
    def test_add_rect2_second_error(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )

        with self.assertRaises(ArgumentError) as context:
            d.addRect2(p_first, p_width)
        
        self.assertTrue('second parameter must be a' in str(context.exception))
        
    def test_add_rect_missing_layer_error(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("Second")
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )
        
        with self.assertRaises(MissingLayerError) as context:
            d.addRect(p_first, p_width, p_height, layer=5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
        
    def test_add_rect2_missing_layer_error(self):
        d = Drawing2D()
        
        p_first  = (0,1)
        p_second = (2,3)
        
        p_width  = abs( p_first[0] - p_second[0] )
        p_height = abs( p_first[1] - p_second[1] )
        
        with self.assertRaises(MissingLayerError) as context:
            d.addRect2(p_first, p_second, layer=5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
    

if __name__ == '__main__':
    unittest.main()
