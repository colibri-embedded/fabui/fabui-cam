import unittest
from unittest import TestCase
import numpy as np

# Unit under test
from img2gcode import laser_planner

from fabui.cam.common.raster import Raster
from fabui.cam.common.raster2drawing import ( 
    image2drawing, angle2line_params, line_bbox_intersec, line_length )

class TestRaster2Drawing(TestCase):
    
    def setUp(self):
        pass
        
    def __default_config(self, levels=1, invert=False):
        
        config = {
            "version": 2,
            "general" : {
                "dot_size": 0.1,
                "travel_feedrate": 10000,
                "off_during_travel": True,
                "optimize": 0,
                "levels": levels,
                "width": 100.0,
                "height": 12.0,
                "invert": invert
            },
            "layers" : [
                {
                    "name": "*",
                    "pwm": {
                        "type": "const",
                        "value": 128
                    },
                    "feedrate": {
                        "type": "const",
                        "value": 1000
                    },
                    "pattern": {
                        "type": "line",
                        "angle": 0,
                        "spacing": 1
                    }
                }
            ]
        }
        
        fresh_config = {}
        fresh_config.update(config)
        
        return fresh_config

    def test_raster2drawing_1px_top(self):
        config = self.__default_config()
        in_fn = './data/png/raster_1px_top.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn)
        
        drawing = image2drawing(raster, config)
    
        self.assertEqual( len(drawing.layers[0].primitives), 10 )
        self.assertEqual( drawing.width(), 1000 )
        self.assertEqual( drawing.height(), 9 )
    
    def test_raster2drawing_1px_bottom(self):
        config = self.__default_config()
        in_fn = './data/png/raster_1px_bottom.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 10 )
        self.assertEqual( drawing.width(), 1000 )
        self.assertEqual( drawing.height(), 9 )
    
    def test_raster2drawing_1px_off_center(self):
        config = self.__default_config()
        in_fn = './data/png/raster_1px_off_center_1px_high.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn, sharp_resize=True)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 10 )
        self.assertEqual( drawing.width(), 980 )
        self.assertEqual( drawing.height(), 9 )
    
    def test_raster2drawing_1px_off_center_1px_high(self):
        config = self.__default_config()
        in_fn = './data/png/raster_1px_off_center_1px_high.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn, sharp_resize=True)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 10 )
        self.assertEqual( drawing.width(), 980 )
        self.assertEqual( drawing.height(), 9 )
    
    def test_raster2drawing_1px_off_center_50px_high(self):
        config = self.__default_config()
        in_fn = './data/png/raster_1px_left_right.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 500 )
        self.assertEqual( drawing.width(), 980 )
        self.assertEqual( drawing.height(), 499 )
    
    def test_raster2drawing_50px_left(self):
        config = self.__default_config()
        in_fn = './data/png/raster_50px_left.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 500 )
        self.assertEqual( drawing.width(), 500 )
        self.assertEqual( drawing.height(), 499 )
        
        allowed_x = [0, 500]
        
        for prim in drawing.layers[0].primitives:
            pts = prim['points']
            self.assertEqual( line_length(pts[0], pts[1]), 500 )
            self.assertTrue( pts[0][0] in  allowed_x)
            self.assertTrue( pts[1][0] in  allowed_x)
    
    def test_raster2drawing_50px_right(self):
        config = self.__default_config()
        in_fn = './data/png/raster_50px_right.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 500 )
        self.assertEqual( drawing.width(), 500 )
        self.assertEqual( drawing.height(), 499 )
        
        allowed_x = [500, 1000]
        
        for prim in drawing.layers[0].primitives:
            pts = prim['points']
            self.assertEqual( line_length(pts[0], pts[1]), 500 )
            self.assertTrue( pts[0][0] in  allowed_x)
            self.assertTrue( pts[1][0] in  allowed_x)
    
    def test_raster2drawing_20px_stripes(self):
        config = self.__default_config()
        in_fn = './data/png/raster_20px_2strips.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 1000 )
        self.assertEqual( drawing.width(), 600 )
        self.assertEqual( drawing.height(), 499 )
        
        allowed_x = [200, 400, 600, 800]
        
        for prim in drawing.layers[0].primitives:
            self.assertTrue( prim['points'][0][0] in allowed_x)
    
    def test_raster2drawing_1px_45deg_line(self):
        config = self.__default_config()
        in_fn = './data/png/raster_1px_45deg_line.png'
        
        dpm = 1 / float(config['general']['dot_size'])
        width = config['general']['width']
        height = config['general']['height']
        
        raster = Raster(dpm, width, height)
        raster.loadFromFile(in_fn, sharp_resize=True)
        
        drawing = image2drawing(raster, config)
        
        self.assertEqual( len(drawing.layers[0].primitives), 500 )
        self.assertEqual( drawing.width(), 500 )
        self.assertEqual( drawing.height(), 499 )
        
        for prim in drawing.layers[0].primitives:
            pts = prim['points']
            self.assertEqual( line_length(pts[0], pts[1]), 10 )
            
if __name__ == '__main__':
    unittest.main()
