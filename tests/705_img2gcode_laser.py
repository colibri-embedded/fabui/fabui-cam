import unittest
from unittest import TestCase

# Unit under test
from img2gcode import laser_planner
from fabui.cam.gcode.writerbase import WriterBase

class OutpuWriter(WriterBase):
    
    def __init__(self):
        self.data = ''
        
    def write(self, v):
        self.data += v + '\r\n'
        
    def clear(self):
        self.data = ''
        
    def saveToFile(self, filename):
        with open(filename, 'w') as f:
            f.write(self.data)

class TestImg2GCodeLaser(TestCase):
    
    def setUp(self):
        pass
        
    def test_img2gcode_laser_planner(self):
        
        #~ in_fn = './data/jpg/mini_pcb_black_white_191x145.jpg'
        in_fn = './data/png/raster_1px_bottom.png'
        out_fn = '/tmp/output.gcode'
        
        config = {
            "version": 2,
            "general" : {
                "dot_size": 0.1,
                "travel_feedrate": 10000,
                "off_during_travel": True,
                "levels": 1,
                "width": 100.0,
                "height": 12.0,
                "invert": True,
                "align": "top-left"
            },
            "layers" : [
                {
                    "name": "*",
                    "pwm": {
                        "type": "const",
                        "value": 128
                    },
                    "feedrate": {
                        "type": "const",
                        "value": 1000
                    },
                    "pattern": {
                        "type": "line",
                        "angle": 0,
                        "spacing": 1
                    }
                }
            ]
        }
        
        output = OutpuWriter()
        
        laser_planner(in_fn, output, config)
        
        output.saveToFile(out_fn)

if __name__ == '__main__':
    unittest.main()
