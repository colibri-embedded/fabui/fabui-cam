import subprocess
import json
import unittest
from unittest import TestCase

# Unit under test
from vector2gcode import laser_planner
from fabui.cam.gcode.writerbase import WriterBase

class OutpuWriter(WriterBase):
    
    def __init__(self):
        self.data = ''
        
    def write(self, v):
        self.data += v + '\r\n'
        
    def clear(self):
        self.data = ''
        
    def saveToFile(self, filename):
        with open(filename, 'w') as f:
            f.write(self.data)

class TestVector2GCodeLaser(TestCase):
    
    def setUp(self):
        pass

    def __default_config(self, layer_name="Default"):
        return {
            "version": 2,
            "general" : {
                 "dot_size": 0.1,
                 "travel_feedrate": 10000,
                 "off_during_travel": True,
                 "optimize": 2,
                 "levels": 1,
                 "width": 100.0,
                 "height": 12.0,
                 "invert": False,
                 "align": "top-left",
                 "scale": 1.0
            },
            "layers" : [
                {
                    "name": layer_name,
                    "pwm": {
                        "type": "const",
                        "value": 255
                    },
                    "feedrate": {
                        "type": "const",
                        "value": 500
                    },
                    "cut": {
                        "type": "continuous",
                        "depth": 0.0,
                        "steps": 1,
                        "passes": 1,
                        "sequential": False
                    }
                }
            ]
        }
        
    def test_vector2gcode_plan(self):

        in_fn = './data/dxf/dxf_default.dxf'
        out_fn = '/tmp/output.gcode'
        
        config = self.__default_config()

        output = OutpuWriter()

        laser_planner(in_fn, output, config)
        
        output.saveToFile(out_fn)

    def test_vector2gcode_cli_run_success(self):
        cfg_fn  = '/tmp/test_config.json'
        in_fn   = './data/dxf/dxf_default.dxf'
        out_fn  = '/tmp/output.gcode'

        config = self.__default_config()
        with open(cfg_fn, 'w') as f:
            content = json.dumps(config)
            f.write(content)

        cmd = '../vector2gcode.py'
        cmd += ' -P laser'
        cmd += ' ' + cfg_fn
        cmd += ' ' + in_fn
        cmd += ' -o ' + out_fn

        exit_code = subprocess.call(cmd.split())
        self.assertEquals(0, exit_code, "Exit code is not 0")


if __name__ == '__main__':
    unittest.main()
