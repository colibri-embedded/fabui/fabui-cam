import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.gcode.laser25d import Laser25D
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.gcode.cnc import CNC
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = []
    
    def write(self, data):
        self.data.append(data)
    
    def clear(self):
        self.data = []
    
    def __str__(self):
        return '|'.join(self.data)

class TestLaser25D(TestCase):
    
    def setUp(self):
        pass
    
    def __default(self, config={}, ndigits=3, compact=False, position=None):
        output = OneWriteOutput()
        cnc = Laser25D(output, config, ndigits=ndigits, compact=compact)
        
        if position is not None:
            if len(position) == 2:
                cnc.curX = float(position[0])
                cnc.curY = float(position[1])
            if len(position) == 3:
                cnc.curZ = float(position[2])
        
        return output, cnc
        
    def test_laser25d_mill_finish(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.millTo(10,11)
        cnc.finish()
        self.assertEqual(str(output), 'M61S255|G1X10Y11F100|; Finish laser engraving|M62|M400')
        
    def test_laser25d_mill_cutdepth_finish(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.setCutDepth(5)
        cnc.millTo(10,11)
        cnc.finish()
        self.assertEqual(str(output), 'M61S255|M62|G0Z-5F200|M61S255|G1X10Y11F100|; Finish laser engraving|M62|M400')
        
    def test_laser25d_mill_travel_finish(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.millTo(10,11)
        cnc.travelTo(12,14)
        cnc.finish()
        self.assertEqual(str(output), 'M61S255|G1X10Y11F100|M62|G0X12Y14F200|; Finish laser engraving|M400')
        
    def test_laser25d_mill_cutdepth_travel_finish(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.setCutDepth(5)
        cnc.millTo(10,11)
        cnc.travelTo(12,14)
        cnc.finish()
        self.assertEqual(str(output), 'M61S255|M62|G0Z-5F200|M61S255|G1X10Y11F100|M62|G0X12Y14F200|; Finish laser engraving|M400')
        
    def test_laser25d_mill_finish_return_home(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.millTo(10,11)
        cnc.finish(return_home=True)
        self.assertEqual(str(output), 'M61S255|G1X10Y11F100|; Finish laser engraving|M62|G0X0Y0F200|M400')
        
    def test_laser25d_mill_travel_finish_return_home(self):
        output, cnc = self.__default(compact=True)
        
        cnc.laserOn()
        cnc.millTo(10,11)
        cnc.travelTo(12,14)
        cnc.finish(return_home=True)
        self.assertEqual(str(output), 'M61S255|G1X10Y11F100|M62|G0X12Y14F200|; Finish laser engraving|G0X0Y0|M400')
        

        
if __name__ == '__main__':
    unittest.main()
