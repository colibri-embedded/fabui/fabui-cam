import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.slicer.slic3r import ConfigConverter

class TestSlic3rConfigConverter(TestCase):
    
    def setUp(self):
        # print
        pass

    def test_new_object(self):
        cc = ConfigConverter()

        self.assertTrue( isinstance(cc, ConfigConverter) )

    def test_flatten_keys(self):
        cfg = {
            'slvl0' : 'str_value',
            'ilvl0' : 3,
            'flvl0' : 0.56,
            'alvl0' : [1,2,3,4],
            'dlvl0' : {
                'slvl1': 'str_value2',
                'ilvl1': 4,
                'flvl1': 0.57,
                'alvl1': [5,6,7,9],
                'dlvl1': {
                    'slvl2': 'str_value2'
                }
            }
        }
        cc = ConfigConverter(cfg)

        flat = cc.flattenedKeys()

        self.assertEquals(flat, {   
            'alvl0': [1, 2, 3, 4],
            'dlvl0.alvl1': [5, 6, 7, 9],
            'dlvl0.dlvl1.slvl2': 'str_value2',
            'dlvl0.flvl1': 0.57,
            'dlvl0.ilvl1': 4,
            'dlvl0.slvl1': 'str_value2',
            'flvl0': 0.56,
            'ilvl0': 3,
            'slvl0': 'str_value'
        })

    def test_convert_svg(self):

        cfg = {
            "output": {
                "svg": True
            },
            "layers": {
                "height": 0.1
            }
        }

        cc = ConfigConverter(cfg)
        errors, config, args = cc.convert()

        self.assertEquals(0, len(errors))
        self.assertEquals(config, 'layer_height = 0.1\n')
        self.assertEquals(args, ' --export-svg')

        errors, config, args = cc.convert(cli_only=True)
        self.assertEquals(0, len(errors))
        self.assertEquals(config, '')
        self.assertEquals(args, ' --layer-height 0.1 --export-svg')


if __name__ == '__main__':
    unittest.main()
