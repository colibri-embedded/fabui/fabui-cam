import unittest
from unittest import TestCase
from math import sqrt

# Unit under test
from fabui.cam.common.drawing2d import Drawing2D, MissingLayerError

class TestDrawing2D(TestCase):
    
    def setUp(self):
        pass
        
    def test_add_ellipse_horizontal(self):
        d = Drawing2D()
        
        p_center = (0,0)
        p_axis   = (10.0,0.0)
        p_ratio  = 0.5
        
        (x,y,w,h) = d.addEllipse(p_center, p_axis, p_ratio)
        
        self.assertEqual( w, p_axis[0]*2 + p_axis[1]*2*p_ratio )
        self.assertEqual( h, p_axis[0]*2*p_ratio + p_axis[1]*2 )
        
        self.assertEqual( len(d.layers[0].primitives), 1 )

        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'ellipse' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['major_axis'], p_axis )
        self.assertEqual( prim['ratio'], p_ratio )
        self.assertEqual( prim['start'], 0.0 )
        self.assertEqual( prim['end'], 360.0 )
        
    def test_add_ellipse_vertical(self):
        d = Drawing2D()
        
        p_center = (0,0)
        p_axis   = (0.0,10.0)
        p_ratio  = 0.5
        
        (x,y,w,h) = d.addEllipse(p_center, p_axis, p_ratio)
        
        self.assertEqual( w, p_axis[0]*2 + p_axis[1]*2*p_ratio )
        self.assertEqual( h, p_axis[0]*2*p_ratio + p_axis[1]*2 )
        
        self.assertEqual( len(d.layers[0].primitives), 1 )

        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'ellipse' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['major_axis'], p_axis )
        self.assertEqual( prim['ratio'], p_ratio )
        self.assertEqual( prim['start'], 0.0 )
        self.assertEqual( prim['end'], 360.0 )
        
    def test_add_ellipse_param(self):
        d = Drawing2D()
        
        p_center = (0,0)
        p_axis   = (0.0,10.0)
        p_ratio  = 0.5
        
        (x,y,w,h) = d.addEllipse(major_axis=p_axis, center=p_center, ratio=p_ratio, filled=True)
        
        self.assertEqual( w, p_axis[0]*2 + p_axis[1]*2*p_ratio )
        self.assertEqual( h, p_axis[0]*2*p_ratio + p_axis[1]*2 )
        
        self.assertEqual( len(d.layers[0].primitives), 1 )

        prim = d.layers[0].primitives[0]
        self.assertEqual( prim['type'], 'ellipse' )
        self.assertEqual( prim['filled'], True )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['major_axis'], p_axis )
        self.assertEqual( prim['ratio'], p_ratio )
        self.assertEqual( prim['start'], 0.0 )
        self.assertEqual( prim['end'], 360.0 )
        
    def test_add_ellipse_custom_layer(self):
        d = Drawing2D()
        
        layer, idx = d.addLayer("Second")

        p_center = (0,0)
        p_axis   = (0.0,10.0)
        p_ratio  = 0.5
        
        (x,y,w,h) = d.addEllipse(major_axis=p_axis, center=p_center, ratio=p_ratio, layer=idx)
        
        self.assertEqual( w, p_axis[0]*2 + p_axis[1]*2*p_ratio )
        self.assertEqual( h, p_axis[0]*2*p_ratio + p_axis[1]*2 )
        
        self.assertEqual( len(d.layers[idx].primitives), 1 )

        prim = d.layers[idx].primitives[0]
        self.assertEqual( prim['type'], 'ellipse' )
        self.assertEqual( prim['filled'], False )
        self.assertEqual( prim['center'], p_center )
        self.assertEqual( prim['major_axis'], p_axis )
        self.assertEqual( prim['ratio'], p_ratio )
        self.assertEqual( prim['start'], 0.0 )
        self.assertEqual( prim['end'], 360.0 )
        
    def test_add_ellipse_missing_layer_error(self):
        d = Drawing2D()
        
        p_center = (0,0)
        p_axis   = (0.0,10.0)
        p_ratio  = 0.5
        
        with self.assertRaises(MissingLayerError) as context:
            d.addEllipse(p_center, p_axis, p_ratio, layer=5)
        
        self.assertTrue('Layer #5 does not exist' in str(context.exception))
        
if __name__ == '__main__':
    unittest.main()
