import unittest
from unittest import TestCase

# Unit under test
from fabui.cam.gcode.raw import RAW
from fabui.cam.gcode.exceptions import ParameterError

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_M92(self):
        self.__default_config()
        
        self.raw.M92()
        self.assertEqual(str(self.output), 'M92')
        
    def test_M92_x(self):
        self.__default_config()
        
        self.raw.M92(10)
        self.assertEqual(str(self.output), 'M92 X10')
        
    def test_M92_x_param(self):
        self.__default_config()
        
        self.raw.M92(X=10)
        self.assertEqual(str(self.output), 'M92 X10')
        
    def test_M92_xy(self):
        self.__default_config()
        
        self.raw.M92(10, 11)
        self.assertEqual(str(self.output), 'M92 X10 Y11')
        
    def test_M92_y_param(self):
        self.__default_config()
        
        self.raw.M92(Y=11)
        self.assertEqual(str(self.output), 'M92 Y11')
        
    def test_M92_xyz(self):
        self.__default_config()
        
        self.raw.M92(10, 11, 12)
        self.assertEqual(str(self.output), 'M92 X10 Y11 Z12')
        
    def test_M92_z_param(self):
        self.__default_config()
        
        self.raw.M92(Z=10)
        self.assertEqual(str(self.output), 'M92 Z10')
        
    def test_M92_e_param(self):
        self.__default_config()
        
        self.raw.M92(E=13)
        self.assertEqual(str(self.output), 'M92 E13')
        
    def test_M92_xyz_param(self):
        self.__default_config()
        
        self.raw.M92(Z=12, Y=11, X=10)
        self.assertEqual(str(self.output), 'M92 X10 Y11 Z12')
        
    def test_M92_xyz_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M92(Z=12, Y=11, X=10)
        self.assertEqual(str(self.output), 'M92X10Y11Z12')
        
    def test_M92_xyze_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M92(Z=12, Y=11, X=10, E=13)
        self.assertEqual(str(self.output), 'M92X10Y11Z12E13')
        
    def test_M201_xyze(self):
        self.__default_config()
        
        self.raw.M201(Z=12, Y=11, X=10, E=13)
        self.assertEqual(str(self.output), 'M201 X10 Y11 Z12 E13')
        
    def test_M203_xyze(self):
        self.__default_config()
        
        self.raw.M203(Z=12, Y=11, X=10, E=13)
        self.assertEqual(str(self.output), 'M203 X10 Y11 Z12 E13')
        
    def test_M204_s(self):
        self.__default_config()
        
        self.raw.M204(100)
        self.assertEqual(str(self.output), 'M204 S100')
        
    def test_M204_s_param(self):
        self.__default_config()
        
        self.raw.M204(S=100)
        self.assertEqual(str(self.output), 'M204 S100')
        
    def test_M204_st(self):
        self.__default_config()
        
        self.raw.M204(100,200)
        self.assertEqual(str(self.output), 'M204 S100 T200')
        
    def test_M204_t_param(self):
        self.__default_config()
        
        self.raw.M204(T=200)
        self.assertEqual(str(self.output), 'M204 T200')

    def test_M204_st_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M204(100,200)
        self.assertEqual(str(self.output), 'M204S100T200')
        
    def test_M205_s(self):
        self.__default_config()
        
        self.raw.M205(100)
        self.assertEqual(str(self.output), 'M205 S100')
        
    def test_M205_s_param(self):
        self.__default_config()
        
        self.raw.M205(S=100)
        self.assertEqual(str(self.output), 'M205 S100')
        
    def test_M205_t_param(self):
        self.__default_config()
        
        self.raw.M205(T=100)
        self.assertEqual(str(self.output), 'M205 T100')
        
    def test_M205_b_param(self):
        self.__default_config()
        
        self.raw.M205(B=100)
        self.assertEqual(str(self.output), 'M205 B100')
        
    def test_M205_xy_param(self):
        self.__default_config()
        
        self.raw.M205(XY=100)
        self.assertEqual(str(self.output), 'M205 X100')
        
    def test_M205_z_param(self):
        self.__default_config()
        
        self.raw.M205(Z=100)
        self.assertEqual(str(self.output), 'M205 Z100')
        
    def test_M205_e_param(self):
        self.__default_config()
        
        self.raw.M205(E=100)
        self.assertEqual(str(self.output), 'M205 E100')
        
    def test_M205_stbxyze(self):
        self.__default_config()
        
        self.raw.M205(100, 200, 300, 400, 500, 600)
        self.assertEqual(str(self.output), 'M205 S100 T200 B300 X400 Z500 E600')
        
    def test_M205_stbxyze_param(self):
        self.__default_config()
        
        self.raw.M205(S=100, T=200, B=300, XY=400, Z=500, E=600)
        self.assertEqual(str(self.output), 'M205 S100 T200 B300 X400 Z500 E600')
        
    def test_M205_stbxyze_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M205(S=100, T=200, B=300, XY=400, Z=500, E=600)
        self.assertEqual(str(self.output), 'M205S100T200B300X400Z500E600')
        
    def test_M206_x(self):
        self.__default_config()
        
        self.raw.M206(10.5)
        self.assertEqual(str(self.output), 'M206 X10.5')
        
    def test_M206_x_param(self):
        self.__default_config()
        
        self.raw.M206(X=10.5)
        self.assertEqual(str(self.output), 'M206 X10.5')
        
    def test_M206_xy(self):
        self.__default_config()
        
        self.raw.M206(10.5, 11.5)
        self.assertEqual(str(self.output), 'M206 X10.5 Y11.5')
        
    def test_M206_y_param(self):
        self.__default_config()
        
        self.raw.M206(Y=10.5)
        self.assertEqual(str(self.output), 'M206 Y10.5')
        
    def test_M206_z_param(self):
        self.__default_config()
        
        self.raw.M206(Z=10.5)
        self.assertEqual(str(self.output), 'M206 Z10.5')
        
    def test_M206_xyz(self):
        self.__default_config()
        
        self.raw.M206(10.5, 11.5, 12.5)
        self.assertEqual(str(self.output), 'M206 X10.5 Y11.5 Z12.5')
        
    def test_M206_xyz_compact(self):
        self.__default_config(compact=True)
        
        self.raw.M206(X=10.5, Y=11.5, Z=12.5)
        self.assertEqual(str(self.output), 'M206X10.5Y11.5Z12.5')
        
if __name__ == '__main__':
    unittest.main()
