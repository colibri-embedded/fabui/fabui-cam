import unittest
from unittest import TestCase
#from mock import patch, Mock, PropertyMock

# Unit under test
from fabui.cam.gcode.raw import RAW

class OneWriteOutput:
    """
    Output buffer writer.
    """
    
    def __init__(self):
        self.data = ''
    
    def write(self, data):
        self.data = data
        
    def __str__(self):
        return self.data

class TestGCodeRaw(TestCase):
    """
    Testing Raw gcode generation functionality.
    """
    
    def setUp(self):
        self.output = OneWriteOutput()
        self.raw = RAW(self.output)

    def __default_config(self, ndigits=6, compact=False, no_comment=False):
        self.raw.ndigits = ndigits
        self.raw.compact = compact
        self.raw.no_comments = no_comment

    def test_comment(self):
        self.__default_config()
        
        self.raw.COMMENT('A test comment')
        self.assertEqual(str(self.output), '; A test comment')

    def test_no_comment(self):
        self.__default_config(no_comment=True)
        
        self.raw.COMMENT('A test comment')
        self.assertEqual(str(self.output), '')
        
        
if __name__ == '__main__':
    unittest.main()
