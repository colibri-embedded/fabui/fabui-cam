#!/usr/bin/env python
# # -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

# Import standard python module
import argparse
import time
import gettext
import json
import os

from fabui.cam.loaders.detect import detect_format
from fabui.cam.loaders import dxfgrabber
from fabui.cam.loaders.dxfgrabber import aci_to_true_color
from fabui.cam.loaders.dxfgrabber.color import TrueColor
from fabui.cam.common.drawing2d import Drawing2D

def main():
    # SETTING EXPECTED ARGUMENTS
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("filename",          help="File to check")
    
    # GET ARGUMENTS
    args = parser.parse_args()

    # INIT VARs
    filename = args.filename
    
    
    mime = detect_format(filename)
    extra = {}
    
    if mime == 'image/png' or mime == 'image/jpeg':
        pass
    elif mime == 'application/dxf':
        drawing = Drawing2D()
        drawing.loadFromDXF(filename)
        
        extra = {
            'layers': []
        }
        
        for l in drawing.layers:
            #~ print l.name, l.color
            
            tc = TrueColor.from_aci(l.color)
            
            layer = {
                'name': l.name,
                'color': {
                    'index': l.color,
                    'r': tc.r,
                    'g': tc.g,
                    'b': tc.b,
                    'hex': '#{:02X}{:02X}{:02X}'.format(tc.r, tc.g, tc.b)
                }
            }
            
            extra['layers'].append(layer)
            
    elif mime == 'application/sla':
        # STL
        pass
    elif mime == 'image/svg+xml':
        pass
    else:
        mime = 'unknown'
    
    result = {
        'mime': mime,
        'extra': extra
    }
    
    print json.dumps(result)
    
if __name__ == "__main__":
    main()
