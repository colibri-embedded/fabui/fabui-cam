#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

import os
import magic

def detect_format(filename):

    fn, ext = os.path.splitext(filename.lower())
    
    mime = magic.from_file(filename, mime=True)
    
    if mime == 'text/plain':
        if ext == '.dxf':
            mime = 'application/dxf'
    elif mime == 'application/octet-stream':
        if ext == '.stl':
            mime = 'application/sla'
    
    return mime
