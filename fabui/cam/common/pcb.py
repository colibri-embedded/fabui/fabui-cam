#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

import numpy as np
import cv2

from fabui.cam.loaders import gerber
from fabui.cam.loaders.gerber.render import ShapelyContext

class PCB(object):

    def __init__(self, dpm=10.0):
        """
        @param dpm Dots per mm
        """
        self._pcb = None
        self._size = (0,0)
        self._bounds = ((0,0),(0,0))
        self._dpm = dpm

    def from_directory(self, directory, board_name=None, verbose=False):
        """
        """
        pcb = self._pcb = gerber.PCB.from_directory(directory, board_name, verbose)

        min_x = pcb.layers[0].bounds[0][0]
        max_x = pcb.layers[0].bounds[0][1]
        min_y = pcb.layers[0].bounds[1][0]
        max_y = pcb.layers[0].bounds[1][1]

        for layer in pcb.layers:
            if ( layer.layer_class in ['outline', 'top', 'bottom'] 
                 and not layer.mirrored ):
                min_x = min(layer.bounds[0][0], min_x)
                max_x = max(layer.bounds[0][1], max_x)
                min_y = min(layer.bounds[1][0], min_y)
                max_y = max(layer.bounds[1][1], max_y)

        self._bounds = ((min_x, max_x), (min_y, max_y))

        gwidth = abs(max_x - min_x)
        gheight = abs(max_y - min_y)
        self._size = (gwidth, gheight)

        print "Size:",self._size

    @property
    def bounds(self):
        return self._bounds

    @property
    def size_in_pixels(self):
        return ( self._size[0] * self._dpm,  self._size[1] * self._dpm )

    @property
    def size(self):
        return self._size

    @property
    def width(self):
        return self.size[0]

    @property
    def height(self):
        return self.size[1]

    @property
    def layers(self):
        """
        Return all PCB layers
        """
        return self._pcb.layers

    @property
    def copper_layers(self):
        """
        Return copper PCB layers
        """
        return self._pcb.copper_layers

    @property
    def drill_layers(self):
        """
        Return drill PCB layers
        """
        return self._pcb.drill_layers

    @property
    def outline_layers(self):
        """
        Return outline PCB layers
        """
        return self._pcb.outline_layers

    def map_to_pixels(self, point, scale=1.0):
        """
        Map point to pixel in raster image.

        @param point X,Y tuple
        @param scale Scaling factor

        @returns X,Y tuple
        """
        ox = self.bounds[0][0]
        oy = self.bounds[1][0]
        x,y = point

        x = x-ox
        y = y-oy

        x = int( round(x * self._dpm * scale) )
        y = int( round(y * self._dpm * scale) )

        return (x,y)

    def render_top_layers(self, exclude_holes=False):
        """
        Render top layers to a raster image
        
        @param use_drill Render drill holes

        @returns Raster representation of top layers
        """
        top_layers = []
        for layer in self._pcb.top_layers:
            if not layer.mirrored:
                top_layers.append(layer)

        return self.__render_layers(top_layers, exclude_holes=exclude_holes)

    def render_bottom_layers(self, exclude_holes=False):
        """
        Render bottom layers to a raster image

        @param use_drill Render drill holes

        @returns Raster representation of bottom layers
        """
        # Prepare layers
        bottom_layers = []
        for layer in self._pcb.bottom_layers:
            if layer.mirrored or layer.layer_class == 'bottom': # bottom mirrored flag is false
                bottom_layers.append(layer)

        return self.__render_layers(bottom_layers, flip=True, exclude_holes=exclude_holes)

    def __render_layers(self, layers, flip=False, exclude_holes=False):
        """
        Render input layers

        @param layers    Layers to render
        @param flip      Flip the resulting raster image by the X axis
        @param use_drill Render drill holes

        @returns Raster representation of input layers
        """
        width, height = self.size_in_pixels

        front = np.ones((int(height), int(width), 1), np.uint8)*255
        back = None
        #print "cv2:version",cv2.__version__
        _fill_scale_fix = 2.0

        for layer in layers:
            if layer.layer_class in ['top', 'bottom']:
                shp = ShapelyContext()
                layer.cam_source.render( shp )
                back = np.ones((int(height), int(width), 1), np.uint8)*255

                if layer.mirrored:
                    shp.mirror_x()

                for fig in shp.figs:
                    use_alpha = False
                    if fig.interiors:
                        # If there are interior polygons an extra layer to rendering is needed 
                        # so they don't overlap with the already rendered shapes
                        use_alpha = True
                    
                    points = []
                    for pt in fig.exterior.coords:
                        points.append( self.map_to_pixels(pt, _fill_scale_fix) )
                    
                    pts = np.array(points)
                    if use_alpha:
                        cv2.fillPoly(back, [pts], False, 0, 1)
                    else:
                        cv2.fillPoly(front, [pts], False, 0, 1)

                    for inte in fig.interiors:
                        items = []
                        if inte.geom_type == 'LinearRing':
                            items = inte.coords
                        elif inte.geom_type == 'Polygone':
                            items = inte.exterior.coords

                        points = []
                        for pt in items:
                            points.append( self.map_to_pixels(pt) )

                        if points:
                            pts = np.array(points)
                            cv2.fillPoly(back, [pts], 255)

        # Merge front and back render layers
        if back is not None:
            front = cv2.multiply(front, back)

        # Fill in the drill holes
        if not exclude_holes:
            for layer in layers:
                if layer.layer_class == 'drill':
                    for drill in layer.primitives:
                        p1 = self.map_to_pixels(drill.position)
                        r1 = int( round(drill.diameter * self._dpm * 0.5) )
                        cv2.circle(front, p1, r1, 255, cv2.FILLED)

        if not flip:
            front = cv2.flip(front,0)

        return front