#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

# Import external modules
import numpy as np
import cv2

class FileReadError(Exception):
    pass
    
class ArgumentError(Exception):
    pass

class Raster(object):
    """
    Raster image container.
    """

    def __init__(self, dpm=10, width=None, height=None, invert=False):
        """
        
        @param dpm    Dots per milimeter (used to scale the image on load)
        @param width  Target width in mm
        @param height Target height in mm
        @param invert Invert colors
        """
        
        if width is None and height is None:
            width = 100.0
            
        if width is not None and width != 0:
            height = None
        
        self.config = {
            "dpm": dpm,
            "invert": invert,
            "width": width,
            "height": height
        }
        
        self.__scaled_image = None
        self.__width = 0
        self.__height = 0

    def loadFromMemory(self, image, sharp_resize=False):
        """
        """
        
        invert           = self.config['invert']
        #~ invert           = False
        target_width_mm  = self.config['width']
        target_height_mm = self.config['height']
        dpm             = self.config['dpm']

        img = image
        
        img, alpha = self.__fix_alpha(img, invert)
        
        height_px, width_px = img.shape[:2]
        ratio = height_px / float(width_px)
        
        #~ print "WxH:", width_px, height_px
        #~ print "Ratio:", ratio
        #~ print "tgtWxH:", target_width_px, target_height_px
        
        scale = 1.0
        
        if target_width_mm is not None:
            target_width_px = int(target_width_mm * dpm)
            target_height_px = int(target_width_px * ratio)
            scale = target_width_px / float(width_px)
        elif target_height_mm is not None:
            target_height_px = int(target_height_mm * dpm)
            target_width_px = int(target_height_px / ratio)
            scale = target_height_px / float(height_px)
        else:
            raise ArgumentError("Width and height are not specified")
            
        # Resize image if needed
        #~ print "Scale:", scale
        if scale != 1:
            #~ print "Resize to {0}x{1}".format(target_width_px,target_height_px)
            img = cv2.resize(img, (target_width_px, target_height_px), interpolation= cv2.INTER_NEAREST if sharp_resize else cv2.INTER_CUBIC)
        
        #~ if crop:
            #~ crop = crop.split(',')
            #~ x1 = int(crop[0])
            #~ x2 = x1 + int(crop[2])
            #~ y1 = int(crop[1])
            #~ y2 = y1 + int(crop[3])
            #~ print "Crop {0} {1} {2} {3}".format(x1, x2, y1, y2)
            #~ img = img[y1:y2, x1:x2]
        
        img = cv2.flip(img,0)
        
        self.__scaled_image = img
        self.__width = target_width_px
        self.__height = target_height_px

    def loadFromFile(self, filename, sharp_resize=False):
        """
        Load an image an resize it to fit into target dpm and dimensions.
        
        @param filename Image filename (png or jpeg)
        """
        image = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
        
        # Convert black&white 1 channel image to 3 channels
        if len(image.shape) == 2:
            image = cv2.imread(filename, cv2.IMREAD_COLOR)
        
        if image is None:
            raise FileReadError("Cannot open '{0}' image file.".format(filename))

        self.loadFromMemory(image, sharp_resize)

    def getImage(self):
        """
        Loaded image object.
        """
        return self.__scaled_image
        
    def width(self):
        """
        Loaded image width in pixels.
        """
        return self.__width
        
    def height(self):
        """
        Loaded image height in pixels.
        """
        return self.__height

    def __fix_alpha(self, img, invert):
        """
        Fix alpha channel data.
        
        @param img
        @param invert
        """
        
        height, width, c = img.shape
        if c < 4:
            return img, None
            
        img2 = cv2.split(img)
        img3 = np.ones((int(height), int(width), 3), np.uint8)*255

        for y in range(width):
            for x in range(height):
                if not invert:
                    img3[x][y][0] = (int(img2[0][x][y]) * int(img2[3][x][y])) / 255
                    img3[x][y][1] = (int(img2[1][x][y]) * int(img2[3][x][y])) / 255
                    img3[x][y][2] = (int(img2[2][x][y]) * int(img2[3][x][y])) / 255
                else:
                    img3[x][y][0] = (int(img2[0][x][y]) * int(img2[3][x][y])) / 255 + (255-int(img2[3][x][y]))
                    img3[x][y][1] = (int(img2[1][x][y]) * int(img2[3][x][y])) / 255 + (255-int(img2[3][x][y]))
                    img3[x][y][2] = (int(img2[2][x][y]) * int(img2[3][x][y])) / 255 + (255-int(img2[3][x][y]))
                
        return img3, img2[3]
        
    def createLevels(self, num_of_levels):
        """
        
        @param num_of_levels
        """
        
        if num_of_levels > 1:
            return self.__multiple_levels(num_of_levels)
        else:
            return self.__black_n_white()
        
    def __black_n_white(self, threshold=127):
        """
        Split image into a single black and white level.
        
        @param threshold Pixel black/white threshold value
        """
        levels = []
        img = self.__scaled_image
        invert = self.config['invert']
        
        cv2.imwrite('input_bw.png', img)
        
        t = cv2.THRESH_BINARY
        if invert:
            t = cv2.THRESH_BINARY_INV
        
        ACTIVE_LABEL = 1
        
        gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret,mask = cv2.threshold(gray_image, threshold, ACTIVE_LABEL, t)
        
        tmp = mask * 255
        data = tmp.reshape( (self.__height,self.__width,1) )
        
        levels.append({
            'data': data,
            'value': 0
        })
        
        return levels
    
    def __multiple_levels(self, num_of_levels):
        """
        Split image into multiple gray level images.
        
        @param num_of_levels
        """
        levels = []
        
        img = self.__scaled_image
        invert = self.config['invert']
        
        cv2.imwrite('input_ml.png', img)
        
        # ?
        Z = img.reshape((-1,3))
        # Convert to np.float32
        Z = np.float32(Z)
        # Define criteria, number of clusters(K) and apply kmeans()
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        # Define number of levels
        ret,label,center = cv2.kmeans(Z, num_of_levels, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
        
        shades_of_gray = np.ndarray( (center.shape[0], 1) , dtype=int)
        i = 0
        for c in center:
            x = c[0] * 0.3 + c[1] * 0.59 + c[2] * 0.11
            if invert:
                x = 255 - int(x)
            shades_of_gray[i] = int(x)
            i += 1
        
        flat_labels = label.flatten()
        
        res = shades_of_gray[flat_labels]
        res2 = res.reshape( (self.__height,self.__width,1) )
        img = res2

        sorted_geays = shades_of_gray.copy()
        sorted_geays.sort(axis=0)    
        mapped = range(shades_of_gray.shape[0])
        rmapped = range(shades_of_gray.shape[0])
        
        for i in xrange(sorted_geays.shape[0]):
            value = sorted_geays[i]
            idx = np.nonzero(shades_of_gray == value)
            try:
                j = int(idx[0])
                mapped[i] = j
                rmapped[j] = i
            except Exception as e:
                print str(e)

        # Create layer images
        levels = []

        for lvl in xrange(num_of_levels):
            
            idx = mapped[lvl]
            
            value = int(shades_of_gray[ idx ])
            
            mask = np.zeros((shades_of_gray.shape), dtype=np.int)
            mask[ idx ] = 255
            
            res = mask[flat_labels]
            res2 = res.reshape( (self.__height,self.__width,1) )

            res2 = 255 - res2

            levels.append({
                'data': res2,
                'value': value
            })

        return levels
