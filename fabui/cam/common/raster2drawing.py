#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

import numpy as np
from fabui.cam.common.drawing2d import Drawing2D

def line_length(a, b):
    """
    Calculate line length.
    
    @param a First point tuple (x,y)
    @param b Second point tuple (x,y)
    
    @returns line length
    """
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    
    length = float( np.sqrt( dx*dx + dy*dy ) )
    return length

def ray2vector(img, x0, y0, x1, y1, ACTIVE = 0):
    """
    Convert a pixel line into a vector.
    
    @param img    Image data
    @param x0     Line start x
    @param y0     Line start y
    @param x1     Line end x
    @param y1     Line end y
    @param ACTIVE Which pixel value to consider as active
    
    @returns list of lines
    """
    x0 = int(x0)
    y0 = int(y0)
    x1 = int(x1)
    y1 = int(y1)
    h,w = img.shape[:2]
    
    #~ print x0, y0, x1, y1
    
    lines = []
    
    dx = abs(x1 - x0)    # distance to travel in X
    dy = abs(y1 - y0)    # distance to travel in Y
    
    if x0 < x1:
        ix = 1           # x will increase at each step
    else:
        ix = -1          # x will decrease at each step

    if y0 < y1:
        iy = 1           # y will increase at each step
    else:
        iy = -1          # y will decrease at each step
    
    if dx != 0:
        slope_y = dy / float(dx)
        slope_x = 1.0
    else:
        slope_y = 1.0
        slope_x = 0.0
    
    r = int( max(dx, dy) ) +1
    
    xs = int(x0)
    ys = int(y0)
    old_pixel = img[ys, xs][0]
    p_start = (x0, y0)
    p_end = (x0, y0)
    
    for i in range(r):
        
        x1 = round(x0 + i*ix*slope_x)
        y1 = round(y0 + i*iy*slope_y)
        
        x2 = round(x0 + (i+1)*ix*slope_x)
        y2 = round(y0 + (i+1)*iy*slope_y)

        x = int(x1)
        y = int(y1)
        
        new_pixel = img[y, x][0]
        
        if new_pixel != old_pixel:
            if line_length(p_start, p_end) > 0:
                line = [ p_start, p_end ]
                
                if old_pixel == ACTIVE:
                    lines.append(line)
            
            p_start = (x1, y1)
            
        old_pixel = new_pixel
        p_end = (x2, y2)
    
    # Finalize conversion in case the last pixel was an active one
    # This is needed as the final pixel value change is not detected in this corner case
    if line_length(p_start, p_end) > 0:
        if old_pixel == ACTIVE:
            line = [ p_start, p_end ]
            lines.append(line)
    
    return lines

def line_bbox_intersec(width, height, k, c):
    """
    Get clip points from line(k,c) and a bounding box of (width x height)
    
    @param width
    @param height
    @param k
    @param c
    
    @returns points
    """
    # 0 -- 1
    # |    |
    # |    |
    # 3 -- 2
    
    # x = (c2 - c1) / (k1 - k2)
    # y = k1*(c2-c1)/(k1-k2) + c1
    
    k03 = np.inf
    c03 = 0
    
    k12 = np.inf
    c12 = width
    
    k01 = 0
    c01 = height
    
    k32 = 0
    c32 = 0
    
    if  k == 0:
        """ horizontal line """
        if c >= 0 and c <= height:
            return [ (0,c), (width,c) ]
        else:
            return []
    elif k == np.inf:
        """ vertical line """
        if c >= 0 and c <= width:
            return [ (c,0), (c,height) ]
        else:
            return []
    else:
        """ line at an angle (not 0 or 90) """
        x03 = 0
        y03 = int(0 + c)
        
        x12 = width
        y12 = int( round(x12 * k + c) )
        
        y01 = height
        x01 = int( round((y01 - c) / float(k)) )

        y32 = 0
        x32 = int( round((y32 - c) / float(k)) )
        
        points = []
        
        if y03 >= 0 and y03 <= height:
            pt = (x03,y03)
            if pt not in points:
                points.append( pt )

        if x01 >= 0 and x01 <= width:
            pt = (x01,y01)
            if pt not in points:
                points.append( pt )

        if y12 >= 0 and y12 <= height:
            pt = (x12,y12)
            if pt not in points:
                points.append( pt )
                
        if x32 >= 0 and x32 <= width:
            pt = (x32,y32)
            if pt not in points:
                points.append( pt )

    if len(points) != 2 and len(points) != 0:
        print "R:", points
        
    if len(points) == 3:
        del points[2]
        
    return points
    
def angle2line_params(angle, width, height):
    """
    Return line parameters for y=kx+c formula.
    
    @param angle  Line angle
    @param width  Max width
    @param height Max height
    
    @returns (k, c, c_max)
    """
    if angle == 90 or angle == -90:
        k = np.inf
        c = 0.0
        c_max = width
    else:
        k = round( np.tan(np.deg2rad(angle)), 8 )
        #~ c = y0 - k*x0
        c = round(-np.abs(k)*width,6)
        
        if c == -0.0:
            c = 0.0
        
        c_max = height +abs(c)
        
    return k, c, c_max

def reverse_lines(lines):
    """
    Reverse order of line list and the line coordinates.
    """
    new_lines = []
    
    for line in lines:
        new_line = [ line[1], line[0] ]
        new_lines.insert(0, new_line)
    
    return new_lines

def raster2vector(img, pattern = 'line', angle = 0, spacing = 1, invert = False):
    """
    Convert a raster image to vector data
    
    @param img     Image data
    @param pattern Vectorising pattern (line or cross)
    @param angle   Slice angle
    @param spacing Slice spacing (distance is dot_size * spacing)
    @param invert  Invert active pixel logic
    """
    height, width = img.shape[:2]
    x0,y0 = (0.0,0.0)
    ACTIVE = 0 if not invert else 255
    lines = []
    
    width -= 1
    height -= 1
    
    if pattern in ['line', 'cross']:
    
        k, c, c_max = angle2line_params(angle, width, height)
        reverse = False
        c_max += 1 # fix for -1 on width/height
        
        for i in np.arange(c, c_max, spacing):
            points = line_bbox_intersec(width, height, k, int(i) )
            
            if len(points) == 2:
                pt0 = points[0]
                pt1 = points[1]
                
                new_lines = ray2vector(img, pt0[0], pt0[1], pt1[0], pt1[1], ACTIVE )
                if reverse:
                    new_lines = reverse_lines(new_lines)
                    
                if new_lines:
                    lines.extend( new_lines )

                reverse = not reverse

    if pattern == 'cross':
        
        k, c, c_max = angle2line_params(angle+90, width, height)
        reverse = False
        c_max += 1 # fix for -1 on width/height
        
        for i in np.arange(c, c_max, spacing):
            
            points = line_bbox_intersec(width, height, k, int(i) )
            if len(points) == 2:
                pt0 = points[1]
                pt1 = points[0]
                
                new_lines = ray2vector(img, pt0[0], pt0[1], pt1[0], pt1[1], ACTIVE )
                if reverse:
                    new_lines = reverse_lines(new_lines)
                    
                if new_lines:
                    lines.extend( new_lines )
                    
                reverse = not reverse

    return lines

def image2drawing(raster, config):
    """
    Convert a raster image to a drawing.
    
    @param raster Raster object
    @param config Configuration
    """
    
    num_of_levels = config['general']['levels']
    levels = raster.createLevels(num_of_levels)
    #~ invert = config['general']['invert']
    invert = False
    
    new_config = {}
    new_config.update(config)
    
    all_layer_config = new_config['layers'].pop(0)
    all_layer_config['cut'] = {
        "type": "continuous",
        "depth": 0.0,
        "steps": 1,
        "sequential": False
    }
    
    drawing = Drawing2D()
    drawing.clear()
    
    idx = 0
    for level in levels:
        
        new_layer_config = {}
        new_layer_config.update(all_layer_config)
        new_layer_config['name'] = 'grayscale_{0}'.format(idx)
        
        new_config['layers'].append(new_layer_config)
        
        layer, lyr_idx = drawing.addLayer(new_layer_config['name'], level['value'])
        
        pattern = new_layer_config['pattern']
        
        #~ import cv2
        #~ cv2.imshow('debug', level['data'])
        #~ cv2.waitKey(0)
        #~ cv2.destroyAllWindows()
        
        lines = raster2vector(level['data'], pattern['type'], pattern['angle'], int(pattern['spacing']), invert=invert )
        
        for line in lines:
            drawing.addLine(line[0], line[1], lyr_idx)

        idx += 1
        
    return drawing
