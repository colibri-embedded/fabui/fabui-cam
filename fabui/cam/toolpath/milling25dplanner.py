#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

from .simple25dplanner import Simple25DPlanner
from fabui.cam.gcode.filewriter import FileWriter, WriterBase
from fabui.cam.gcode.milling25d import Milling25D

class Milling25DPlanner(Simple25DPlanner):
    
    def __init__(self, output, config, cnc=None, ndigits=4, compact=True, no_comment=False):
        """
        """
        if isinstance(output, str):
            self.output = FileWriter(output)
        elif isinstance(output, WriterBase):
            self.output = output
        
        self.config = config
        
        travel_feedrate = 10000
        safe_z = 5.0
        plunge_rate = 10
        retract_rate = 30
        
        if 'general' in config:
            if 'travel_feedrate' in config['general']:
                travel_feedrate = config['general']['travel_feedrate']
            if 'safe_z' in config['general']:
                safe_z = config['general']['safe_z']
            if 'plunge_rate' in config['general']:
                plunge_rate = config['general']['plunge_rate']
            if 'retract_rate' in config['general']:
                retract_rate = config['general']['retract_rate']
        else:
            self.config['general'] = {
                'zero_cut': False,
                'travel_feedrate': travel_feedrate,
                'safe_z': safe_z,
                'plunge_rate': plunge_rate,
                'retract_rate': retract_rate
            }
        if 'zero_cut' not in self.config['general']:
            self.config['general']['zero_cut'] = False
        
        cnc_config = {
            'travel-feedrate':  travel_feedrate,
            'plunge-rate':      plunge_rate,
            'retract-rate':     retract_rate,
            'cut-depth':        0.0,
            'safe-z':           safe_z
        }
        
        if cnc is None:
            cnc = Milling25D(self.output, cnc_config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        super(Milling25DPlanner, self).__init__(cnc=cnc)
    
    def apply_settings(self, layer, layer_config):
        """
        
        @param layer
        @param layer_config
        """
        speed = layer_config['speed']
        self.cnc.setSpindleSpeed(speed['value'])

