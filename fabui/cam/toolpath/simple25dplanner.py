#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

import numpy as np

from fabui.cam.gcode.filewriter import FileWriter, WriterBase
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.common.drawing2d import Drawing2D, Layer2D
from fabui.cam.toolpath.exceptions import LayerNotFound

class Simple25DPlanner(object):
    """
    Simple 2.5D planner that converts vector paths to toolpaths without taking account of the tool diameter.
    """
    
    def __init__(self, output=None, config=None, cnc=None, ndigits=4, compact=True, no_comment=False):
        """
        """
        if cnc is None:
            if isinstance(output, str):
                self.output = FileWriter(output)
            elif isinstance(output, WriterBase):
                self.output = output
            else:
                print "ERRRROR", type(output)
                
            if config is None:
                config = {}
                
            self.cnc = CNC25D(self.output, config=config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        else:
            self.cnc = cnc
            
        if config is not None:
            self.config = {}
            self.config.update(config)
            
        self.value_map = {
            'const': self.value_map_const,
            'linear': self.value_map_linear,
        }
    
    def value_map_const(self, in_value, config):
        """
        """
        out_value = config['value']
        return out_value
        
    def value_map_linear(self, in_value, config):
        """

        """
        x_min = int(config['in_min'])
        x_max = int(config['in_max'])
        y_min = int(config['out_min'])
        y_max = int(config['out_max'])
        
        try:
            dx = float(x_max - x_min)
            dy = float(y_max - y_min)
            k = dy / dx

            y = 0

            if in_value >= x_min and in_value < x_max:
                y = y_min + (in_value-x_min) * k
                
            if in_value >= x_max:
                y = y_max
            
            if in_value < x_min:
                y = y_min
            
        except Exception as e:
            print "value_map_linear:", str(e)
            y = y_max

        return int(y)
    
    def sort_elements(self, elements, sort_list = [], reverse_list = [], use_reverse = False):
        """
        """
        if len(elements) == 0:
            return [], []
        
        while len(sort_list) != len(elements):
        
            if len(sort_list) == 0:
                cur_x = 0.0
                cur_y = 0.0
            else:
                last = sort_list[-1]
                if last in reverse_list:
                    p0 = elements[last]['points'][0]
                else:
                    p0 = elements[last]['points'][-1]
                cur_x = p0[0]
                cur_y = p0[1]
        
            closest = None
            reverse = None
            dist = 1e20
        
            for i in range( len(elements) ):
                            
                if i not in sort_list:
                    e = elements[i]
                    p0 = e['points'][0]
                    dx = p0[0] - cur_x
                    dy = p0[1] - cur_y
                    d0 = np.sqrt(dx*dx + dy*dy)
                    
                    d1 = d0
                    if use_reverse:
                        p0 = e['points'][-1]
                        dx = p0[0] - cur_x
                        dy = p0[1] - cur_y
                        d1 = np.sqrt(dx*dx + dy*dy)

                    if d0 < dist:
                        dist = d0
                        closest = i
                            
                    if d1 < dist and use_reverse:
                        dist = d1
                        closest = i
                        reverse = i

            if reverse is not None:
                reverse_list.append(reverse)

            if closest is not None:
                sort_list.append(closest)

        return sort_list, reverse_list

    def skip_layer(self, layer, layer_config):
        """
        
        @param layer
        @param layer_config
        """
        return False
        
    def plan(self, drawing):
        """
        
        @param drawing
        """
        for layer_config in self.config['layers']:
            name = layer_config['name']
            layer = drawing.getLayerByName(name)
            if layer:
                self.plan_layer(layer, layer_config)
            else:
                raise LayerNotFound('Layer with name "{:s}" not found'.format(name))
            
    def finish(self, return_home=True):
        """
        
        @param return_home
        """
        self.cnc.finish(return_home=return_home)
        
    def apply_settings(self, layer, layer_config):
        """
        
        @param layer
        @param layer_config
        """
        pass
        
    def plan_layer(self, layer, layer_config):
        """
        
        @param layer
        @param layer_config
        """
        
        if self.skip_layer(layer, layer_config):
            return
        
        optimize = self.config['general']['optimize']
        do_zero_cut = self.config['general']['zero_cut']
        
        elements = layer.primitives
        # print elements
        feedrate_cfg = layer_config['feedrate']
        
        feedrate = self.value_map[feedrate_cfg['type']]
        
        self.cnc.comment('Layer "{0}"'.format(layer.name))
        
        cut = layer_config['cut']
        depth = cut['depth']
        steps = cut['steps']
        sequential = cut['sequential']
        step_inc = 0 if do_zero_cut else 1
        z_step = depth / float(steps)
        
        if optimize > 0:
            idx_list, reverse_list = self.sort_elements(layer.primitives, [], [], use_reverse=(optimize>1) )
        else:
            idx_list = range(len(elements))
            reverse_list = []

        if sequential:
            for i in idx_list:
                e = elements[i]
                for j in xrange(steps):
                    
                    self.cnc.setFeedrate( feedrate(layer.color, feedrate_cfg) )
                    self.cnc.setCutDepth( (j+step_inc)*z_step )
                    self.apply_settings(layer, layer_config)
                    
                    self.cnc.millPolyline( e['points'], j in reverse_list )
        else:
            for i in xrange(steps):
                self.cnc.setFeedrate( feedrate(layer.color, feedrate_cfg) )
                self.cnc.setCutDepth( (i+step_inc)*z_step )
                self.apply_settings(layer, layer_config)
        
                for j in idx_list:
                    e = elements[j]
                    self.cnc.millPolyline( e['points'], j in reverse_list )
