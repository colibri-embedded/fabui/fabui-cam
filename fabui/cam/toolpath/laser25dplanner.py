#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

from .simple25dplanner import Simple25DPlanner
from fabui.cam.gcode.filewriter import FileWriter, WriterBase
from fabui.cam.gcode.laser25d import Laser25D

class Laser25DPlanner(Simple25DPlanner):
    
    def __init__(self, output, config, ndigits=4, compact=True, no_comment=False):
        """
        """
        if isinstance(output, str):
            self.output = FileWriter(output)
        elif isinstance(output, WriterBase):
            self.output = output
        
        self.config = config
        
        travel_feedrate = 10000
        off_during_travel = True
        
        if 'general' in config:
            if 'travel_feedrate' in config['general']:
                travel_feedrate = config['general']['travel_feedrate']
            if 'off_during_travel' in config['general']:
                off_during_travel = config['general']['off_during_travel']
        else:
            self.config['general'] = {
                'zero_cut': True,
                'travel_feedrate': travel_feedrate,
                'off_during_travel': off_during_travel
            }
        if 'zero_cut' not in self.config['general']:
            self.config['general']['zero_cut'] = True
        
        z_feedrate = 1000
        
        cnc_config = {
            'travel-feedrate':  travel_feedrate,
            'plunge-rate':      z_feedrate,
            'retract-rate':     z_feedrate,
            'cut-depth':        0.0,
            'safe-z':           0.0,
            'off-during-travel': off_during_travel
        }
        
        cnc = Laser25D(self.output, cnc_config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        super(Laser25DPlanner, self).__init__(cnc=cnc)
    
    def apply_settings(self, layer, layer_config):
        """
        
        @param layer
        @param layer_config
        """
        
        # TODO: exception if type is unknown
        
        pwm_cfg = layer_config['pwm']
        pwm_value = self.value_map[ pwm_cfg['type'] ]
        
        self.cnc.setPWM( pwm_value(layer.color, pwm_cfg) )

    def skip_layer(self, layer, layer_config):
        pwm_cfg = layer_config['pwm']
        pwm_value = self.value_map[ pwm_cfg['type'] ]
        
        return pwm_value(layer.color, pwm_cfg) == 0
