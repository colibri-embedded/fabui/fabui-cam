#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

import numpy as np

from fabui.cam.gcode.filewriter import FileWriter, WriterBase
from fabui.cam.gcode.cnc25d import CNC25D
from fabui.cam.common.drawing2d import Drawing2D, Layer2D
from .milling25dplanner import Milling25DPlanner

import shapely.geometry as sg
from shapely.ops import cascaded_union
from shapely.ops import linemerge
from shapely import affinity
from shapely import speedups

from fabui.cam.toolpath.exceptions import LayerNotFound

if speedups.available:
    speedups.enable()

################################ DEBUG #########################################
try:
    from matplotlib import pyplot
    use_matplot = True
except:
    use_matplot = False

BLUE = '#6699cc'
RED =  '#ff0000'
GREEN =  '#00cc00'
GRAY = '#999999'

colors = ['#ff0000','#00cc00', '#0000cc', '#cccc00', '#cc00cc', '#00cccc']

def plot_line(ax, ob, color=GRAY, width=0.5):
    if not ob:
        return
    x, y = ob.xy
    ax.plot(x, y, color=color, linewidth=width, solid_capstyle='round', zorder=1)

def plot_line_xy(ax, x, y, color=GRAY, width=0.5):
    ax.plot(x, y, color=color, linewidth=width, solid_capstyle='round', zorder=1)

def plot_coords(ax, ob):
    if not ob:
        return
    x, y = ob.xy
    ax.plot(x, y, 'o', color='#999999', zorder=1)

################################################################################


class Isolation25DPlanner(Milling25DPlanner):
    """
    """
    
    def __init__(self, output=None, config=None, cnc=None, ndigits=4, compact=True, no_comment=False):
        """
        """
        super(Isolation25DPlanner, self).__init__(
            output=output, 
            config=config, 
            cnc=cnc, 
            ndigits=ndigits, 
            compact=compact, 
            no_comment=no_comment)

    def __merge_elements(self, elements):
        merge_lines = []
        merge_polygons = []

        for e in elements:
            if e.geom_type == 'LineString':
                merge_lines.append(e)
            elif e.geom_type == 'MultiPolygon':
                for pe in e:
                    if pe.area > 0:
                        merge_polygons.append(pe)
            else:
                merge_polygons.append(e)
            
        result = []

        merged_lines = linemerge(merge_lines)
        merged_polygons = cascaded_union(merge_polygons)

        if merged_lines:
            if merged_lines.geom_type == 'LineString':
                result.append(merged_lines)
            else: # MultiLineString
                for e in merged_lines:
                    result.append(e)

        if type(merge_polygons) == list:
            for e in merge_polygons:
                result.append(e)
        elif merge_polygons.geom_type == 'Polygon':
            result.append(merge_polygons)
        else: # MultiPolygon
            for e in merge_polygons:
                if e.area > 0:
                    result.append(e)

        return result

    def plan(self, drawing):
        """
        
        @param drawing
        """
        tool_diameter = self.config['tool-diameter']

        for layer in drawing.layers:
            layer_name = layer.name

            # fig = pyplot.figure(1, figsize=(8,8), dpi=90)
            # ax = fig.add_subplot(111)
            # ax.set_aspect(1)

            print '   Planning', layer_name
            layer_config = self.config['layers'][layer_name]

            elements = []
            for e in layer.primitives:
                if 'shapely' in e:
                    eshp = e['shapely']
                    # plot_line(ax, eshp.coords, color=BLUE)
                    tps = eshp.buffer(tool_diameter/2)
                    elements.append(tps)
                else:
                    print "No shapely object"

            merged = self.__merge_elements(elements)

            toolpaths = Drawing2D(default_layer_name=layer_name)
            for geom in merged:
                toolpaths.addShapelyGeometry(geom, use_interiors=False)

            self.plan_layer(toolpaths.layers[0], layer_config)

            # for element in merged:
            #     if element.geom_type == 'MultiPolygon':
            #         for e in element:
            #             plot_line(ax, e.exterior, color=RED)    
            #     else:
            #         plot_line(ax, element.exterior, color=GREEN)

            # pyplot.show()


