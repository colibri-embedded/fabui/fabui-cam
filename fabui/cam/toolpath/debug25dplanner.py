#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

from .simple25dplanner import Simple25DPlanner
from fabui.cam.gcode.filewriter import FileWriter, WriterBase
from fabui.cam.gcode.debug25d import Debug25D

class Debug25DPlanner(Simple25DPlanner):
    
    def __init__(self, width, height, config, colors = {}, ndigits=4, compact=True, no_comment=False):
        """
        """
        self.config = config
        
        travel_feedrate = 10000
        off_during_travel = True
        
        if 'general' in config:
            if 'travel_feedrate' in config['general']:
                travel_feedrate = config['general']['travel_feedrate']
            if 'off_during_travel' in config['general']:
                off_during_travel = config['general']['off_during_travel']
        else:
            self.config['general'] = {
                'zero_cut': True,
                'travel_feedrate': travel_feedrate,
                'off_during_travel': off_during_travel
            }
        if 'zero_cut' not in self.config['general']:
            self.config['general']['zero_cut'] = True
        
        z_feedrate = 1000
        
        cnc_config = {
            'travel-feedrate':  travel_feedrate,
            'plunge-rate':      z_feedrate,
            'retract-rate':     z_feedrate,
            'cut-depth':        0.0,
            'safe-z':           0.0,
            'off-during-travel': off_during_travel
        }

        self.colors = colors
        
        cnc = Debug25D(width, height, cnc_config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        super(Debug25DPlanner, self).__init__(cnc=cnc)
    
    def apply_settings(self, layer, layer_config):
        """
        
        @param layer
        @param layer_config
        """

        if 'pwm' in layer_config:
            pwm_cfg = layer_config['pwm']
            pwm_value = self.value_map[ pwm_cfg['type'] ]
            
            print "PWM", pwm_value(layer.color, pwm_cfg), '@', layer.color, pwm_cfg['type']
            print layer.name
            self.cnc.setPWM( pwm_value(layer.color, pwm_cfg) )

        if layer.name in self.colors:
            color = self.colors[layer.name]
            self.cnc.setColor( color[0], color[1], color[2] )
            
            

