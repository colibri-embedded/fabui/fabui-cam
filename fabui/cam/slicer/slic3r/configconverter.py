import os
import json

KEY_MAP = {
    'layers.height': 'layer_height'
}

CLI_MAP = {
    'version': lambda v: '',
    'output.svg': lambda v: '--export-svg' if v else '',
    'output.format': lambda v: '--output-format "{:s}"'.format(v),
    'layer.height': lambda v: '--layer-height {0}'.format(v)
}

class ConfigConverter(object):

    def __init__(self, config={}, cli_only=False):
        self.config = config
        self.cli_only = cli_only

    def clearConfig(self):
        self.config = {}

    def loadConfig(self, config_file):
        """
        Load configuration from a file and update internal data.
        """
        with open(config_file, 'r') as f:
            content = f.read()
            data = json.loads(content)

            self.config.update(data)

    def addConfig(self, config_data):
        """
        """
        self.config.update(config_data)

    def flattenedKeys(self):
        """
        """
        def flatten(data, path = ''):
            # print "@", path, type(data)

            result = {}

            if type(data) == dict:
                for key, value in data.items():
                    if path:
                        r = flatten(value, path + '.' + key)
                        result.update(r)
                    else:
                        r = flatten(value, key)
                        result.update(r)
            else:
                result[path] = data

            return result
        
        return flatten(self.config)

    def convert(self, cli_only=None):
        """
        """
        errors = []
        flat = self.flattenedKeys()

        if cli_only is None:
            cli_only = self.cli_only

        config_content = ''
        cli_args = ''

        for key, value in flat.items():
            if key in KEY_MAP and not cli_only:
                config_content += '{0} = {1}\n'.format(KEY_MAP[key], value)
            elif key in CLI_MAP:
                cli_args += ' ' + CLI_MAP[key](value)
            else:
                errors.append({
                    'msg': 'Unknown key value "{:s}"'.format(key)
                })

        return errors, config_content, cli_args
