#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

from .raw import RAW
from .exceptions import ParameterError

class CNC(object):
    """
    CNC class abstracting RAW GCode usage.
    """
    
    """ Absolute coordinates. """
    eABSOLUTE = 1
    """ Relative coordinates. """
    eRELATIVE = 2
    """ Clock wise movement. """
    eCW  = 0
    """ Counter clock wise movement. """
    eCCW = 1
    
    X_AXIS = 1
    Y_AXIS = 2
    Z_AXIS = 4
    E_AXIS = 8
    XY_AXIS  = X_AXIS | Y_AXIS
    XZ_AXIS  = X_AXIS | Z_AXIS
    YZ_AXIS  = Y_AXIS | Z_AXIS
    XYZ_AXIS = X_AXIS | Y_AXIS | Z_AXIS
    ALL_AXIS = XYZ_AXIS | E_AXIS
    
    DEFAULT_CONFIG = {
        'feedrate': 30.0,
        'xyz-mode': eABSOLUTE,
        'e-mode': eABSOLUTE,
        'use-probe': False
    }
    
    def __init__(self, output, config={}, ndigits=6, compact=False, no_comment=False):
        """
        """
        self.raw = RAW(output, ndigits, compact, no_comment)
        
        self.curX = 0
        self.curY = 0
        self.curZ = 0
        self.curE = 0
        
        self.settingsStack = []
        
        self.config = {}
        self.config.update(CNC.DEFAULT_CONFIG)
        self.config.update(config)
        self.update_feedrate = True
        
        self.useProbe = False

    def setConfig(self, new_config):
        """
        Set config to new value an the missing values to default values.
        """
        self.config.update(self.DEFAULT_CONFIG)
        self.config.update(new_config)
            
    def updateConfig(self, new_config):
        """
        Update config with new values
        """
        old_feedrate = self.config['feedrate']
        self.config.update(new_config)
        new_feedrate = self.config['feedrate']
        
        self.update_feedrate = (old_feedrate != new_feedrate) or self.update_feedrate
        
    def getConfig(self):
        """
        Return current config.
        """
        return self.config
        
    def setFeedrate(self, feedrate):
        """
        Set new feedrate. It will be update with the next move command.
        
        @param feedrate Feedrate in mm/s
        """
        old_feedrate = self.config['feedrate']
        self.config['feedrate'] = feedrate
        
        self.update_feedrate = (old_feedrate != feedrate) or self.update_feedrate
        
    def getFeedrate(self):
        """
        Return current feedrate.
        """
        return self.config['feedrate']

    def pushSettings(self):
        """
        Push current feedrate, mode and probe settings to stack
        """
        cur_config = {}
        cur_config.update( self.getConfig() )
        self.settingsStack.append( cur_config )
        
    def popSettings(self):
        """
        Pop feedrate, mode and probe settings from stack
        """
        prev_config = self.settingsStack.pop(-1)
        
        if prev_config:
            self.setAxisMode( CNC.X_AXIS, prev_config['xyz-mode'] )
            self.setAxisMode( CNC.E_AXIS, prev_config['e-mode'] )
            
            del prev_config['xyz-mode']
            del prev_config['e-mode']
            
            self.updateConfig(prev_config)
            
    def comment(self, message):
        """
        Add gcode comment
        """
        self.raw.COMMENT(message)

    def homeAxis(self, X=False, Y=False, Z=False):
        """
        Home selected axis
        """
        if self.useProbe:
            self.raw.G28(X,Y,Z)
        else:
            self.raw.G27(X,Y,Z)
            
        if X:
            self.curX = 0.0
            
        if Y:
            self.curY = 0.0
            
        if Z:
            self.curZ = 0.0
        
    def homeAll(self):
        """
        Home all axis
        """
        self.homeAxis(True,True,True)

    def homeXY(self):
        """
        Home X and Y axis
        """
        self.homeAxis(True,True)
        
    def homeZ(self):
        """
        Home Z axis
        """
        self.homeAxis(Z=True)

    def zeroAxis(self, X=None, Y=None, Z=None, E=None):
        """
        Zero axis
        """
        self.raw.G92(X,Y,Z,E)
        
        if X is not None:
            self.curX = X
            
        if Y is not None:
            self.curY = Y
            
        if Z is not None:
            self.curZ = Z
            
        if E is not None:
            self.curE = E
        
    def zeroAll(self):
        """
        Zero all axis (x,y,z,e)
        """
        self.zeroAxis(0,0,0,0)
        
    def zeroXY(self, X=0.0, Y=0.0):
        """
        Zero XY axis
        """
        self.zeroAxis(X,Y)
        
    def zeroXYZ(self, X=0.0, Y=0.0, Z=0.0):
        """
        Zero XYZ axis
        """
        self.zeroAxis(X,Y,Z)
        
    def zeroZ(self, Z=0.0):
        """
        Zero Z axis
        """
        self.zeroAxis(Z=Z)
        
    def zeroE(self, E=0.0):
        """
        Zero E axis
        """
        self.zeroAxis(E=E)
    
    def isAt(self, X=None, Y=None, Z=None, E=None):
        """
        Check whether we are at given position.
        Takes into account the configure ndigit setting.
        
        @param Check x coordinate
        @param Check y coordinate
        @param Check z coordinate
        @param Check e coordinate
        """
        atX = True
        atY = True
        atZ = True
        atE = True
        
        if X is not None:
            x_val = self.raw.number_to_str(X)
            x_cur = self.raw.number_to_str(self.curX)
            atX = (x_val == x_cur)
        
        if Y is not None:
            y_val = self.raw.number_to_str(Y)
            y_cur = self.raw.number_to_str(self.curY)
            atY = (y_val == y_cur)
        
        if Z is not None:
            z_val = self.raw.number_to_str(Z)
            z_cur = self.raw.number_to_str(self.curZ)
            atZ = (z_val == z_cur)
        
        if E is not None:
            e_val = self.raw.number_to_str(E)
            e_cur = self.raw.number_to_str(self.curE)
            atE = (e_val == e_cur)
        
        return (atX and atY and atZ and atE)
    
    def travelBy(self, X = None, Y = None, Z = None, E = None, F = None):
        """
        Executa a travel move relative to current position.
        """
        self.moveBy(X,Y,Z,E,F,use_tool=False)
    
    def __set_feedrate(self, F):
        f_val = None
        
        if F is None: # Not defined
            if self.update_feedrate:
                f_val = self.config['feedrate']
        else: # Defined
            f_new = int(F)
            if self.config['feedrate'] != f_new or self.update_feedrate:
                self.config['feedrate'] = f_new
                f_val = f_new
                
        self.update_feedrate = False
        
        return f_val
    
    def moveBy(self, X = None, Y = None, Z = None, E = None, F = None, use_tool=True):
        """
        Executa a tool move relative to current position.
        """
        # We are at desired position, no need to move
        if self.isAt(X,Y,Z,E):
            return
            
        x_val = None
        y_val = None
        z_val = None
        e_val = None
        f_val = self.__set_feedrate(F)
        
        if X is not None:
            x_new = float(X)
            if self.isAbsolute(CNC.X_AXIS):
                x_round = self.raw.round_number( self.curX + x_new )
                if self.curX != x_round or not self.raw.compact:
                    x_val = self.curX = x_round
                
            # TODO: relative move
        
        if Y is not None:
            y_new = float(Y)
            if self.isAbsolute(CNC.Y_AXIS):
                y_round = self.raw.round_number( self.curY + y_new )
                if self.curY != y_round or not self.raw.compact:
                    y_val = self.curY = y_round
                
            # TODO: relative move
        
        if Z is not None:
            z_new = float(Z)
            if self.isAbsolute(CNC.Z_AXIS):
                z_round = self.raw.round_number( self.curZ + z_new )
                if self.curZ != z_round or not self.raw.compact:
                    z_val = self.curZ = z_round
                
            # TODO: relative move
        
        if E is not None:
            e_new = float(E)
            if self.isAbsolute(CNC.E_AXIS):
                e_round = self.raw.round_number( self.curE + e_new )
                if self.curE != e_round or not self.raw.compact:
                    e_val = self.curE = e_round
                
            # TODO: relative move
        
        if use_tool:
            self.raw.G1(x_val, y_val, z_val, e_val, f_val)
        else:
            self.raw.G0(x_val, y_val, z_val, e_val, f_val)

    
    def travelTo(self, X = None, Y = None, Z = None, E = None, F = None):
        """
        Execute a travel move to given coordinates.
        """
        self.moveTo(X,Y,Z,E,F,use_tool=False)
        
    def moveTo(self, X = None, Y = None, Z = None, E = None, F = None, use_tool=True):
        """
        Execute a tool move to given coordinates.
        """
        # We are at desired position, no need to move
        if self.isAt(X,Y,Z,E):
            return
           
        x_val = None
        y_val = None
        z_val = None
        e_val = None 
        f_val = self.__set_feedrate(F)
        
        if X is not None:
            x_val = self.raw.round_number(X)
            if self.isAbsolute(CNC.X_AXIS):
                if not self.isAt(X=x_val):
                    self.curX = float(x_val)
                else:
                    if self.raw.compact:
                        x_val = None
            # TODO: relative
        
        if Y is not None:
            y_val = self.raw.round_number(Y)
            if self.isAbsolute(CNC.Y_AXIS):
                if not self.isAt(Y=y_val):
                    self.curY = float(y_val)
                else:
                    if self.raw.compact:
                        y_val = None
            # TODO: relative
        
        if Z is not None:
            z_val = self.raw.round_number(Z)
            if self.isAbsolute(CNC.Z_AXIS):
                if not self.isAt(Z=z_val):
                    self.curZ = float(z_val)
                else:
                    if self.raw.compact:
                        z_val = None
            # TODO: relative
        
        if E is not None:
            e_val = self.raw.round_number(E)
            if self.isAbsolute(CNC.E_AXIS):
                if not self.isAt(E=e_val):
                    self.curE = float(e_val)
                else:
                    if self.raw.compact:
                        e_val = None
            # TODO: relative
        
        if use_tool:
            self.raw.G1(x_val, y_val, z_val, e_val, f_val)
        else:
            self.raw.G0(x_val, y_val, z_val, e_val, f_val)

    def wait(self, S):
        """
        Pause execution for specified time.
        
        @param S time in seconds
        """
        self.raw.G4(S=S)
        
    def sync(self):
        """
        Wait for all current moves to finish.
        """
        self.raw.M400()

    def finishAllMoves(self):
        """
        Wait for all current moves to finish.
        """
        self.sync()
        
    def isAbsolute(self, axis=X_AXIS):
        """
        Return if the axis is in absolute mode.
        
        @param axis Axis of intereset [X_AXIS, Y_AXIS, Z_AXIS, E_AXIS]
        """
        if axis in [CNC.X_AXIS, CNC.Y_AXIS, CNC.Z_AXIS]:
            return (self.getAxisMode(CNC.X_AXIS) == CNC.eABSOLUTE)
        elif axis == CNC.E_AXIS:
            return (self.getAxisMode(CNC.E_AXIS) == CNC.eABSOLUTE)
            
    def isRelative(self, axis=X_AXIS):
        """
        Return if the axis is in absorelativelute mode.
        
        @param axis Axis of intereset [X_AXIS, Y_AXIS, Z_AXIS, E_AXIS]
        """
        return not self.isAbsolute(axis)
        
    def setAxisMode(self, axis=None, mode=None):
        """
        Set axis coordinate mode
        
        @param axis Axis of intereset [X_AXIS, Y_AXIS, Z_AXIS, E_AXIS]
        @param mode Coordinate mode [eABSOLUTE, eRELATIVE]
        """
        
        axis = axis if axis is not None else CNC.X_AXIS
        mode = mode if mode is not None else CNC.eABSOLUTE
        
        if axis not in [CNC.X_AXIS, CNC.Y_AXIS, CNC.Z_AXIS, CNC.E_AXIS]:
            raise ParameterError('CMC.setAxisMode: axis value out of allowed range')
            
        if mode not in [CNC.eABSOLUTE, CNC.eRELATIVE]:
            raise ParameterError('CMC.setAxisMode: mode value out of allowed range')
        
        if axis in [CNC.X_AXIS, CNC.Y_AXIS, CNC.Z_AXIS]:
            if mode != self.config['xyz-mode']:
                self.config['xyz-mode'] = mode
                if mode == CNC.eABSOLUTE:
                    self.raw.G90()
                else:
                    self.raw.G91()
        elif axis == CNC.E_AXIS:
            if mode != self.config['e-mode']:
                self.config['e-mode'] = mode
                if mode == CNC.eABSOLUTE:
                    self.raw.M82()
                else:
                    self.raw.M83()

    def getAxisMode(self, axis=None):
        """
        """
        axis = axis if axis is not None else CNC.X_AXIS
        
        if axis in [CNC.X_AXIS, CNC.Y_AXIS, CNC.Z_AXIS]:
            return self.config['xyz-mode']
        elif axis == CNC.E_AXIS:
            return self.config['e-mode']

    def getCurrentPosition(self, axis=None):
        """
        Return the current position of selected axis.
        
        @param axis
        """
        
        axis = axis if axis is not None else CNC.XYZ_AXIS
        
        if not (axis & CNC.ALL_AXIS):
            raise ParameterError('CMC.getCurrentPosition: unknown axis specified')
        
        x = self.curX
        y = self.curY
        z = self.curZ
        e = self.curE

        if CNC.ALL_AXIS == axis:
            return (x,y,z,e)

        if CNC.XYZ_AXIS == axis:
            return (x,y,z)
        
        if CNC.XY_AXIS == axis:
            return (x,y)
            
        if CNC.XZ_AXIS == axis:
            return (x,z)
            
        if CNC.YZ_AXIS == axis:
            return (y,z)
            
        if CNC.X_AXIS == axis:
            return x
            
        if CNC.Y_AXIS == axis:
            return y
            
        if CNC.Z_AXIS == axis:
            return z
            
        if CNC.E_AXIS == axis:
            return e
            
        return ()
