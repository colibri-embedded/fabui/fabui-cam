#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

class WriterBase(object):
    """
    Default gcode output writer
    """
    
    def __init__(self):
        """
        """
        pass
        
    def write(self, v):
        """
        Write line to output.
        """
        pass
        
    def close(self):
        """
        Close the file.
        """
        pass
        
    def clear(self):
        """
        Clear content
        """
        pass
