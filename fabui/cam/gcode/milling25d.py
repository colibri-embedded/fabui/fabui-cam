#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

from .cnc25d import CNC25D
from .raw import RAW
from .exceptions import ParameterError

class Milling25D(CNC25D):
    
    DEFAULT_CONFIG = {
        'spindle-speed' : 14000,
        'spindle-direction' : CNC25D.eCW
    }
    
    def __init__(self, output, config = {}, ndigits=6, compact=False, no_comment=False):
        """
        Initialize Milling-2.5D object
        """
        super(Milling25D, self).__init__(output, config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        # Extend CNC2.5.config with Millind2.5 config
        super(Milling25D, self).updateConfig(self.DEFAULT_CONFIG)
        super(Milling25D, self).updateConfig(config)
        
        #~ self.update_speed = False
        self.curRPM = 0
    
    def setSpindleSpeed(self, speed):
        """
        Set default spindle speed.
        
        @param speed Spindle speed in RPM
        """
        
        if type(speed) != int and type(speed) != float:
            raise ParameterError('Milling25d.setSpindleSpeed: spindle speed must be a number')
            
        if speed < RAW.RPM_MIN or speed > RAW.RPM_MAX:
            raise ParameterError('Milling25d.setSpindleSpeed: speed value is out of allowed range')
        
        self.config['spindle-speed'] = speed
    
    def spindleOn(self, speed = None, direction = None):
        """
        Turn spindle motor on at selected speed and direction
        
        @param speed     Spindle speed in RPM
        @param direction Spindle spin direction [eCW|eCCW]
        """
        if speed is None:
            speed = self.config['spindle-speed']
        
        if direction is None:
            direction = self.config['spindle-direction']
            
        if type(speed) != int and type(speed) != float:
            raise ParameterError('Milling25d.spindleOn: spindle speed must be a number')
            
        if speed < RAW.RPM_MIN or speed > RAW.RPM_MAX:
            raise ParameterError('Milling25d.spindleOn: speed value is out of allowed range')
            
        if direction not in [CNC25D.eCW, CNC25D.eCCW]:
            raise ParameterError('Milling25d.spindleOn: spindle direction is out of allowed range')
        
        if self.curRPM == speed:
            return
        
        # Milling bit bust be at safe_z before the spindle turns on
        pos = self.getCurrentPosition()
        self.travelTo(X=pos[0], Y=pos[1], force=True)
        
        if direction == CNC25D.eCW:
            self.raw.M3(S=speed)
        elif direction == CNC25D.eCCW:
            self.raw.M4(S=speed)
            
        self.curRPM = speed
            
    def spindleOff(self):
        """
        Turn spindle motor off.
        """
        if self.curRPM != 0:
            self.raw.M5()
            
        self.curRPM = 0

    def millTo(self, X, Y):
        """
        Move from current position to X,Y position and mill the path at the
        current cut depth.
        
        @param X Target x coordinate
        @param Y Target y coordinate
        """
        self.spindleOn()
        super(Milling25D, self).millTo(X, Y)
        
    def millPolyline(self, points, reverse = False):
        """
        Travel to starting point if needed and mill the polyline shape with at 
        the current cut depth.
        
        @param points  List of point tuples [(x,y)...]
        @param reverse Reverse list traversal
        """
        self.spindleOn()
        super(Milling25D, self).millPolyline(points, reverse)
        
    def finish(self, X = None, Y = None, return_home = False):
        """
        """
        self.comment('Finish milling')
        super(Milling25D, self).finish(X, Y, return_home)
        
        # Wait for 3 seconds
        self.comment('Wait for 3 sec and switch off spindle')
        self.wait(S=3)
        self.spindleOff()
