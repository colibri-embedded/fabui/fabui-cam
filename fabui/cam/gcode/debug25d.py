#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

from .raw import RAW
from .cnc25d import CNC25D
from .exceptions import ParameterError
from fabui.cam.gcode.filewriter import WriterBase

import numpy as np
import cv2

class ObliviousWriter(WriterBase):
    """
    Obliviouse writer
    """
    def __init__(self): pass
    def write(self, v): pass
    def close(self): pass
    def clear(self): pass

class Debug25D(CNC25D):
    
    DEFAULT_CONFIG = {
        'pwm' : 255,
        'cut-feedrate': 100,
        'travel-feedrate': 200,
        'plunge-rate': 200,
        'retract-rate': 200,
        'cut-depth': 0.0,
        'safe-z': 0.0,
        'off-during-travel': True
    }
    
    def __init__(self, width, height, config = {}, ndigits=6, compact=False, no_comment=False):
        """
        Initialize Milling-2.5D object
        """
        
        output = ObliviousWriter()
        
        super(Debug25D, self).__init__(output, config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        # Extend CNC2.5.config with Laser2.5 config
        super(Debug25D, self).updateConfig(self.DEFAULT_CONFIG)
        super(Debug25D, self).updateConfig(config)
        
        self.dbg_img = np.ones((int(height), int(width), 3), np.uint8)*255
        
        self.curPWM = 0
        self.curColor = None

    def setColor(self, r, g, b):
        self.curColor = (b,g,r)
        
    def setPWM(self, pwm):
        """
        """
        if type(pwm) != int:
            raise ParameterError('Laser25d.laserOn: laser pwm must be a number')
        
        if pwm < RAW.PWM_MIN or pwm > RAW.PWM_MAX:
            raise ParameterError('Laser25D.laserOn: pwm value is out of allowed range')
            
        self.config['pwm'] = pwm
        
    def laserOn(self, pwm = None):
        """
        Turn spindle motor on at selected speed and direction
        
        @param speed     Spindle speed in RPM
        @param direction Spindle spin direction [eCW|eCCW]
        """
        if pwm is None:
            pwm = self.config['pwm']
        
        if type(pwm) != int:
            raise ParameterError('Laser25d.laserOn: laser pwm must be a number')
            
        if pwm < RAW.PWM_MIN or pwm > RAW.PWM_MAX:
            raise ParameterError('Laser25D.laserOn: pwm value is out of allowed range')
        
        if self.curPWM != pwm:
            self.curPWM = pwm
            self.raw.M61(S=pwm)
            
    def laserOff(self):
        """
        Turn spindle motor off.
        """
        if self.curPWM != 0:
            self.raw.M62()
            self.curPWM = 0

    def setCutDepth(self, cut_depth):
        """
        """
        self.updateConfig({'cut-depth' : float(cut_depth)})
        self.updateConfig({'safe-z' : float(cut_depth)})

    def travelTo(self, X, Y, force = False):
        """
        """
        travel_feedrate = self.config['travel-feedrate']
        
        if self.isAt(X=X, Y=Y) and not force:
            return
            
        if self.config['off-during-travel'] and self.curPWM != 0:
            self.laserOff()
        
        super(CNC25D, self).travelTo(X,Y, F=travel_feedrate)
        
    def millTo(self, X, Y):
        """
        Move from current position to X,Y position and mill the path at the
        current cut depth.
        
        @param X Target x coordinate
        @param Y Target y coordinate
        """
        cut_z = -self.config['cut-depth']
        plunge_rate = self.config['plunge-rate']
        cut_feedrate = self.config['cut-feedrate']
        
        if not self.isAt(Z=cut_z):
            if self.config['off-during-travel'] and self.curPWM != 0:
                self.laserOff()
            self.moveTo(Z=cut_z, F=plunge_rate, use_tool=False)
        
        self.laserOn()
        
        cur_position = self.getCurrentPosition()
        
        x0 = int(round(cur_position[0]))
        y0 = int(round(cur_position[1]))
        
        x1 = int(round(X))
        y1 = int(round(Y))
        
        pwm = 255 - int(self.curPWM)

        color = (pwm, pwm, pwm)

        if self.curColor is not None:
            color = self.curColor
        
        cv2.line(self.dbg_img, (x0,y0), (x1,y1), color )
        
        self.moveTo(X,Y, F=cut_feedrate)
        
    def finish(self, X = None, Y = None, return_home = False):
        """
        """
        
        self.comment('Finish laser engraving')
        self.laserOff()
        super(Debug25D, self).finish(X, Y, return_home)
        self.dbg_img = cv2.flip(self.dbg_img,0)
