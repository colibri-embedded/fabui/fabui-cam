#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

from .cnc import CNC

class CNC25D(CNC):
    """
    CNC-2.5 class extending CNC base class with automatic z-height and feedrate
    adjustments.
    """
    
    DEFAULT_CONFIG = {
        'cut-feedrate' : 30.0,      #
        'travel-feedrate': 100.0,   #
        'plunge-rate': 10.0,        #
        'retract-rate': 20.0,       #
        'safe-z' : 5.0,             # mm
        'cut-depth' : 1.0           # mm
    }
    
    def __init__(self, output, config = {}, ndigits=6, compact=False, no_comment=False):
        """
        Initialize CNC-2.5 object
        """
        super(CNC25D, self).__init__(output, config, ndigits=ndigits, compact=compact, no_comment=no_comment)
        
        # Extend CNC.config with CNC2.5 config
        super(CNC25D, self).updateConfig(CNC25D.DEFAULT_CONFIG)
        super(CNC25D, self).updateConfig(config)
    
    def setCutDepth(self, cut_depth):
        """
        """
        self.updateConfig({'cut-depth' : float(cut_depth)})
        
    def getCutDepth(self):
        """
        """
        return self.config['cut-depth']
        
    def setFeedrate(self, feedrate):
        """
        """
        self.config['cut-feedrate'] = feedrate
    
    def travelTo(self, X, Y, force = False):
        """
        Travel to X,Y position at safe-z height to avoid milling or any obstacles.
        
        @param X Target x coordinate
        @param Y Target y coordinate
        """
        safe_z = self.config['safe-z']
        travel_feedrate = self.config['travel-feedrate']
        retract_rate = self.config['retract-rate']
        
        if self.isAt(X=X, Y=Y) and not force:
            return
        
        if not self.isAt(Z=safe_z):
            super(CNC25D, self).travelTo( Z=safe_z, F=retract_rate )
            
        super(CNC25D, self).travelTo(X,Y, F=travel_feedrate)
        
    def millTo(self, X, Y):
        """
        Move from current position to X,Y position and mill the path at the
        current cut depth.
        
        @param X Target x coordinate
        @param Y Target y coordinate
        """
        cut_z = -self.config['cut-depth']
        plunge_rate = self.config['plunge-rate']
        cut_feedrate = self.config['cut-feedrate']
        
        if not self.isAt(Z=cut_z):
            self.moveTo(Z=cut_z, F=plunge_rate)
            
        self.moveTo(X,Y, F=cut_feedrate)

    def millPolyline(self, points, reverse = False):
        """
        Travel to starting point if needed and mill the polyline shape with at 
        the current cut depth.
        
        @param points  List of point tuples [(x,y)...]
        @param reverse Reverse list traversal
        """
        if not points:
            return
            
        if reverse:
            p0 = points[-1]
            x0 = p0[0]
            y0 = p0[1]
            
            self.travelTo(x0,y0)
            for pt in reversed(points[:-1]):
                self.millTo( pt[0], pt[1] )
        else:                
            p0 = points[0]
            x0 = p0[0]
            y0 = p0[1]
            
            self.travelTo(x0,y0)
            for pt in points[1:]:
                self.millTo( pt[0], pt[1] )
        
    def finish(self, X = None, Y = None, return_home = False):
        """
        Finish all moves and travel to safe position.
        
        @param X           Move to specific X position all moves have finished
        @param Y           Move to specific Y position all moves have finished
        @param return_home Return to home position after all moves have finished
        """
        if return_home:
            self.travelTo(0.0, 0.0)
            self.finishAllMoves()
            return
        
        if X is not None or Y is not None:
            self.travelTo(X, Y, force=True)
            self.finishAllMoves()
            return
        
        pos = self.getCurrentPosition()
        self.travelTo(pos[0], pos[1], force=True)
        self.finishAllMoves()
