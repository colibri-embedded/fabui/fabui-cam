#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

from .exceptions import ParameterError

class RAW(object):
    """
    RAW GCodes for Marlin/FABtotum version.
    GCode list take from:
    http://forum.fabtotum.cc/showthread.php?1364-Supported-Gcodes
    """
    
    RPM_MIN = 1000
    RPM_MAX = 25000
    
    TEMP_MIN = 0
    TEMP_MAX = 500
    
    BED_TEMP_MIN = 0
    BED_TEMP_MAX = 200
    
    PWM_MIN = 0
    PWM_MAX = 255
    
    EXTRUDER_ID_MIN = 0
    EXTRUDER_ID_MAX = 4
    
    BEEPS_NUM_MAX = 10
    
    PROBE_ID_MIN = 0
    PROBE_ID_MAX = 5
    
    def __init__(self, output, ndigits=6, compact=False, no_comments=False):
        """
        @param output  GCode output writer object
        @param ndigits Number of decimal places to use in number representation
        @param compact Skip spaces between gcode parameters
        """
        self.output = output
        self.ndigits = ndigits
        self.compact = compact
        self.no_comments = no_comments
        
    def number_to_str(self, value):
        """
        Round number to given ndigits decimal places and
        remove unnecessary zeros to reduce gcode size
        """
        
        f_value = float(value)
        new_value = str( round(value, self.ndigits) )
        
        if new_value.endswith('.0'):
            new_value = new_value[0:-2]
        
        if new_value == '-0':
            new_value = '0'
        
        return new_value
        
    def round_number(self, value):
        """
        Round number to given ndigits decimal places
        """
        
        f_value = float(value)
        new_value = round(value, self.ndigits)
        return new_value
        
    def __compact(self, value):
        """
        Remove space between params to reduce gcode size
        """
        return value.replace(' ', '') if self.compact else value
        
    def COMMENT(self, comment):
        """
        Add gcode comment or skip if no_comment is set to True
        """
        if self.no_comments:
            return
        self.output.write('; ' + comment)
    
    def G0(self, X = None, Y = None, Z = None, E = None, F = None, code = 'G0'):
        """
        Coordinated Movement X Y Z E F
        
        @param E e axis
        @param F feedrate
        """
        if X != None:
            code += ' X' + self.number_to_str(X)
        if Y != None:
            code += ' Y' + self.number_to_str(Y)
        if Z != None:
            code += ' Z' + self.number_to_str(Z)
        if E != None:
            code += ' E' + self.number_to_str(E)
        if F != None:
            code += ' F' + self.number_to_str(F)
        
        self.output.write(self.__compact(code))
        
    def G1(self, X = None, Y = None, Z = None, E = None, F = None):
        """
        G1 translates to G0
        """
        self.G0(X, Y, Z, E, F, code='G1')
        
    def G2(self, X = None, Y = None, I = None, J = None, E = None, F = None, code='G2'):
        """
        Clockwise arc
        """
        if X != None:
            code += ' X' + self.number_to_str(X)
        if Y != None:
            code += ' Y' + self.number_to_str(Y)
        if I != None:
            code += ' I' + self.number_to_str(I)
        if J != None:
            code += ' J' + self.number_to_str(J)
        if E != None:
            code += ' E' + self.number_to_str(E)
        if F != None:
            code += ' F' + self.number_to_str(F)
        
        self.output.write(self.__compact(code))

    def G3(self, X = None, Y = None, I = None, J = None, E = None, F = None):
        """
        Counter-clockwise arc
        """
        self.G2(X,Y,I,J,E,F,code='G3')

    def G4(self, S = None, P = None):
        """
        Pause the machine for a period of time.
        
        @param S seconds
        @param P miliseconds
        """
        
        if S != None and P != None:
            raise ParameterError('G4: S and P parameters cannot be used at the same time')
        
        code = 'G4'
        if S != None:
            code += ' S' + self.number_to_str(S)
        if P != None:
            code += ' P' + self.number_to_str(P)
        
        self.output.write(self.__compact(code))

    #~ G10 S<0..1> - retract filament according to settings of M207
    #~ def G10(self):
        #~ pass

    #~ G11 - retract recover filament according to settings of M208
    #~ def G11(self):
        #~ pass

    def G27(self, X = None, Y = None, Z = None):
        """
        Home all axis (use z-max endstop)
        """
        self.G28(X,Y,Z,code='G27')
        
    def G28(self, X = None, Y = None, Z = None, code='G28'):
        """
        Home all axis (use z-probe)
        """
        
        if X and Y and Z:
            pass
        else:        
            if X:
                code += ' X'
            if Y:
                code += ' Y'
            if Z:
                code += ' Z'
        
        self.output.write(self.__compact(code))

    def G29(self):
        """
        Autobed leveling (ABL)
        """
        code = 'G29'
        self.output.write(self.__compact(code))

    def G30(self, U = None, D = None):
        """
        Single Z Probe, probes bed at current XY location searching Z length
        
        @param U up_speed
        @param D down-speed
        """
        code = 'G30'
        if U != None:
            code += ' U' + self.number_to_str(U)
        if D != None:
            code += ' D' + self.number_to_str(D)
        
        self.output.write(self.__compact(code))

    def G38(self, S = None):
        """
        User external z-probe (conductive or digitizer head)
        Use M746 to activate and deactivate (see Factotum custom M codes below)
        
        @param S feedrate
        """
        code = 'G38'
        if S != None:
            code += ' S' + self.number_to_str(S)
        
        self.output.write(self.__compact(code))

    def G90(self):
        """
        Swithc to absolute Coordinates 
        """
        code = 'G90'
        self.output.write(self.__compact(code))

    def G91(self):
        """
        Switch to relative Coordinates
        """
        code = 'G91'
        self.output.write(self.__compact(code))

    def G92(self, X = None, Y = None, Z = None, E = None, code='G92'):
        """
        Set current position to X,Y,Z,E coordinates.
        Without coordinates will reset all axes to zero.
        """
        if X == 0.0 and Y == 0.0 and Z == 0.0 and E == 0.0:
            pass
        else:
            if X != None:
                code += ' X' + self.number_to_str(X)
            if Y != None:
                code += ' Y' + self.number_to_str(Y)
            if Z != None:
                code += ' Z' + self.number_to_str(Z)
            if E != None:
                code += ' E' + self.number_to_str(E)
        
        self.output.write(self.__compact(code))

    #~ M0   - Unconditional stop - Wait for user to press a button on the LCD (Only if ULTRA_LCD is enabled)        
    #~ M1   - Same as M0
        
    def M17(self):
        """
        Enable/Power all stepper motors
        """
        code = 'M17'
        self.output.write(self.__compact(code))

    def M18(self):
        """
        Disable all stepper motors
        """
        code = 'M18'
        self.output.write(self.__compact(code))
            
    #~ M20  - List SD card
    #~ M21  - Init SD card
    #~ M22  - Release SD card
    #~ M23  - Select SD file (M23 filename.g)
    #~ M24  - Start/resume SD print
    #~ M25  - Pause SD print
    #~ M26  - Set SD position in bytes (M26 S12345)
    #~ M27  - Report SD print status
    #~ M28  - Start SD write (M28 filename.g)
    #~ M29  - Stop SD write
    #~ M30  - Delete file from SD (M30 filename.g)
    #~ M31  - Output time since last M109 or SD card start to serial
    #~ M32  - Select file and start SD print (Can be used _while_ printing from SD card files):
           #~ syntax "M32 /path/filename#", or "M32 S<startpos bytes> !filename#"
           #~ Call gcode file : "M32 P !filename#" and return to caller file after finishing (similar to #include).
           #~ The '#' is necessary when calling from within sd files, as it stops buffer prereading

    #~ M42  - Change pin status via gcode Use M42 Px Sy to set pin x to value y, when omitting Px the onboard led will be used.
        
    def M60(self, S = None, code='M60'):
        """
        Set laser pwm immediately
        
        @param S laser pwm [0-255]
        """
        if S != None:
            pwm = int(S)
            if pwm < self.PWM_MIN or pwm > self.PWM_MAX:
                raise ParameterError('{0} pwm value is out of allowed range'.format(code))
            code += ' S' + self.number_to_str(S)
        
        self.output.write(self.__compact(code))
        
    def M61(self, S = None):
        """
        Set laser pwm after finishing previous move
        
        @param S laser pwm [0-255]
        """
        self.M60(S, code='M61')
        
    def M62(self):
        """
        Turn laser off.
        """
        code = 'M62'
        self.output.write(self.__compact(code))
        
    #~ M80  - Turn on Power Supply
    #~ M81  - Turn off Power Supply
    
    def M82(self):
        """
        Set E axis to absolute mode.
        """
        code = 'M82'
        self.output.write(self.__compact(code))
        
    def M83(self):
        """
        Set E axis to relative coordinates, while xyz are in absolute mode (G90)
        """
        code = 'M83'
        self.output.write(self.__compact(code))
    
    #~ M84  - Disable steppers until next move,
            #~ or use S<seconds> to specify an inactivity timeout, after which the steppers will be disabled.  S0 to disable the timeout.
            
    def M85(self, S):
        """
        Set inactivity shutdown timer.
        @param S timeout in seconds
        """
        code = 'M85'
        code += ' S' + self.number_to_str(S)
        self.output.write(self.__compact(code))
    
    def M92(self, X = None, Y = None, Z = None, E = None):
        """
        Set steps pre axis unit
        
        @param X X-axis steps per unit
        @param Y Y-axis steps per unit
        @param Z Z-axis steps per unit
        @param E E-axis steps per unit
        """
        self.G92(X,Y,Z,E, code='M92')
    
    def M104(self, S, code='M104'):
        """
        Set extruder target temperature
        
        @param S temperature [0-500]
        """
        temp = float(S)
        if temp < self.TEMP_MIN or temp > self.TEMP_MAX:
            raise ParameterError('{0} temp value is out of allowed range'.format(code))
            
        code += ' S' + self.number_to_str(S)
        self.output.write(self.__compact(code))
        
    def M105(self):
        """
        Return current temperature
        """
        code = 'M105'
        self.output.write(self.__compact(code))
    
    def M106(self, S = None):
        """
        Turn head fan on
        
        @param S fan pwm value
        """
        code = 'M106'
        if S != None:
            pwm = int(S)
            if pwm < self.PWM_MIN or pwm > self.PWM_MAX:
                raise ParameterError('{0} pwm value is out of allowed range'.format(code))
            code += ' S' + self.number_to_str(S)

        self.output.write(self.__compact(code))
        
    def M107(self):
        """
        Turn head fan off
        """
        code = 'M107'
        self.output.write(self.__compact(code))
        
    def M109(self, S = None, R = None, T = None, code='M109'):
        """
        Wait for extruder temperature to reach target.
        @param S target temperature. Waits only when heating
        @param R target temperature. Waits both when heating and cooling
        @param T extruder number
        """
        if S is None and R is None:
            raise ParameterError('{0}: S or R parameter must be specified'.format(code))
        
        if S != None:
            temp = float(S)
            if temp < self.TEMP_MIN or temp > self.TEMP_MAX:
                raise ParameterError('{0}: temp value is out of allowed range'.format(code))
                
            code += ' S' + self.number_to_str(S)
        
        if R != None:
            temp = float(R)
            if temp < self.TEMP_MIN or temp > self.TEMP_MAX:
                raise ParameterError('{0}: temp value is out of allowed range'.format(code))
            code += ' R' + self.number_to_str(R)
            
        if T != None:
            eid = int(T)
            if eid < self.EXTRUDER_ID_MIN or eid > self.EXTRUDER_ID_MAX:
                raise ParameterError('{0}: extruder index is out of allowed range'.format(code))
            code += ' T' + self.number_to_str(T)
        
        self.output.write(self.__compact(code))

    #~ M114 - Output current position to serial port
    def M114(self):
        code = 'M114'
        self.output.write(self.__compact(code))
            
    #~ M115 - Capabilities string
    def M115(self):
        code = 'M115'
        self.output.write(self.__compact(code))
            
    def M117(self, msg):
        """
        Display message
        
        @param msg Message text
        """
        code = 'M119'
        code += msg
        self.output.write(self.__compact(code))
    
    #~ M119 - Output Endstop status to serial port
    def M119(self):
        code = 'M119'
        self.output.write(self.__compact(code))
        
    #~ M126 - Solenoid Air Valve Open (BariCUDA support by jmil)
    #~ M127 - Solenoid Air Valve Closed (BariCUDA vent to atmospheric pressure by jmil)
    #~ M128 - EtoP Open (BariCUDA EtoP = electricity to air pressure transducer by jmil)
    #~ M129 - EtoP Closed (BariCUDA EtoP = electricity to air pressure transducer by jmil)
    
    def M140(self, S = None):
        """
        Set bed target temperature
        @param S bed target temperature [0-200]
        """
        code = 'M140'
        temp = float(S)
        if temp < self.BED_TEMP_MIN or temp > self.BED_TEMP_MAX:
            raise ParameterError('{0} temp value is out of allowed range'.format(code))
            
        code += ' S' + self.number_to_str(S)
        self.output.write(self.__compact(code))
    
    def M190(self, S = None, R = None):
        """
        Wait for bed temperature to reach target.
        @param S target temperature. Waits only when heating
        @param R target temperature. Waits both when heating and cooling
        """
        self.M109(S, R, code='M190')
    
    #~ M200 D<millimeters>- set filament diameter and set E axis units to cubic millimeters (use S0 to set back to millimeters).
    
    def M201(self, X = None, Y = None, Z = None, E = None):
        """
        Set max acceleration in units/s^2 for print moves
        
        @param X X-axis acceleration
        @param Y Y-axis acceleration
        @param Z Z-axis acceleration
        @param E E-axis acceleration
        """
        self.G92(X, Y, Z, E, code='M201')
    
    #~ M202 - Set max acceleration in units/s^2 for travel moves (M202 X1000 Y1000) Unused in Marlin!!
    
    def M203(self, X = None, Y = None, Z = None, E = None):
        """
        Set max feedrate per axis in mm/sec
        
        @param X X-axis max feedrate
        @param Y Y-axis max feedrate
        @param Z Z-axis max feedrate
        @param E E-axis max feedrate
        """
        self.G92(X, Y, Z, E, code='M203')
    
    def M204(self, S = None, T = None):
        """
        Default acceleration
        
        @param S Nomal move acceleration
        @param T Retraction acceleration
        """
        code = 'M204'
        
        if S != None:
            code += ' S' + self.number_to_str(S)
            
        if T != None:
            code += ' T' + self.number_to_str(T)
            
        self.output.write(self.__compact(code))
    
    def M205(self, S = None, T = None, B = None, XY = None, Z = None, E = None):
        """
        Advanced settings
        
        @param S minimum printing speed
        @param T minimum travel speed
        @param B minimum segment time
        @param XY maximum xy jerk
        @param Z maximum Z jerk
        @param E maximum E jerk
        """
        code = 'M205'
        
        if S != None:
            code += ' S' + self.number_to_str(S)
        
        if T != None:
            code += ' T' + self.number_to_str(T)
        
        if B != None:
            code += ' B' + self.number_to_str(B)
        
        if XY != None:
            code += ' X' + self.number_to_str(XY)
        
        if Z != None:
            code += ' Z' + self.number_to_str(Z)
        
        if E != None:
            code += ' E' + self.number_to_str(E)
            
        self.output.write(self.__compact(code))
    
    def M206(self, X = None, Y = None, Z = None):
        """
        Set additional homing offset
        
        @param X x offset
        @param Y y offset
        @param Z z offset
        """
        self.G92(X,Y,Z,code='M206')
    
    #~ M207 - set retract length S[positive mm] F[feedrate mm/min] Z[additional zlift/hop], stays in mm regardless of M200 setting
    #~ M208 - set recover=unretract length S[positive mm surplus to the M207 S*] F[feedrate mm/sec]
    #~ M209 - S<1=true/0=false> enable automatic retract detect if the slicer did not support G10/11: every normal extrude-only move will be classified as retract depending on the direction.
    #~ 
    #~ M218 - set hotend offset (in mm): T<extruder_number> X<offset_on_X> Y<offset_on_Y>
    #~ M220 S<factor in percent>- set speed factor override percentage
    #~ M221 S<factor in percent>- set extrude factor override percentage
    #~ M226 P<pin number> S<pin state>- Wait until the specified pin reaches the state required
    #~ M240 - Trigger a camera to take a photograph
    #~ M250 - Set LCD contrast C<contrast value> (value 0..63)
    #~ M280 - set servo position absolute. P: servo index, S: angle or microseconds
    
    def M300(self, S=None, D=None, P=None):
        """
        Play beep sound
        
        @param S number of beeps
        @param D duration of beep in number of 10ms
        @param P pause between beeps in number of 10ms
        """
        code = 'M300'
        
        if S != None:
            beeps = int(S)
            if beeps > self.BEEPS_NUM_MAX:
                raise ParameterError('{0}: number of beeps is out of allowed range'.format(code))
            
            code += ' S' + self.number_to_str(S)
            
        if D != None:
            if S is None:
                raise ParameterError('{0}: D parameter cannot be used without S'.format(code))
            
            code += ' D' + self.number_to_str(D)
            
        if P != None:
            if S is None:
                raise ParameterError('{0}: P parameter cannot be used without S'.format(code))
                
            code += ' P' + self.number_to_str(P)
        
        self.output.write(self.__compact(code))
    
    def M301(self, P = None, I = None, D = None):
        """
        Set extruder PID parameters
        
        @param P
        @param I
        @param D
        """
        code = 'M301'
        
        if P is None and I is None and D is None:
            raise ParameterError('{0}: P,I or D parameter must be specified'.format(code))
        
        if P != None:
            code += ' P' + self.number_to_str(P)
        
        if I != None:
            code += ' I' + self.number_to_str(I)
        
        if D != None:
            code += ' D' + self.number_to_str(D)
        
        self.output.write(self.__compact(code))
        
    def M302(self, S = None):
        """
        Allow cold extrudes, or set the minimum extrude
        
        @param S minimum extrude temperature
        """
        code = 'M302'
        
        if S != None:
            temp = float(S)
            if temp < self.TEMP_MIN or temp > self.TEMP_MAX:
                raise ParameterError('{0}: temp value is out of allowed range'.format(code))
            code += ' S' + self.number_to_str(S)
        
        self.output.write(self.__compact(code))
    
    def M303(self, S, C = None, E = None):
        """
        @param S target temperatur
        @param E extruder
        @param C number of cycles
        """
        code = 'M303'
        
        code += ' S' + self.number_to_str(S)
        
        if C != None:
            code += ' C' + self.number_to_str(C)
            
        if E != None:
            code += ' E' + self.number_to_str(E)
        
        self.output.write(self.__compact(code))
        
    #~ M304 - Set bed PID parameters P I and D
    
    def M400(self):
        """
        Finish all moves
        """
        code = 'M400'
        self.output.write(self.__compact(code))

    def M401(self):
        """
        Lower z-probe if present
        """
        code = 'M401'
        self.output.write(self.__compact(code))
        
    def M402(self):
        """
        Raise z-probe if present
        """
        code = 'M402'
        self.output.write(self.__compact(code))

    #~ M500 - stores parameters in EEPROM
    #~ M501 - reads parameters from EEPROM (if you need reset them after you changed them temporarily).
    #~ M502 - reverts to the default "factory settings".  You still need to store them in EEPROM afterwards if you want to.
    #~ M503 - print the current settings (from memory not from EEPROM)
    
    #~ M540 - Use S[0|1] to enable or disable the stop SD card print on endstop hit (requires ABORT_ON_ENDSTOP_HIT_FEATURE_ENABLED)
    #~ M600 - Pause for filament change X[pos] Y[pos] Z[relative lift] E[initial retract] L[later retract distance for removal]
    #~ M665 - set delta configurations
    #~ M666 - set delta endstop adjustment
    #~ M605 - Set dual x-carriage movement mode: S<mode> [ X<duplication x-offset> R<duplication temp offset> ]
    #~ M907 - Set digital trimpot motor current using axis codes.
    #~ M908 - Control digital trimpot directly.
    #~ M350 - Set microstepping mode.
    #~ M351 - Toggle MS1 MS2 pins directly.
    
    #~ M928 - Start SD logging (M928 filename.g) - ended by M29
    
    #~ M999 - Restart after being stopped by error
    def M999(self):
        code = 'M999'
        self.output.write(self.__compact(code))
        
    # FABtotum custom M code
    def M3(self, S, code='M3'):
        """
        Turn spindle motor with clockwise spinning.
        @param S RPM [6000-14000]
        """
        if S != None:
            rpm = float(S)
            if rpm < self.RPM_MIN or rpm > self.RPM_MAX:
                raise ParameterError('{0} rpm value is out of allowed range'.format(code))
            
            code += ' S' + self.number_to_str(S)
            
        self.output.write(self.__compact(code))

    def M4(self, S):
        """
        Turn spindle motor with Counter-clockwise spinning.
        @param S RPM [6000-14000]
        """
        self.M3(S, code='M4')
        
    def M5(self):
        """
        Turn off spindle motor.
        """
        code = 'M5'
        self.output.write(self.__compact(code))
        
    def M700(self, S, code='M700'):
        """
        Set head laser PWM
        
        @param S Laser PWM [0-255]
        """
        if S != None:
            pwm = int(S)
            if pwm < self.PWM_MIN or pwm > self.PWM_MAX:
                raise ParameterError('{0}: pwm value is out of allowed range'.format(code))
                
            code += ' S' + self.number_to_str(S)
        self.output.write(self.__compact(code))
    
    def M701(self, S):
        """
        Red ambient light
        
        @param S light pwm value [0-255]
        """
        self.M700(S, code='M701')
            
    def M702(self, S):
        """
        Green ambient light
        
        @param S light pwm value [0-255]
        """
        self.M700(S, code='M702')
        
    def M703(self, S):
        """
        Blue ambient light
        
        @param S light pwm value [0-255]
        """
        self.M700(S, code='M703')
        
    def M704(self, code='M704'):
        """
        Signalling Light ON
        """
        self.output.write(self.__compact(code))
        
    def M705(self):
        """
        Singalling Light OFF
        """
        self.M704(code='M705')
        
    def M706(self, S):
        """
        Head light
        
        @param S light pwm value [0-255]
        """
        self.M700(S, code='M706')
        
    #~ M710 S<VAL> - write and store in eeprom calibrated z_probe offset length
    #~ M711 - write and store in eeprom calibrated zprobe extended angle
    #~ M712 - write and store in eeprom calibrated zprobe retacted angle
    #~ M713 - autocalibration of z-probe length and store in eeprom
    #~ M720 - 24VDC head power ON
    #~ M721 - 24VDC head power OFF
    #~ M722 - 5VDC SERVO_1 power ON
    #~ M723 - 5VDC SERVO_1 power OFF
    #~ M724 - 5VDC SERVO_2 power ON
    #~ M725 - 5VDC SERVO_2 power OFF
    #~ M726 - 5VDC RASPBERRY PI power ON
    #~ M727 - 5VDC RASPBERRY PI power OFF
    #~ M728 - RASPBERRY Alive/awake Command
    #~ M729 - RASPBERRY Sleep                    //wait for the complete shutdown of raspberryPI
    #~ M730 - Read last error code
    #~ M731 - Disable kill on Door Open
    #~ M740 - read WIRE_END sensor
    #~ M741 - read DOOR_OPEN sensor
    #~ M742 - read REEL_LENS_OPEN sensor
    #~ M743 - read SECURE_SWITCH sensor
    #~ M744 - read HOT_BED placed in place
    #~ M745 - read Head placed in place
    
    def M746(self, S = None):
        """
        Select external Z probe mode
        
        @param S 0=disabled, 1=continuity, 2=digitizer
        """
        code = 'M746'
        
        if S != None:
            value = int(S)
            if value < 0 or value > 5:
                raise ParameterError('{0}: probe id is out of allowed range'.format(code))
                
            code += ' S' + self.number_to_str(S)
        self.output.write(self.__compact(code))
        
    #~ 
    #~ M750 - read PRESSURE sensor (ANALOG 0-1023)
    #~ M751 - read voltage monitor 24VDC input supply (ANALOG V)
    #~ M752 - read voltage monitor 5VDC input supply (ANALOG V)
    #~ M753 - read current monitor input supply (ANALOG A)
    #~ M754 - read tempearture raw values (10bit ADC output)
    #~ M760 - read FABtotum Personal Fabricator Main Controller serial ID
    #~ M761 - read FABtotum Personal Fabricator Main Controller control code of serial ID
    #~ M762 - read FABtotum Personal Fabricator Main Controller board version number
    #~ M763 - read FABtotum Personal Fabricator Main Controller production batch number
    #~ M764 - read FABtotum Personal Fabricator Main Controller control code of production batch number
    #~ M765 - read FABtotum Personal Fabricator Firmware Version
    #~ M766 - read FABtotum Personal Fabricator Firmware Build Date and Time
    #~ M767 - read FABtotum Personal Fabricator Firmware Update Author
    #~ 
    #~ M780 - read Head Product Name
    #~ M781 - read Head Vendor Name
    #~ M782 - read Head product ID
    #~ 
    #~ M783 - read Head vendor ID
    #~ M784 - read Head Serial ID
    #~ M785 - read Head firmware version
    #~ M786 - read needed firmware version of FABtotum Personal Fabricator Main Controller
    #~ M787 - read Head capability: type0 (passive, active)
    #~ M788 - read Head capability: type1 (additive, milling, syringe, laser etc..)
    #~ M789 - read Head capability: purpose (single purpose, multipurpose)
    #~ M790 - read Head capability: wattage (0-200W)
    #~ M791 - read Head capability: axis (number of axis)
    #~ M792 - read Head capability: servo (number of axis)
