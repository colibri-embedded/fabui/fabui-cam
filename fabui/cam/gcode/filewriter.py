#!/bin/env python
# -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

from .writerbase import WriterBase

class FileWriter(WriterBase):
    """
    Default gcode file writer
    """
    
    def __init__(self, filename):
        """
        """
        self.filename = filename
        self.fd = open(filename, 'w')
    
    def write(self, v):
        """
        Write line to file.
        """
        if self.fd:
            self.fd.write(v + '\r\n')
        
    def close(self):
        """
        Close the file.
        """
        if self.fd:
            self.fd.close()
        self.fd = None
        
    def clear(self):
        """
        """
        pass
