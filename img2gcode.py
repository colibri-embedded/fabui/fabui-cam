#!/usr/bin/env python
# # -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

# Import standard python module
import argparse
import time
import gettext
import json
import os

from jsonschema import validate

from fabui.cam.common.raster import Raster
from fabui.cam.common.drawing2d import Drawing2D
from fabui.cam.common.raster2drawing import image2drawing
from fabui.cam.toolpath.laser25dplanner import Laser25DPlanner
from fabui.cam.toolpath.debug25dplanner import Debug25DPlanner
from fabui.cam.gcode.filewriter import FileWriter
#~ from fabui.cam.toolpath.milling25dplanner import Milling25DPlanner

def prepare_raster_image(input_file, config):
    """
    """
    target_width = config['general']['width']
    target_height = config['general']['height']
    dpm = 1.0 / config['general']['dot_size']
    invert = config['general']['invert']
    
    if "optimize" not in config['general']:
        config['general']['optimize'] = 0
    ###
    
    # Load raster image
    raster = Raster(dpm=dpm, width=target_width, height=target_height, invert=invert)
    raster.loadFromFile(input_file)
    
    return raster

def laser_planner(input_file, gcode_file, config, preview_file=''):
    """
    Laser engraving
    """
    scale = config['general']['dot_size']
    align_map = {
        'middle-center': Drawing2D.MIDDLE_CENTER,
        'middle-left':   Drawing2D.MIDDLE_LEFT,
        'middle-right':  Drawing2D.MIDDLE_RIGHT,
        'top-center':    Drawing2D.TOP_CENTER,
        'top-left':      Drawing2D.TOP_LEFT,
        'top-right':     Drawing2D.TOP_RIGHT,
        'bottom-center': Drawing2D.BOTTOM_CENTER,
        'bottom-left':   Drawing2D.BOTTOM_LEFT,
        'bottom-right':  Drawing2D.BOTTOM_RIGHT
    }
    ###
    
    raster = prepare_raster_image(input_file, config)
    
    # Convert raster to drawing
    drawing = image2drawing(raster, config)
    # Scale to correct size
    drawing.scale( scale, scale )
    # Align to selected homing point
    drawing.align_to( align_map[ config['general']['align'] ] )
    
    # Generate toolpaths
    tpp = Laser25DPlanner(gcode_file, config, ndigits=3)
    tpp.plan(drawing)
    tpp.finish()
    
    # Generate preview
    if preview_file:
        drawing.scale( 1 / scale, 1 / scale )
        
        drawing.normalize()
        
        width = drawing.width()
        height = drawing.height()
    
        dbg = Debug25DPlanner(width, height, config, ndigits=3)
        dbg.plan(drawing)
        dbg.finish()
        
        import cv2
        cv2.imwrite(preview_file, dbg.cnc.dbg_img)

def main():
    # SETTING EXPECTED ARGUMENTS
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("config_file",          help="Preset file [json file]")
    parser.add_argument("input_file",           help="Image file [jpg, png]")
    parser.add_argument("-d", "--preview", action="store_true",      help="Generate preview image", default=False)
    parser.add_argument("-o", "--output",       help="Output gcode file.",    default='output.gcode')
    parser.add_argument("-P", "--planner",      help="Planner (laser, milling).")
    
    # GET ARGUMENTS
    args = parser.parse_args()

    # INIT VARs
    gcode_file      = args.output
    input_file      = args.input_file
    config_file     = args.config_file
    planner         = args.planner
    preview         = args.preview
    preview_file    = ''

    topdir = os.path.dirname(os.path.abspath(__file__))
    output_path = os.path.dirname(gcode_file)

    try:
        os.makedirs(output_path)
    except:
        pass

    if planner == 'laser':    
        schema_file = os.path.join(topdir, "schemas/laser-raster.schema")
    #~ elif planner == 'milling':
        #~ schema_file = "schemas/milling-vector.schema"
    else:
        print "Unknown planner '{0}'".format(planner)
        exit(1)

    with open(schema_file) as f:
        schema = json.load(f)
        
    with open(config_file) as f:
        config = json.load(f)
    
    try:
        validate(config, schema)
        layer_schema = schema['properties']['layers']['item']
        for layer in config['layers']:
            validate(layer, layer_schema)
        
        output = FileWriter(gcode_file)

        if preview:
            
            preview_file = os.path.join(output_path, 'preview.png')
        
        if planner == 'laser':
            laser_planner(input_file, output, config, preview_file)
        
        output.close()
        
    except Exception as e:
        print str(e)
        exit(1)
    
if __name__ == "__main__":
    main()
