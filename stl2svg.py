#!/usr/bin/env python
# # -*- coding: utf-8; -*-
#
# (c) 2020 Colibri-Embedded
#
# This file is part of fabui-cam.
#

__authors__ = "Daniel Kesler"
__license__ = "MIT"
__version__ = "1.0"

# Import standard python module
import argparse
import time
import gettext
import json
import os
import subprocess

from jsonschema import validate
from fabui.cam.slicer.slic3r import ConfigConverter

def run_slic3r(config, input_filename, output_filename):
    cc = ConfigConverter(config)

    errors, cfg, cli = cc.convert(cli_only=True)

    if errors:
        for e in errors:
            print e['msg']

        exit(1)

    cmd = 'slic3r ' + input_filename + cli + ' -o ' + output_filename

    return subprocess.call(cmd.split())

def main():
    # SETTING EXPECTED ARGUMENTS
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c",  dest='config_files', action='append',   help="Preset file [json file]", default=[])
    parser.add_argument("input_file",           help="Image file [jpg, png]")
    parser.add_argument("-o", "--output",       help="Output svg file.",    default='output.svg')
    
    # GET ARGUMENTS
    args = parser.parse_args()

    # INIT VARs
    svg_file      = args.output
    input_file      = args.input_file
    config_files     = args.config_files
    
    topdir = os.path.dirname(os.path.abspath(__file__))
    schema_file = os.path.join(topdir, "schemas/slic3r-svg.schema")

    with open(schema_file) as f:
        schema = json.load(f)
    
    config = {}
    for config_file in config_files:
        with open(config_file) as f:
            config.update( json.load(f) )
    
    try:
        validate(config, schema)
        exit_code = run_slic3r(config, input_file, svg_file)
        exit(exit_code)
        
    except Exception as e:
        print str(e)
        exit(1)
    
if __name__ == "__main__":
    main()